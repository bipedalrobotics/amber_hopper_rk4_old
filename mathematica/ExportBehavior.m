(* ::Package:: *)

(* ::Title:: *)
(*Export Behavior*)


(* ::Section:: *)
(*Configure symbol subsititution*)


(* compute maximum number of parameter vectors*)
nParamMaxRD1=Block[
	{name},
	Max[
		Table[
			name=ToExpression[domain["name"]];
			Subscript[nParamRD1, name]
			,
			{domain,domains}
		]
	]
];
nParamMaxRD2=Block[
	{name},
	Max[
		Table[
			name=ToExpression[domain["name"]];
			Subscript[nParamRD2, name]*Subscript[nOutputRD2, name]
			,
			{domain,domains}
		]
	]
];
nParamMaxPhaseVars=Block[
	{name},
	Max[
		Table[
			name=ToExpression[domain["name"]];
			Subscript[nParamPhaseVar, name]
			,
			{domain,domains}
		]
	]
];


statesubs=Dispatch@Join[
	((\[ScriptCapitalX][[#+1,1]]-> HoldForm@x[[#]]&)/@(Range[2*ndof]-1)),
	{\[Sigma][t]-> HoldForm@tau[[0]],D[\[Sigma][t],t]-> HoldForm@dtau[[0]]},
	((v[#+1]->HoldForm@v[[#]]&)/@(Range[nParamMaxRD1]-1)),
	((a[#+1]-> HoldForm@a[[#]]&)/@(Range[nParamMaxRD2]-1)),
	((p[#+1]-> HoldForm@p[[#]]&)/@(Range[nParamMaxPhaseVars]-1))
];


SetOptions[CseWriteCpp,
	Directory->$DurusSimExportPath,
	SubstitutionRules-> statesubs
];


(* ::Section:: *)
(*Export Expressions*)


Block[
	{(*nameStr,name*)},
	Table[
		nameStr = domain["name"];
		name=ToExpression[nameStr];
		SetOptions[CseWriteCpp,
			ArgumentLists-> {x},
			ArgumentDimensions-> {{ndof*2,1}}];
		(* holonomic constraints *)
		CseWriteCpp["hol_"<>nameStr,{Subscript[hol,name]}];
		CseWriteCpp["Jhol_"<>nameStr,{Subscript[Jhol,name]}];
		CseWriteCpp["dJhol_"<>nameStr,{Subscript[dJhol,name]}];
		If[!EmptyQ[Subscript[Jimp, name]],
			CseWriteCpp["Jimp_"<>nameStr,{Subscript[Jimp,name]}];
		];
		(* holonomic constraints *)
		(*
		CseWriteCpp["opthol_"<>nameStr,{Subscript[opthol,name]}];
		CseWriteCpp["Jopthol_"<>nameStr,{Subscript[Jopthol,name]}];
		CseWriteCpp["dJopthol_"<>nameStr,{Subscript[dJopthol,name]}];
		*)

		(* guard condition *)		
		CseWriteCpp["guard_"<>nameStr,{{Subscript[guard,name]}}];
		CseWriteCpp["Jguard_"<>nameStr,{Subscript[Jguard,name]}];

		(* actual non-linear hip position *)
		
		CseWriteCpp["phip_"<>nameStr,{{Subscript[phip,name]}}];
		CseWriteCpp["vhip_"<>nameStr,{{Subscript[vhip,name]}}];
	
		(* actual outputs *)
		If[!EmptyQ[Subscript[ya1,name]],
			CseWriteCpp["ya1_"<>nameStr,{Subscript[ya1,name]}];
			CseWriteCpp["Dya1_"<>nameStr,{Subscript[Dya1,name]}];
		];
		CseWriteCpp["ya2_"<>nameStr,{Subscript[ya2,name]}];
		CseWriteCpp["Dya2_"<>nameStr,{Subscript[Dya2,name]}];
		CseWriteCpp["DLfya2_"<>nameStr,{Subscript[DLfya2,name]}];

		(* phase variables *)
		CseWriteCpp["deltaphip_"<>nameStr,{Subscript[deltaphip,name]}];
		CseWriteCpp["Jdeltaphip_"<>nameStr,{Subscript[Jdeltaphip,name]}];
		
		SetOptions[CseWriteCpp,
			ArgumentLists-> {x,p},
			ArgumentDimensions-> {{ndof*2,1},{Subscript[nParamPhaseVar, name],1}}];
		CseWriteCpp["tau_"<>nameStr,{Subscript[tau,name]}];
		CseWriteCpp["dtau_"<>nameStr,{Subscript[dtau,name]}];
		CseWriteCpp["Jtau_"<>nameStr,{Subscript[Jtau,name]}];
		CseWriteCpp["Jdtau_"<>nameStr,{Subscript[Jdtau,name]}];

		(* desired function *)
		(** relatvie degree one output **)
		If[!EmptyQ[Subscript[yd1,name]],
			SetOptions[CseWriteCpp,
				ArgumentLists-> {tau,v},
				ArgumentDimensions-> {{1,1},{Subscript[nParamRD1, name],1}}];
			CseWriteCpp["yd1_"<>nameStr,{Subscript[yd1,name]}];		
			CseWriteCpp["dyd1_"<>nameStr,{Subscript[dyd1,name]}];
		];
		(** relative degree two outputs **)
		SetOptions[CseWriteCpp,
			ArgumentLists-> {tau,a},
			ArgumentDimensions-> {{1,1},{Subscript[nParamRD2, name]*Subscript[nOutputRD2, name],1}}];
		CseWriteCpp["yd2_"<>nameStr,{Subscript[yd2,name]}];		
		CseWriteCpp["dyd2_"<>nameStr,{Subscript[dyd2,name]}];
		CseWriteCpp["ddyd2_"<>nameStr,{Subscript[ddyd2,name]}];
		(*
		SetOptions[CseWriteCpp,
			ArgumentLists-> {x},
			ArgumentDimensions-> {{ndof*2,1}}];
		If[!EmptyQ[Subscript[ikActive, name]],
			CseWriteCpp["ikActive_"<>nameStr,{Subscript[ikActive,name]}];
			CseWriteCpp["JikActive_"<>nameStr,{Subscript[JikActive,name]}];
			CseWriteCpp["ikInactive_"<>nameStr,{Subscript[ikInactive,name]}];
			CseWriteCpp["JikInactive_"<>nameStr,{Subscript[JikInactive,name]}];
		];
		*)
		,
		{domain,domains}
	];
];
