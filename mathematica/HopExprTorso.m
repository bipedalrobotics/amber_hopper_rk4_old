(* ::Package:: *)

(* ::Title:: *)
(*DURUS 3D ROBOT*)


SetDirectory[NotebookDirectory[]]
UtilsPath=FileNameJoin[{NotebookDirectory[],"utils"}];
$Path=DeleteDuplicates[Append[$Path,UtilsPath]];


Needs["Units`"];
Needs["RobotLinks`"];
Needs["ExtraUtil`"];
Needs["AmberShared`"];
Needs["SnakeYaml`"];
Needs["CseOptimization`"];


On[Assert];


YamlInit[];


(* exported files for simulation path *)
$DurusSimExportPath=FileNameJoin[{ParentDirectory[],"matlab","build_sim","Periodic2DFlatWalking"}];
EnsureDirectoryExists[$DurusSimExportPath];(*
EnsureDirectoryExists[FileNameJoin[{$DurusSimExportPath,"src"}]]
EnsureDirectoryExists[FileNameJoin[{$DurusSimExportPath,"include"}]]
*)


<<HipBasedModel`;


$Context


<<TorsoBasedModel`;


$Context


Subscript[p, hipHP]=Subscript[HipModel`p, HipModel`hip]/.TorsoModel`hipBaseSubs;
Subscript[p, hipRF]=Subscript[HipModel`p, HipModel`RightFoot]/.TorsoModel`hipBaseSubs;
Subscript[p, hipLF]=Subscript[HipModel`p, HipModel`LeftFoot]/.TorsoModel`hipBaseSubs;
Subscript[p, hipBM]=Subscript[HipModel`p, HipModel`Boom]/.TorsoModel`hipBaseSubs;
Subscript[v, hipHP]=Subscript[HipModel`v, HipModel`hip]/.TorsoModel`hipBaseSubs;
Subscript[v, hipRF]=Subscript[HipModel`v, HipModel`RightFoot]/.TorsoModel`hipBaseSubs;
Subscript[v, hipLF]=Subscript[HipModel`v, HipModel`LeftFoot]/.TorsoModel`hipBaseSubs;
Subscript[v, hipBM]=Subscript[HipModel`v, HipModel`Boom]/.TorsoModel`hipBaseSubs;
Subscript[\[Psi], hipRF]=Subscript[HipModel`\[Psi], HipModel`RightFoot]/.TorsoModel`hipBaseSubs;
Subscript[\[Psi], hipLF]=Subscript[HipModel`\[Psi], HipModel`LeftFoot]/.TorsoModel`hipBaseSubs;
Subscript[\[Psi], hipHP]=Subscript[HipModel`\[Psi], HipModel`hip]/.TorsoModel`hipBaseSubs;
Subscript[\[Psi], hipBM]=Subscript[HipModel`\[Psi], HipModel`Boom]/.TorsoModel`hipBaseSubs;
Subscript[\[Omega], hipRF]=Subscript[HipModel`\[Omega], HipModel`RightFoot]/.TorsoModel`hipBaseSubs;
Subscript[\[Omega], hipLF]=Subscript[HipModel`\[Omega], HipModel`LeftFoot]/.TorsoModel`hipBaseSubs;
Subscript[\[Omega], hipBM]=Subscript[HipModel`\[Omega], HipModel`Boom]/.TorsoModel`hipBaseSubs;
Subscript[\[Omega], hipHP]=Subscript[HipModel`\[Omega], HipModel`hip]/.TorsoModel`hipBaseSubs;


Subscript[p, torRF]=Subscript[TorsoModel`p, TorsoModel`RightFoot];
Subscript[p, torLF]=Subscript[TorsoModel`p, TorsoModel`LeftFoot];
Subscript[p, torBM]=Subscript[TorsoModel`p, TorsoModel`Boom];
Subscript[p, torHP]=Subscript[TorsoModel`p, TorsoModel`hip];
Subscript[v, torRF]=Subscript[TorsoModel`v, TorsoModel`RightFoot];
Subscript[v, torLF]=Subscript[TorsoModel`v, TorsoModel`LeftFoot];
Subscript[v, torBM]=Subscript[TorsoModel`v, TorsoModel`Boom];
Subscript[v, torHP]=Subscript[TorsoModel`v, TorsoModel`hip];
Subscript[\[Psi], torRF]=Subscript[TorsoModel`\[Psi], TorsoModel`RightFoot];
Subscript[\[Psi], torLF]=Subscript[TorsoModel`\[Psi], TorsoModel`LeftFoot];
Subscript[\[Psi], torBM]=Subscript[TorsoModel`\[Psi], TorsoModel`Boom];
Subscript[\[Psi], torHP]=Subscript[TorsoModel`\[Psi], TorsoModel`hip];
Subscript[\[Omega], torRF]=Subscript[TorsoModel`\[Omega], TorsoModel`RightFoot];
Subscript[\[Omega], torLF]=Subscript[TorsoModel`\[Omega], TorsoModel`LeftFoot];
Subscript[\[Omega], torBM]=Subscript[TorsoModel`\[Omega], TorsoModel`Boom];
Subscript[\[Omega], torHP]=Subscript[TorsoModel`\[Omega], TorsoModel`hip];


qeRandomSubs=Table[TorsoModel`Qe[[i,1]]->RandomReal[{-Pi,Pi}],{i,TorsoModel`ndof}];
dqeRandomSubs=Table[TorsoModel`dQe[[i,1]]->RandomReal[{-3,3}],{i,TorsoModel`ndof}];
xRandomSubs = Join[qeRandomSubs,dqeRandomSubs];
x0Subs=Join[TorsoModel`qe0subs,TorsoModel`dqe0subs];


Values[Association@xRandomSubs]


TorsoModel`\[ScriptCapitalR]0/.xRandomSubs


TorsoModel`hipBasePos/.xRandomSubs


(Subscript[p, hipHP]/.xRandomSubs)-(Subscript[p, torHP]/.xRandomSubs)
(Subscript[v, hipHP]/.xRandomSubs)-(Subscript[v, torHP]/.xRandomSubs)
(Subscript[\[Psi], hipHP]/.xRandomSubs)-(Subscript[\[Psi], torHP]/.xRandomSubs)
(Subscript[\[Omega], hipHP]/.xRandomSubs)-(Subscript[\[Omega], torHP]/.xRandomSubs)


(Subscript[p, hipRF]/.xRandomSubs)==(Subscript[p, torRF]/.xRandomSubs)
(Subscript[p, hipLF]/.xRandomSubs)==(Subscript[p, torLF]/.xRandomSubs)
(Subscript[p, hipBM]/.xRandomSubs)==(Subscript[p, torBM]/.xRandomSubs)


(Subscript[v, hipRF]/.xRandomSubs)==(Subscript[v, torRF]/.xRandomSubs)
(Subscript[v, hipLF]/.xRandomSubs)==(Subscript[v, torLF]/.xRandomSubs)
(Subscript[v, hipBM]/.xRandomSubs)==(Subscript[v, torBM]/.xRandomSubs)


(Subscript[\[Psi], hipRF]/.xRandomSubs)==(Subscript[\[Psi], torRF]/.xRandomSubs)
(Subscript[\[Psi], hipLF]/.xRandomSubs)==(Subscript[\[Psi], torLF]/.xRandomSubs)
(Subscript[\[Psi], hipBM]/.xRandomSubs)==(Subscript[\[Psi], torBM]/.xRandomSubs)


(Subscript[\[Omega], hipRF]/.xRandomSubs)==(Subscript[\[Omega], torRF]/.xRandomSubs)
(Subscript[\[Omega], hipLF]/.xRandomSubs)==(Subscript[\[Omega], torLF]/.xRandomSubs)
(Subscript[\[Omega], hipBM]/.xRandomSubs)==(Subscript[\[Omega], torBM]/.xRandomSubs)



a = ContextChange[Subscript[\[Omega], hipRF],"TorsoModel`"];
b = ContextChange[Subscript[\[Omega], torRF],"TorsoModel`"];


Simplify[a[[1]]]==Simplify[b[[1]]]


(* ::Section:: *)
(*Export Torso Based Model Functions*)


Subscript[qb, HipBased]=ContextChange[TorsoModel`hipBasePos,"TorsoModel`"];
Subscript[dqb, HipBased]=ContextChange[TorsoModel`hipBaseVel,"TorsoModel`"];


Subscript[pos, torRF]=ContextChange[Subscript[TorsoModel`p, TorsoModel`RightFoot],"TorsoModel`"];
Subscript[pos, torLF]=ContextChange[Subscript[TorsoModel`p, TorsoModel`LeftFoot],"TorsoModel`"];
Subscript[pos, torBM]=ContextChange[Subscript[TorsoModel`p, TorsoModel`Boom],"TorsoModel`"];
Subscript[vel, torRF]=ContextChange[Subscript[TorsoModel`v, TorsoModel`RightFoot],"TorsoModel`"];
Subscript[vel, torLF]=ContextChange[Subscript[TorsoModel`v, TorsoModel`LeftFoot],"TorsoModel`"];
Subscript[vel, torBM]=ContextChange[Subscript[TorsoModel`v, TorsoModel`Boom],"TorsoModel`"];
Subscript[ang, torRF]=ContextChange[Subscript[TorsoModel`\[Psi], TorsoModel`RightFoot],"TorsoModel`"];
Subscript[ang, torLF]=ContextChange[Subscript[TorsoModel`\[Psi], TorsoModel`LeftFoot],"TorsoModel`"];
Subscript[ang, torBM]=ContextChange[Subscript[TorsoModel`\[Psi], TorsoModel`Boom],"TorsoModel`"];
Subscript[angvel, torRF]=ContextChange[Subscript[TorsoModel`\[Omega], TorsoModel`RightFoot],"TorsoModel`"];
Subscript[angvel, torLF]=ContextChange[Subscript[TorsoModel`\[Omega], TorsoModel`LeftFoot],"TorsoModel`"];
Subscript[angvel, torBM]=ContextChange[Subscript[TorsoModel`\[Omega], TorsoModel`Boom],"TorsoModel`"];


\[ScriptCapitalX]=ContextChange[TorsoModel`\[ScriptCapitalX],"TorsoModel`"];
ndof=ContextChange[TorsoModel`ndof,"TorsoModel`"];


statesubs=Dispatch@Join[
	((\[ScriptCapitalX][[#+1,1]]-> HoldForm@x[[#]]&)/@(Range[2*ndof]-1))
];


SetOptions[CseWriteCpp,
	Directory->$DurusSimExportPath,
	ArgumentLists-> {x},
	ArgumentDimensions-> {{ndof*2,1}},
	SubstitutionRules-> statesubs
];


Subscript[h, torRF]=Flatten@Join[Subscript[pos, torRF],Subscript[ang, torRF],Subscript[vel, torRF],Subscript[angvel, torRF]];
Subscript[h, torLF]=Flatten@Join[Subscript[pos, torLF],Subscript[ang, torLF],Subscript[vel, torLF],Subscript[angvel, torLF]];
Subscript[h, torBM]=Flatten@Join[Subscript[pos, torBM],Subscript[ang, torBM],Subscript[vel, torBM],Subscript[angvel, torBM]];


exprs={
	{Subscript[qb, HipBased],"qbMap"},
	{Subscript[dqb, HipBased],"dqbMap"},
	{Subscript[h, torRF],"rightfoot_torsomodel"},
	{Subscript[h, torLF],"leftfoot_torsomodel"},
	{Subscript[h, torBM],"torso_torsomodel"}
};


Block[
	{sym,name},
	Table[
		{sym,name}=expr;
		CseWriteCpp[name,{sym}];
		,
		{expr,exprs}
	];
];


phipbase=ContextChange[Subscript[TorsoModel`p, TorsoModel`hip],"TorsoModel`"];
vhipbase=ContextChange[Subscript[TorsoModel`v, TorsoModel`hip],"TorsoModel`"];
Rhipbase=ContextChange[Subscript[TorsoModel`Rsub, TorsoModel`hip],"TorsoModel`"];
Omegahipbase=ContextChange[Subscript[TorsoModel`\[Omega], TorsoModel`hip],"TorsoModel`"];
Jomegahipbase=ContextChange[TorsoModel`J\[Omega],"TorsoModel`"];


CseWriteCpp["phipbase",{phipbase}];
CseWriteCpp["vhipbase",{vhipbase}];
CseWriteCpp["Rhipbase",{Rhipbase}];
CseWriteCpp["Omegahipbase",{Omegahipbase}];
CseWriteCpp["Jomegahipbase",{Jomegahipbase}];


(* ::Section:: *)
(*Export Hip Based Model Functions*)


Subscript[pos, hipRF]=ContextChange[Subscript[HipModel`p, HipModel`RightFoot],"HipModel`"];
Subscript[pos, hipLF]=ContextChange[Subscript[HipModel`p, HipModel`LeftFoot],"HipModel`"];
Subscript[pos, hipBM]=ContextChange[Subscript[HipModel`p, HipModel`Boom],"HipModel`"];
Subscript[vel, hipRF]=ContextChange[Subscript[HipModel`v, HipModel`RightFoot],"HipModel`"];
Subscript[vel, hipLF]=ContextChange[Subscript[HipModel`v, HipModel`LeftFoot],"HipModel`"];
Subscript[vel, hipBM]=ContextChange[Subscript[HipModel`v, HipModel`Boom],"HipModel`"];
Subscript[ang, hipRF]=ContextChange[Subscript[HipModel`\[Psi], HipModel`RightFoot],"HipModel`"];
Subscript[ang, hipLF]=ContextChange[Subscript[HipModel`\[Psi], HipModel`LeftFoot],"HipModel`"];
Subscript[ang, hipBM]=ContextChange[Subscript[HipModel`\[Psi], HipModel`Boom],"HipModel`"];
Subscript[angvel, hipRF]=ContextChange[Subscript[HipModel`\[Omega], HipModel`RightFoot],"HipModel`"];
Subscript[angvel, hipLF]=ContextChange[Subscript[HipModel`\[Omega], HipModel`LeftFoot],"HipModel`"];
Subscript[angvel, hipBM]=ContextChange[Subscript[HipModel`\[Omega], HipModel`Boom],"HipModel`"];



Subscript[h, hipRF]=Flatten@Join[Subscript[pos, hipRF],Subscript[ang, hipRF],Subscript[vel, hipRF],Subscript[angvel, hipRF]];
Subscript[h, hipLF]=Flatten@Join[Subscript[pos, hipLF],Subscript[ang, hipLF],Subscript[vel, hipLF],Subscript[angvel, hipLF]];
Subscript[h, hipBM]=Flatten@Join[Subscript[pos, hipBM],Subscript[ang, hipBM],Subscript[vel, hipBM],Subscript[angvel, hipBM]];


\[ScriptCapitalX]=ContextChange[HipModel`\[ScriptCapitalX],"HipModel`"];
ndof=ContextChange[HipModel`ndof,"HipModel`"];


statesubs=Dispatch@Join[
	((\[ScriptCapitalX][[#+1,1]]-> HoldForm@x[[#]]&)/@(Range[2*ndof]-1))
];


SetOptions[CseWriteCpp,
	Directory->$DurusSimExportPath,
	ArgumentLists-> {x},
	ArgumentDimensions-> {{ndof*2,1}},
	SubstitutionRules-> statesubs
];


exprs={
	{Subscript[h, hipRF],"rightfoot_hipmodel"},
	{Subscript[h, hipLF],"leftfoot_hipmodel"},
	{Subscript[h, hipBM],"torso_hipmodel"}
};


Block[
	{sym,name},
	Table[
		{sym,name}=expr;
		CseWriteCpp[name,{sym}];
		,
		{expr,exprs}
	];
];


xSubsGlobal=ContextChange[xRandomSubs,"TorsoModel`"]


phipbase/.xSubsGlobal


Subscript[qb, HipBased]/.xSubsGlobal


Values[xSubsGlobal]
