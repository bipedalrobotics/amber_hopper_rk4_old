(* ::Package:: *)

(* ::Title:: *)
(*Hopper Optimization Expressions*)


(* ::Section:: *)
(*Load domain specified informations*)


(* domain specified constraints *)
domainConstr=Apply[Association,Association@domain["constraints"],{1}];
(*
   domain transition specification, it will be different than what defined
   in simulation (foot swapping for periodic gait) 
*)
domainTrans=Association@domain["transition"];
nextDomain=ToExpression[domainTrans["nextDomain"]];
(* domain name for indexing *)
nameStr=domain["name"];
name=ToExpression[nameStr];
kinConsts=First@model["kinConst"];
footDimSubs=KeyMap[ToExpression,kinConsts];
constraints=Apply[Association,Association@domain["constraints"],{2}];


(* ::Subsection:: *)
(*Define Variables*)


\[ScriptCapitalU]=Vec[Table[\[ScriptU][i],{i,Subscript[nAct, name]}]];
\[ScriptCapitalF]e=Vec[Table[\[ScriptF][i],{i,Subscript[nHolConstr, name]}]];
\[ScriptCapitalV]=Vec[Table[v[i],{i,Subscript[nParamRD1, name]}]];
\[ScriptCapitalP]=Vec[Table[p[i],{i,Subscript[nParamPhaseVar, name]}]];
\[ScriptCapitalA]=Vec[Table[a[i],{i,Subscript[nParamRD2, name]*Subscript[nOutputRD2, name]}]];
\[ScriptCapitalH]=Vec[Table[\[ScriptH][i],{i,2}]];(*h[1]\[Rule]step length, h[2]\[Rule]step width*)


(* states of next node *)
Qn=Qe/.Subscript[x_,y_]:>Subscript[x,{y,n}];
dQn=D[Qn,t];
ddQn=D[dQn,t];
(* states of mid point *)
Qm=Qe/.Subscript[x_,y_]:>Subscript[x,{y,m}];
dQm=D[Qm,t];
ddQm=D[dQm,t];


(* time interval *)
\[Delta]t=Switch[optOptions["NodeDistribution"],
	"CGL",tf (Cos[(Pi (k-1)/(nNode-1))]-Cos[(Pi/(nNode-1)) k ])/2,
	"Uniform",tf/(nNode-1),
	"Variable",tf
];


\[ScriptCapitalV]n=Vec[Table[Subscript[v, n][i],{i,Subscript[nParamRD1, name]}]];
\[ScriptCapitalP]n=Vec[Table[Subscript[p, n][i],{i,Subscript[nParamPhaseVar, name]}]];
\[ScriptCapitalA]n=Vec[Table[Subscript[a, n][i],{i,Subscript[nParamRD2, name]*Subscript[nOutputRD2, name]}]];
\[ScriptCapitalH]n=Vec[Table[Subscript[\[ScriptH], n][i],{i,2}]];


(* ::Subsection:: *)
(*Define Functions*)


GetNodeTimeSubs["CGL"]:={t->(-Cos[Pi (k-1)/(nNode-1)]tf+tf)/2};
GetNodeTimeSubs["Uniform"]:={t-> ((k-1)/(nNode-1))*tf};
GetNodeTimeSubs["Variable"]:=$Failed; (*no definition available*)
GetMidTimeSubs["CGL"]:={t->(-Cos[Pi (k-1)/(nNode-1)]tf-Cos[Pi (k)/(nNode-1)]tf+2tf)/4};
GetMidTimeSubs["CGL"]:={t-> ((k-1/2)/(nNode-1))*tf};
GetMidTimeSubs["Variable"]:=$Failed; (* no definition available *)


(*
GetConstrValue["NonstanceFootPosX"]:={-\[ScriptH][1]};(*step length*)
GetConstrValue["RightFootPosY"]:={-\[ScriptH][2]/2};(*step width*)
GetConstrValue["LeftFootPosX"]:={-\[ScriptH][1]};(*step length*)
GetConstrValue["LeftFootPosY"]:={\[ScriptH][2]/2};(*step width*)
GetConstrValue["RightToePosY"]:={-\[ScriptH][2]/2};(*step width*)
GetConstrValue["LeftToePosX"]:={-\[ScriptH][1]};(*step length*)
GetConstrValue["LeftToePosY"]:={\[ScriptH][2]/2};(*step width*)
GetConstrValue["RightHeelPosY"]:={-\[ScriptH][2]/2};(*step width*)
GetConstrValue["LeftHeelPosX"]:={\[ScriptH][1]-(lt+lh)};(*step length*)
GetConstrValue["LeftHeelPosY"]:={\[ScriptH][2]/2};(*step width*)
*)
GetConstrValue[pos_?StringQ]:={0};


RemoveTimeDependence[expr_]:=expr/.Join[(#-> Qt[First@First@Position[Qe,#]])&/@Flatten[Qe],
		(#-> dQt[First@First@Position[dQe,#]])&/@Flatten[dQe],
		(#-> ddQt[First@First@Position[ddQe,#]])&/@Flatten[ddQe]];
nodetimesubs=GetNodeTimeSubs[optOptions["NodeDistribution"]];
midtimesubs=GetMidTimeSubs[optOptions["NodeDistribution"]];


(* ::Section:: *)
(*Generate Expressions*)


(* ::Subsection:: *)
(*Controller Input Dynamics (RHS of the EOM)*)


Block[
	{controlDynamics},
	controlDynamics = -Subscript[\[ScriptCapitalB]e, name].\[ScriptCapitalU] - Subscript[Jhol, name]\[Transpose].\[ScriptCapitalF]e;
	ExportWithGradient["controlDynamics_"<>nameStr,
		               controlDynamics,{Qe,dQe,ddQe,\[ScriptCapitalU],\[ScriptCapitalF]e}];
];


(* ::Subsection:: *)
(*Step Length Constraint*)


(* ::Text:: *)
(*Limitations: px as phase_variable.*)


Block[
	{stepLengthConstr},
	stepLengthConstr =  Qe[[1]] - \[ScriptCapitalP][[2]] - \[ScriptCapitalH][[1]];
    If[StringMatchQ[nameStr, "fly"],
	    ExportWithGradient["stepLengthConstr_"<>nameStr,
                           stepLengthConstr,{Qe,\[ScriptCapitalP],\[ScriptCapitalH]}];
    ];
];


(* ::Subsection:: *)
(*Holonomic Constraints*)


Block[
	{holConstr,holonomicPos},
	holConstr=GetConstrValue[#]&/@Subscript[domainConfig, name]["constraints"];
	holonomicPos=Subscript[hol, name]-Flatten[holConstr/.footDimSubs];
	(*ExportWithGradient["holonomicPos_"<>nameStr,holonomicPos,{Qe,\[ScriptCapitalH]}];*)
	ExportWithGradient["holonomicPos_"<>nameStr,holonomicPos,{Qe}];
];


Block[
	{holnomicVel},
	holnomicVel=Subscript[Jhol, name].dQe;
	ExportWithGradient["holonomicVel_"<>nameStr,holnomicVel,{Qe,dQe}];
];


Block[
	{holnomicAcc},
	holnomicAcc=Subscript[Jhol, name].ddQe+Subscript[dJhol, name].dQe;
	ExportWithGradient["holonomicAcc_"<>nameStr,holnomicAcc,{Qe,dQe,ddQe}];
];


(* ::Subsection:: *)
(*Guard Condition*)


Block[
    {guardConfig,guardCond},
	guardConfig=Subscript[domainConfig, name]["guard"]//First;
	guardCond=
		Switch[guardConfig["type"],
			"kinematics",
				Subscript[guard, name] - guardConfig["threshold"],
			"forces",
				\[ScriptCapitalF]e[[First@(guardConfig["name"]/.PositionIndex[Subscript[domainConfig, name]["constraints"]])]]
				- guardConfig["threshold"],
			"phase",
				Subscript[tau, name] - guardConfig["threshold"]
		];
	Switch[guardConfig["type"],
		"kinematics",ExportWithGradient["guard_"<>nameStr,guardCond,{Qe}],
		"forces",ExportWithGradient["guard_"<>nameStr,guardCond,{\[ScriptCapitalF]e}],
		"phase",ExportWithGradient["guard_"<>nameStr,guardCond,{Qe,\[ScriptCapitalP]}]
	];
];


(* ::Subsection:: *)
(*Relative Degeree One Output*)


Block[
	{y1Vel,vars,y1Acc},
	If[!EmptyQ[Subscript[ya1, name]],
	If[optOptions["TimeBased"],
		(* relative degree one output, it can be constrained to be zero or NOT*)
		y1Vel=Subscript[ya1, name]-Subscript[yd1, name]/.{\[Sigma][t]->t};	
		vars = RemoveTimeDependence[{tf,Qe,dQe,\[ScriptCapitalV]}];
		ExportWithGradient["y1Vel_"<>nameStr,
			RemoveTimeDependence[y1Vel]/.nodetimesubs,vars,{k,nNode}];
		ExportWithGradient["y1VelMid_"<>nameStr,
			RemoveTimeDependence[y1Vel]/.midtimesubs,vars,{k,nNode}];
	
		(* degree one output linear dynamics, it has to be zero *)
		y1Acc=D[y1Vel,t] + optOptions["ep"]*y1Vel;
		vars = RemoveTimeDependence[{tf,Qe,dQe,ddQe,\[ScriptCapitalV]}];
		ExportWithGradient["y1Acc_"<>nameStr,
			RemoveTimeDependence[y1Acc]/.nodetimesubs,vars,{k,nNode}];
		ExportWithGradient["y1AccMid_"<>nameStr,
			RemoveTimeDependence[y1Acc]/.midtimesubs,vars,{k,nNode}];
		,
		y1Vel=Subscript[ya1, name]-Subscript[yd1, name]/.{\[Sigma][t]-> Subscript[tau, name]};
		ExportWithGradient["y1Vel_"<>nameStr,y1Vel,{Qe,dQe,\[ScriptCapitalP],\[ScriptCapitalV]}];
		
		y1Acc=D[y1Vel,t] + optOptions["ep"]*y1Vel;
		ExportWithGradient["y1Acc_"<>nameStr,y1Acc,{Qe,dQe,ddQe,\[ScriptCapitalP],\[ScriptCapitalV]}];
	];
	];
];


(* ::Subsection:: *)
(*Relative Degree Two Outputs*)


Block[
	{vars,y2Pos,y2Vel,y2Acc},
	If[optOptions["TimeBased"],
		(* output dynamics, simply force them to be zero to guarantee Zero Dynamics *)
		y2Pos = Flatten[Subscript[ya2, name]-Subscript[yd2, name]/.{\[Sigma][t]->t}];
		y2Vel = D[y2Pos,t];
		y2Acc = D[y2Vel,t] + 2*optOptions["ep"]*y2Vel + (optOptions["ep"]^2)*y2Pos;
		(*Position Level*)	
		vars=RemoveTimeDependence[{tf,Qe,\[ScriptCapitalA]}];
		ExportWithGradient["y2Pos_"<>nameStr,
			RemoveTimeDependence[y2Pos]/.nodetimesubs,vars,{k,nNode}];
		ExportWithGradient["y2PosMid_"<>nameStr,
			RemoveTimeDependence[y2Pos]/.midtimesubs,vars,{k,nNode}];
	
		(*Velocity Level*)	
		vars=RemoveTimeDependence[{tf,Qe,dQe,\[ScriptCapitalA]}];
		ExportWithGradient["y2Vel_"<>nameStr,
			RemoveTimeDependence[y2Vel]/.nodetimesubs,vars,{k,nNode}];
		ExportWithGradient["y2VelMid_"<>nameStr,
			RemoveTimeDependence[y2Vel]/.midtimesubs,vars,{k,nNode}];
	
		(*Acceleration Level*)	
		vars=RemoveTimeDependence[{tf,Qe,dQe,ddQe,\[ScriptCapitalA]}];
		ExportWithGradient["y2Acc_"<>nameStr,
			RemoveTimeDependence[y2Acc]/.nodetimesubs,vars,{k,nNode}];
		ExportWithGradient["y2AccMid_"<>nameStr,
			RemoveTimeDependence[y2Acc]/.midtimesubs,vars,{k,nNode}];
		,
		(** State Based **)
		(* output dynamics, simply force them to be zero to guarantee Zero Dynamics *)
		y2Pos = Flatten[Subscript[ya2, name]-Subscript[yd2, name]/.{\[Sigma][t]->Subscript[tau, name]}];
		y2Vel = D[y2Pos,t];
		y2Acc = D[y2Vel,t];(* + 2*optOptions["ep"]*y2Vel + (optOptions["ep"]^2)*y2Pos;*)
		
		(*Position Level*)	
		vars={Qe,\[ScriptCapitalP],\[ScriptCapitalA]};
		ExportWithGradient["y2Pos_"<>nameStr,
			y2Pos,vars];
	
		(*Velocity Level*)	
		vars={Qe,dQe,\[ScriptCapitalP],\[ScriptCapitalA]};
		ExportWithGradient["y2Vel_"<>nameStr,
			y2Vel,vars];
	
		(*Position Level*)	
		vars={Qe,dQe,ddQe,\[ScriptCapitalP],\[ScriptCapitalA]};
		ExportWithGradient["y2Acc_"<>nameStr,
			y2Acc,vars];
	];
];


(* ::Subsection:: *)
(*Phase Parameters*)


(* ::Text:: *)
(*Note: no phipf for s.s. generated, since phipf_ss is wrong if using this code*)


Block[
	{phip0Index,deltaPhip0,deltaPhipf},
	phip0Index = Subscript[domainConfig, name]["indexInStep"]+1;
	deltaPhip0 = \[ScriptCapitalP][[phip0Index]]-Subscript[deltaphip, name];
	ExportWithGradient["deltaPhip0_"<>nameStr,deltaPhip0,{Qe,\[ScriptCapitalP]}];
	If[Subscript[domainConfig, name]["indexInStep"]==Subscript[domainConfig, name]["numDomainsInStep"],
		deltaPhipf = \[ScriptCapitalP][[1]]-Subscript[deltaphip, name];
		ExportWithGradient["deltaPhipf_"<>nameStr,deltaPhipf,{Qe,\[ScriptCapitalP]}];
	];
];


(* ::Subsection:: *)
(*Integration Condition*)


Block[
	{IntPos,IntVel},
	Switch[optOptions["IntegrationScheme"],
		"Hermite-Simpson",
			(* position level *)
			IntPos=Qn-Qe-\[Delta]t (dQn+4dQm+dQe)/6;
			ExportWithGradient["intPos_"<>nameStr,IntPos,
				{tf,Qe,dQe,dQm,Qn,dQn},{k,nNode}];

			(* velocity level *)
			IntVel=dQn-dQe-\[Delta]t (ddQn+4ddQm+ddQe)/6;
			ExportWithGradient["intVel_"<>nameStr,IntVel,
				{tf,dQe,ddQe,ddQm,dQn,ddQn},{k,nNode}];
		,
		"Trapezoidal",
			(* position level *)
			IntPos=Qn-Qe-\[Delta]t (dQn+dQe)/2;
			ExportWithGradient["intPos_"<>nameStr,IntPos,
				{tf,Qe,dQe,Qn,dQn},{k,nNode}];
		
			(* velocity level *)
			IntVel=dQn-dQe-\[Delta]t (ddQn+ddQe)/2;
			ExportWithGradient["intVel_"<>nameStr,IntVel,
				{tf,dQe,ddQe,dQn,ddQn},{k,nNode}];
	];
];


(* ::Subsection:: *)
(*Midpoint Constraints (if using hermite-Simpson)*)


Block[
	{MidPos,MidVel},
	If[optOptions["IntegrationScheme"] == "Hermite-Simpson",
		(* position level *)
		MidPos=Qm-(Qe+Qn)/2-\[Delta]t (dQe-dQn)/8;
		ExportWithGradient["midPointPos_"<>nameStr,MidPos,
			{tf,Qe,dQe,Qm,Qn,dQn},{k,nNode}];

		(* velocity level *)
		MidVel=dQm-(dQe+dQn)/2-\[Delta]t (ddQe-ddQn)/8;
		ExportWithGradient["midPointVel_"<>nameStr,MidVel,
			{tf,dQe,ddQe,dQm,dQn,ddQn},{k,nNode}];
	];
];


(* ::Subsection:: *)
(*Monotonic constraints on certain joints.*)


(*If[ Subscript[domainConfig, name]["indexInStep"]\[Equal]1,
Block[
	{monoCont},
	monoCont = Qe[[8]] - Qn[[8]];
	ExportWithGradient["monoCont_"<>nameStr,monoCont,{Qe,Qn}];
];
];*)


(* ::Subsection:: *)
(*Parameters Continuity*)


Block[
	{pCont},
	pCont = \[ScriptCapitalP] - \[ScriptCapitalP]n;
	ExportWithGradient["pCont_"<>nameStr,pCont,{\[ScriptCapitalP],\[ScriptCapitalP]n}];
];


If[!EmptyQ[Subscript[ya1, name]],
Block[
	{vCont},
	vCont = \[ScriptCapitalV] - \[ScriptCapitalV]n;
	ExportWithGradient["vCont_"<>nameStr,vCont,{\[ScriptCapitalV],\[ScriptCapitalV]n}];
];
];


Block[
	{aCont},
	aCont = \[ScriptCapitalA] - \[ScriptCapitalA]n;
	ExportWithGradient["aCont_"<>nameStr,aCont,{\[ScriptCapitalA],\[ScriptCapitalA]n}];
];


Block[
	{desHolCont},
	desHolCont = \[ScriptCapitalH] - \[ScriptCapitalH]n;
	ExportWithGradient["desHolCont_"<>nameStr,desHolCont,{\[ScriptCapitalH],\[ScriptCapitalH]n}];
];


(* ::Subsection:: *)
(*Parameters Continuity (Inter-domain)*)


(* ::Subsubsection:: *)
(*Desired Holonomic Constraints*)


(*\[ScriptCapitalH]n=Vec[Table[Subscript[\[ScriptH], n][i],{i,Subscript[nHolConstr, nextDomain]}]];
Block[
	{nextDomainIndices,currentDomainIndices,desHolContDomain},
	(* define domains except the last domain in a step *)
	If[Subscript[domainConfig, name]["indexInStep"]!=Subscript[domainConfig, name]["numDomainsInStep"],
		(* Find indices for constraints that defined both in current and next domain *)
		nextDomainIndices = Cases[Subscript[domainConfig, name]["constraints"]
			/.PositionIndex[Subscript[domainConfig, nextDomain]["constraints"]],_Integer,{2}];
		currentDomainIndices = Cases[Subscript[domainConfig, nextDomain]["constraints"]
			/.PositionIndex[Subscript[domainConfig, name]["constraints"]],_Integer,{2}];		
		desHolContDomain = \[ScriptCapitalH][[currentDomainIndices,1]] - \[ScriptCapitalH]n[[nextDomainIndices,1]];
		ExportWithGradient["desHolContDomain_"<>nameStr,desHolContDomain,{\[ScriptCapitalH],\[ScriptCapitalH]n}];
	];
];
*)


(* ::Subsubsection:: *)
(*Degree Two Output Parameters*)


\[ScriptCapitalA]n=Vec[Table[Subscript[a, n][i],{i,Subscript[nParamRD2, nextDomain]*Subscript[nOutputRD2, nextDomain]}]];
Block[
	{nextDomainOutputs,currentDomainOutputs,nextDomainIndices,currentDomainIndices,aContDomain},
	(* define domains except the last domain in a step *)
	If[Subscript[domainConfig, name]["indexInStep"]!=Subscript[domainConfig, name]["numDomainsInStep"],
	
		nextDomainOutputs = Cases[Subscript[outputName, name]
			/.PositionIndex[Subscript[outputName, nextDomain]],_Integer,{2}];
		currentDomainOutputs = Cases[Subscript[outputName, nextDomain]
			/.PositionIndex[Subscript[outputName, name]],_Integer,{2}];
		nextDomainIndices = Flatten[
			Table[(i-1)*Subscript[nOutputRD2, nextDomain]+1+(j-1),{i,Subscript[nParamRD2, nextDomain]},
			{j,nextDomainOutputs}]];
		currentDomainIndices = Flatten[
			Table[(i-1)*Subscript[nOutputRD2, name]+1+(j-1),{i,Subscript[nParamRD2, name]},
			{j,currentDomainOutputs}]];
		aContDomain = \[ScriptCapitalA][[currentDomainIndices,1]] - \[ScriptCapitalA]n[[nextDomainIndices,1]];
		ExportWithGradient["aContDomain_"<>nameStr,aContDomain,{\[ScriptCapitalA],\[ScriptCapitalA]n}];
	];
];


(* ::Subsection:: *)
(*Reset Map*)


footXRe = Simplify[Subscript[h, ToExpression["FootPosX"]]]-Qe[[1]];
footZRe = Simplify[Subscript[h, ToExpression["FootPosZ"]]]-Qe[[2]];
Simplify[Sqrt[footXRe^2+footZRe^2]];


If[domainTrans["relabel"],
	(* relabeling matrix *)
(*	newPx = -Sin[Qe[[3]] + Qe[[4]]/2 + Qe[[5]]/2]*((28*Cos[Qe[[4]]/2 - Qe[[5]]/2])/125 + (143*Cos[Qe[[4]]/2 - Qe[[5]]/2 + Qe[[6]]])/250);
	newQe = {newPx, Qe[[2]], Qe[[3]], Qe[[4]], Qe[[5]], Qe[[6]], Qe[[7]]};*)
	footX = Subscript[h, ToExpression["FootPosX"]];
	newQe = {Qe[[1]]-footX, Qe[[2]], Qe[[3]], Qe[[4]], Qe[[5]], Qe[[6]], Qe[[7]]};
	qResetMap = newQe - Qn;
	,
	qResetMap = Qe - Qn;
];
(*qResetMap = RelabelMatrix.Qe - Qn;*)
ExportWithGradient["qResetMap_"<>nameStr,qResetMap,{Qe,Qn}];


If[domainTrans["impact"],
	Subscript[Jimp, name]=Table[Flatten[Subscript[Jh, constr]], 
                           {constr,ToExpression[domainTrans["constraints"]]}];
	(*If[domainTrans["relabel"],
		(*dQnsf = Join[(Join[{Subscript[Jh, NonstanceFootPosX]},{Subscript[Jh, NonstanceFootPosZ]},1]/.Table[Qe[[i,1]]->Qn[[i,1]],{i,ndof}]).dQn,ConstantArray[0,{ndof-2,1}]];*)
		dQnsf = Join[(Join[{Subscript[Jh, FootPosX]},{Subscript[Jh, FootPosZ]},1]/.Table[Qe[[i,1]]->Qn[[i,1]],{i,ndof}]).dQn, ConstantArray[0,{ndof-2,1}]];
		,
		dQnsf = ConstantArray[0,{ndof,1}];
	];*)
	\[ScriptCapitalF]imp = Vec[Table[\[ScriptF]i[i],{i,Length[domainTrans["constraints"]]}]];
	dqResetMap = Join[\[ScriptCapitalD]e.((dQn)-dQe)-Subscript[Jimp, name]\[Transpose].\[ScriptCapitalF]imp,
			          Subscript[Jimp, name].(dQn)];
	If[domainTrans["relabel"],
		ExportWithGradient["dqResetMap_"<>nameStr,dqResetMap,{Qe,dQe,\[ScriptCapitalF]imp,Qn,dQn}];
		,
		ExportWithGradient["dqResetMap_"<>nameStr,dqResetMap,{Qe,dQe,\[ScriptCapitalF]imp,dQn}];
	];
	,
	dqResetMap = dQn - dQe;
	ExportWithGradient["dqResetMap_"<>nameStr,dqResetMap,{dQe,dQn}]
];


(* ::Subsection:: *)
(*Foot Clearance for fly phase.*)
(*problem: this only works for tau is from 0 to 1*)


Block[{FootClearance,footZ,footX,hfoot,htsubs,footHeight},
If[KeyExistsQ[constraints,"FootClearance"],
	FootClearance=constraints["FootClearance"]//First;
	(*footX=Subscript[h, ToExpression[FootClearance["point"]<>"PosX"]];*)
	footZ = Subscript[h, ToExpression[FootClearance["point"]<>"PosZ"]];
	If[optOptions["TimeBased"],
		vars = RemoveTimeDependence[{Qe}];
		footHeight={a*(-2 hmax \[Sigma][t]^2+2 hmax \[Sigma][t])*Exp[ep*\[Sigma][t]]+h0-RemoveTimeDependence[footZ]/.{\[Sigma][t]->t}/.nodetimesubs/.{tf->1}};
		ExportWithGradient["footClearance_"<>nameStr,footHeight,vars,{k,nNode,a,ep,hmax,h0}];
		,
        htsubs={\[Sigma][t]->((Qe[[1]]-\[ScriptCapitalP][[3]])/(\[ScriptCapitalP][[1]] - \[ScriptCapitalP][[3]])) };
		(*htsubs={\[Sigma][t]->Subscript[tau,name]};*)
		footHeight={a*(-2 hmax \[Sigma][t]^2+2 hmax \[Sigma][t])*Exp[ep*\[Sigma][t]]+h0-footZ/.htsubs};
		(*footHeight = {-footZ};*)
		ExportWithGradient["footClearance_"<>nameStr,footHeight,{Qe,\[ScriptCapitalP]},{a,ep,hmax,h0}];
	];
];
];


(* ::Subsection:: *)
(*Output Boundary*)


Block[
	{outputsConstr,outputIndices,yConstr},
	If[KeyExistsQ[constraints,"outputs"],
		outputsConstr = constraints["outputs"];
		outputIndices=Flatten@(#["name"]/.PositionIndex[Subscript[outputName, name]]&/@outputsConstr);
		If[optOptions["TimeBased"],
			yConstr=Flatten[Subscript[ya2, name][[outputIndices]]];
			ExportWithGradient["outputBoundary_"<>nameStr,yConstr,{Qe}];
			,
			yConstr=Flatten[Subscript[yd2, name][[outputIndices]]/.{\[Sigma][t]->Subscript[tau, name]}];
			ExportWithGradient["outputBoundary_"<>nameStr,yConstr,{Qe,\[ScriptCapitalP],\[ScriptCapitalA]}];
		];
	];
];


(* ::Subsection:: *)
(*ZMP Constraints*)


Block[{lt,lh,wf,zmpConstrFeet,nZMPFeet,zmp,xIndex,yIndex,zIndex,rIndex,pIndex,optConstr,mu},
If[KeyExistsQ[constraints,"ZMP"],
	optConstr=StringReplace[Subscript[domainConfig, name]["constraints"],{"CartX"->"Roll","CartY"->"Pitch","CartZ"->"Yaw"}];
	mu= 0.6;
	lt=kinConsts["lt"];
	lh=kinConsts["lh"];
	wf=kinConsts["wf"];
	(*wf = 0.06;*)
	zmpConstrFeet=First[constraints["ZMP"]]["feet"];
	nZMPFeet=Length[zmpConstrFeet];
	If[!nZMPFeet==0,
		zmp=Flatten[
		Table[
			xIndex=First@(zmpConstrFeet[[i]]<>"PosX"/.PositionIndex[optConstr]);
			yIndex=First@(zmpConstrFeet[[i]]<>"PosY"/.PositionIndex[optConstr]);
			zIndex=First@(zmpConstrFeet[[i]]<>"PosZ"/.PositionIndex[optConstr]);
			rIndex=First@(zmpConstrFeet[[i]]<>"Roll"/.PositionIndex[optConstr]);
			pIndex=First@(zmpConstrFeet[[i]]<>"Pitch"/.PositionIndex[optConstr]);
			If[Not[NumberQ[pIndex]],
				zmpConstrFeet[[i]]=zmpConstrFeet[[i]]<>"RollOnly";
			];
			Switch[zmpConstrFeet[[i]],
				"RightFoot",
				{
					(lt+lh) \[ScriptCapitalF]e[[pIndex]]-lt \[ScriptCapitalF]e[[zIndex]],
					-(lt+lh) \[ScriptCapitalF]e[[pIndex]]-lh \[ScriptCapitalF]e[[zIndex]],
					wf \[ScriptCapitalF]e[[rIndex]]-(wf/2) \[ScriptCapitalF]e[[zIndex]],
					-wf \[ScriptCapitalF]e[[rIndex]]-(wf/2) \[ScriptCapitalF]e[[zIndex]],
					\[ScriptCapitalF]e[[xIndex]]-(mu/Sqrt[2]) \[ScriptCapitalF]e[[zIndex]],
					-\[ScriptCapitalF]e[[xIndex]]-(mu/Sqrt[2]) \[ScriptCapitalF]e[[zIndex]],
					\[ScriptCapitalF]e[[yIndex]]-(mu/Sqrt[2]) \[ScriptCapitalF]e[[zIndex]],
					-\[ScriptCapitalF]e[[yIndex]]-(mu/Sqrt[2]) \[ScriptCapitalF]e[[zIndex]]
				},
				"LeftFoot",
				{
					(lt+lh) \[ScriptCapitalF]e[[pIndex]]-lt \[ScriptCapitalF]e[[zIndex]],
					-(lt+lh) \[ScriptCapitalF]e[[pIndex]]-lh \[ScriptCapitalF]e[[zIndex]],
					wf \[ScriptCapitalF]e[[rIndex]]-(wf/2) \[ScriptCapitalF]e[[zIndex]],
					-wf \[ScriptCapitalF]e[[rIndex]]-(wf/2) \[ScriptCapitalF]e[[zIndex]],
					\[ScriptCapitalF]e[[xIndex]]-(mu/Sqrt[2]) \[ScriptCapitalF]e[[zIndex]],
					-\[ScriptCapitalF]e[[xIndex]]-(mu/Sqrt[2]) \[ScriptCapitalF]e[[zIndex]],
					\[ScriptCapitalF]e[[yIndex]]-(mu/Sqrt[2]) \[ScriptCapitalF]e[[zIndex]],
					-\[ScriptCapitalF]e[[yIndex]]-(mu/Sqrt[2]) \[ScriptCapitalF]e[[zIndex]]
				},
				"RightToe",
				{
					(lh+lt) \[ScriptCapitalF]e[[pIndex]]-0 \[ScriptCapitalF]e[[zIndex]],
					-(lh+lt) \[ScriptCapitalF]e[[pIndex]]-(lh+lt) \[ScriptCapitalF]e[[zIndex]],
					wf \[ScriptCapitalF]e[[rIndex]]-(wf/2) \[ScriptCapitalF]e[[zIndex]],
					-wf \[ScriptCapitalF]e[[rIndex]]-(wf/2) \[ScriptCapitalF]e[[zIndex]],
					\[ScriptCapitalF]e[[xIndex]]-(mu/Sqrt[2]) \[ScriptCapitalF]e[[zIndex]],
					-\[ScriptCapitalF]e[[xIndex]]-(mu/Sqrt[2]) \[ScriptCapitalF]e[[zIndex]],
					\[ScriptCapitalF]e[[yIndex]]-(mu/Sqrt[2]) \[ScriptCapitalF]e[[zIndex]],
					-\[ScriptCapitalF]e[[yIndex]]-(mu/Sqrt[2]) \[ScriptCapitalF]e[[zIndex]]
				},
				"RightHeel",
				{
					(lh+lt) \[ScriptCapitalF]e[[pIndex]]-(lh+lt) \[ScriptCapitalF]e[[zIndex]],
					-(lh+lt) \[ScriptCapitalF]e[[pIndex]]- 0 \[ScriptCapitalF]e[[zIndex]],				
					wf \[ScriptCapitalF]e[[rIndex]]-(wf/2) \[ScriptCapitalF]e[[zIndex]],
					-wf \[ScriptCapitalF]e[[rIndex]]-(wf/2) \[ScriptCapitalF]e[[zIndex]],
					\[ScriptCapitalF]e[[xIndex]]-(mu/Sqrt[2]) \[ScriptCapitalF]e[[zIndex]],
					-\[ScriptCapitalF]e[[xIndex]]-(mu/Sqrt[2]) \[ScriptCapitalF]e[[zIndex]],
					\[ScriptCapitalF]e[[yIndex]]-(mu/Sqrt[2]) \[ScriptCapitalF]e[[zIndex]],
					-\[ScriptCapitalF]e[[yIndex]]-(mu/Sqrt[2]) \[ScriptCapitalF]e[[zIndex]]
				},
				"LeftToe",
				{
					(lh+lt) \[ScriptCapitalF]e[[pIndex]]-0 \[ScriptCapitalF]e[[zIndex]],
					-(lh+lt) \[ScriptCapitalF]e[[pIndex]]-(lh+lt) \[ScriptCapitalF]e[[zIndex]],
					wf \[ScriptCapitalF]e[[rIndex]]-(wf/2) \[ScriptCapitalF]e[[zIndex]],
					-wf \[ScriptCapitalF]e[[rIndex]]-(wf/2) \[ScriptCapitalF]e[[zIndex]],
					\[ScriptCapitalF]e[[xIndex]]-(mu/Sqrt[2]) \[ScriptCapitalF]e[[zIndex]],
					-\[ScriptCapitalF]e[[xIndex]]-(mu/Sqrt[2]) \[ScriptCapitalF]e[[zIndex]],
					\[ScriptCapitalF]e[[yIndex]]-(mu/Sqrt[2]) \[ScriptCapitalF]e[[zIndex]],
					-\[ScriptCapitalF]e[[yIndex]]-(mu/Sqrt[2]) \[ScriptCapitalF]e[[zIndex]]
				},
				"LeftHeel",
				{
					(lh+lt) \[ScriptCapitalF]e[[pIndex]]-(lh+lt) \[ScriptCapitalF]e[[zIndex]],
					-(lh+lt) \[ScriptCapitalF]e[[pIndex]]- 0 \[ScriptCapitalF]e[[zIndex]],				
					wf \[ScriptCapitalF]e[[rIndex]]-(wf/2) \[ScriptCapitalF]e[[zIndex]],
					-wf \[ScriptCapitalF]e[[rIndex]]-(wf/2) \[ScriptCapitalF]e[[zIndex]],
					\[ScriptCapitalF]e[[xIndex]]-(mu/Sqrt[2]) \[ScriptCapitalF]e[[zIndex]],
					-\[ScriptCapitalF]e[[xIndex]]-(mu/Sqrt[2]) \[ScriptCapitalF]e[[zIndex]],
					\[ScriptCapitalF]e[[yIndex]]-(mu/Sqrt[2]) \[ScriptCapitalF]e[[zIndex]],
					-\[ScriptCapitalF]e[[yIndex]]-(mu/Sqrt[2]) \[ScriptCapitalF]e[[zIndex]]
				},
				"LeftToeRollOnly",
				{
					wf \[ScriptCapitalF]e[[rIndex]]-(wf/2) \[ScriptCapitalF]e[[zIndex]],
					-wf \[ScriptCapitalF]e[[rIndex]]-(wf/2) \[ScriptCapitalF]e[[zIndex]],
					\[ScriptCapitalF]e[[xIndex]]-(mu/Sqrt[2]) \[ScriptCapitalF]e[[zIndex]],
					-\[ScriptCapitalF]e[[xIndex]]-(mu/Sqrt[2]) \[ScriptCapitalF]e[[zIndex]],
					\[ScriptCapitalF]e[[yIndex]]-(mu/Sqrt[2]) \[ScriptCapitalF]e[[zIndex]],
					-\[ScriptCapitalF]e[[yIndex]]-(mu/Sqrt[2]) \[ScriptCapitalF]e[[zIndex]]
				},
				"LeftHeelRollOnly",
				{			
					wf \[ScriptCapitalF]e[[rIndex]]-(wf/2) \[ScriptCapitalF]e[[zIndex]],
					-wf \[ScriptCapitalF]e[[rIndex]]-(wf/2) \[ScriptCapitalF]e[[zIndex]],
					\[ScriptCapitalF]e[[xIndex]]-(mu/Sqrt[2]) \[ScriptCapitalF]e[[zIndex]],
					-\[ScriptCapitalF]e[[xIndex]]-(mu/Sqrt[2]) \[ScriptCapitalF]e[[zIndex]],
					\[ScriptCapitalF]e[[yIndex]]-(mu/Sqrt[2]) \[ScriptCapitalF]e[[zIndex]],
					-\[ScriptCapitalF]e[[yIndex]]-(mu/Sqrt[2]) \[ScriptCapitalF]e[[zIndex]]
				}
			]
			,
			{i,nZMPFeet}
			]
		];
	ExportWithGradient["zmp_"<>nameStr,zmp,{\[ScriptCapitalF]e}];
	];
];
];		


(* ::Subsection:: *)
(*Position of non-stance foot for stepping ?*)


Block[{pos,posConstr},
	If[KeyExistsQ[constraints,"position"],
		posConstr = constraints["position"];
		pos=Flatten@(Subscript[h,ToExpression[#["name"]]]&/@posConstr);
		ExportWithGradient["posConstr_"<>nameStr,pos,{Qe}];		
	];
];



(* ::Subsection:: *)
(*Hip velocity constraints (NOT w.r.t. stance foot)*)


Block[{vel},
	If[KeyExistsQ[constraints,"hipVelocity"],
		(*vel = Flatten[Subscript[Jh, StanceHipPosX].dQe-Subscript[Jh, StanceFootPosX].dQe];*)
		vel = Flatten[Subscript[Jh, HipPosX].dQe];
		ExportWithGradient["hipVelocity_"<>nameStr,vel,{Qe,dQe}];
	];
];


(* ::Subsection:: *)
(*Time Equality*)


timeCont = {tf - tfn};
ExportWithGradient["timeCont_"<>nameStr,timeCont,{tf,tfn}];


(* ::Subsection:: *)
(*Center of mass position*)


(*pcomY=Subscript[pe, com][[2,1]];
ycomConstr = {pcomY-Subscript[h, RightFootInsidePosY],-pcomY+Subscript[h, RightFootOutsidePosY]};
ExportWithGradient["ycom_"<>nameStr,ycomConstr,{Qe}];*)


(* ::Subsection:: *)
(*Human Data Constraints*)


Subscript[a, nsl]={0.34218705046351877`,0.5362955697970678`,0.3566997280982229`,0.9206636288543293`,-0.14363402740628678`,-0.30882080628868447`,-0.31964559650843544`};
Subscript[a, sk]={0.15901259708504004`,0.34424912475260394`,0.665139511046032`,0.33430064748911115`,0.21729300253041672`,0.2523773795844753`,0.37435331712961306`};
Subscript[a, nsk]={0.3060536050572564`,0.6260416127544512`,0.3491828379073713`,2.1938081504830595`,1.2392935750130842`,-0.07048416712730382`,0.14519658478300618`};
Subscript[a, tor]={0.05843667802188775`,0.08475113671268372`,0.05523653095462769`,0.12795186670790945`,0.06833817102488045`,0.06177896465035895`,0.07181067917421707`};
Subscript[a, sa]={-0.14920813957136284`,-0.1715287742130171`,-0.18908805902530146`,-0.18290306199752826`,-0.19511699287067638`,-0.21211500771732356`,-0.19893547387690264`};
Subscript[a, sh]={-0.10250897219813247`,-0.14515933473500628`,-0.17238675283055335`,-0.31461149088135304`,0.09611375849302596`,-0.19488787029858018`,-0.13908170674844206`};
Subscript[a, nsh]={-0.22168405438543154`,-0.2196659150122761`,-0.045966457474819915`,-0.14337122201274`,-0.31075325557231537`,-0.12304737708563528`,-0.17531629103121557`};


(*actualHumanOutputs=Join[Subscript[ya2, StanceKnee],Subscript[ya2, NonstanceKnee],Subscript[ya2, DeltaNsSlope],Subscript[ya2, Torso]];*)
actualHumanOutputs=Join[Subscript[ya2, StanceKnee],Subscript[ya2, StanceHip], Subscript[ya2, NonstanceHip],Subscript[ya2, NonstanceKnee]]


skSubs=Table[a[i]->Subscript[a, sk][[i]],{i,7}];
shSubs=Table[a[i]->Subscript[a, sh][[i]],{i,7}];
nshSubs=Table[a[i]->Subscript[a, nsh][[i]],{i,7}];
nskSubs=Table[a[i]->Subscript[a, nsk][[i]],{i,7}];
nslSubs=Table[a[i]->Subscript[a, nsl][[i]],{i,7}];
torSubs=Table[a[i]->Subscript[a, tor][[i]],{i,7}];
desiredHumanOutputs=Join[
	Flatten@DesiredFunction["Bezier7thOrder"]/.skSubs,
	Flatten@DesiredFunction["Bezier7thOrder"]/.shSubs,
	Flatten@DesiredFunction["Bezier7thOrder"]/.nshSubs,
	Flatten@DesiredFunction["Bezier7thOrder"]/.nskSubs]/.{\[Sigma][t]->Subscript[tau, name]};
(*desiredHumanOutputs=Join[
	Flatten@DesiredFunction["Bezier7thOrder"]/.skSubs,
	Flatten@DesiredFunction["Bezier7thOrder"]/.nskSubs,
	Flatten@DesiredFunction["Bezier7thOrder"]/.nslSubs,
	Flatten@DesiredFunction["Bezier7thOrder"]/.torSubs]/.{\[Sigma][t]->Subscript[tau, name]};*)


humanOutputsConstr=actualHumanOutputs-desiredHumanOutputs;


(*Comment by W.Ma for less trouble for single leg hopper.*)
(*ExportWithGradient["humanData_"<>nameStr,Flatten[humanOutputsConstr],{Qe,\[ScriptCapitalP]}];*)


(*skIndex="RightKneePitch"/.PositionIndex@(Association@(Subscript[domainConfig, name]["outputs"]//First)["actual"])["degreeTwoOutput"];
nskIndex="LeftKneePitch"/.PositionIndex@(Association@(Subscript[domainConfig, name]["outputs"]//First)["actual"])["degreeTwoOutput"];
nslIndex="LeftLinNSlope"/.PositionIndex@(Association@(Subscript[domainConfig, name]["outputs"]//First)["actual"])["degreeTwoOutput"];
torIndex="RightTorsoPitch"/.PositionIndex@(Association@(Subscript[domainConfig, name]["outputs"]//First)["actual"])["degreeTwoOutput"];*)


(*Subscript[ya2, name][[skIndex]]-Flatten[DesiredFunction["Bezier7thOrder"][t]/.skSubs]
Subscript[ya2, name][[nskIndex]]-Flatten[DesiredFunction["Bezier7thOrder"][t]/.nskSubs]
Subscript[ya2, name][[nslIndex]]-Flatten[DesiredFunction["Bezier7thOrder"][t]/.nslSubs]
Subscript[ya2, name][[torIndex]]-Flatten[DesiredFunction["Bezier7thOrder"][t]/.skSubs]*)


(* ::Subsection:: *)
(*ZMP LIPM Constraints*)


If[optOptions["UseLIPMConstraints"],
	<<ZMPConstraint`;
];


(* ::Subsection:: *)
(*Impact Velocity*)


If[KeyExistsQ[constraints,"impactVelocity"],
	impactVel=Join[{Subscript[Jh, LeftFootPosX].dQe,Subscript[Jh, LeftFootPosY].dQe,Subscript[Jh, LeftFootPosZ].dQe}];
	ExportWithGradient["impactVel_"<>nameStr,Flatten[impactVel],{Qe,dQe}];
];


If[KeyExistsQ[constraints,"impactVelocity2D"],
	impactVel=Join[{Subscript[Jh, FootPosX].dQe,Subscript[Jh, FootPosZ].dQe}];
	ExportWithGradient["impactVel2D_"<>nameStr,Flatten[impactVel],{Qe,dQe}];
];


(* ::Section:: *)
(*Objective Function*)


grav=9.81;


ObjectiveFunc["power"]:=Sqrt[Transpose[\[ScriptCapitalU]*Subscript[\[ScriptCapitalB]e, name]\[Transpose].dQe].(\[ScriptCapitalU]*Subscript[\[ScriptCapitalB]e, name]\[Transpose].dQe)]*tf*(Cos[Pi (k-1)/(nNode-1)]-Cos[Pi k/(nNode-1)])/(2*(tf))//First;
ObjectiveFunc["ZMP"]:=Sqrt[Transpose[comDiff].comDiff];
ObjectiveFunc["HumanData"]:=Sqrt[Transpose[humanOutputsConstr].humanOutputsConstr];
ObjectiveFunc["torque"]:=Sqrt[Transpose[\[ScriptCapitalU]].\[ScriptCapitalU]];
ObjectiveFunc["zero"]:={{0}};
ObjectiveFunc["scot"]:=(Sqrt[Transpose[\[ScriptCapitalU]*Subscript[\[ScriptCapitalB]e, name]\[Transpose].dQe].(\[ScriptCapitalU]*Subscript[\[ScriptCapitalB]e, name]\[Transpose].dQe)]*tf*(Cos[Pi (k-1)/(nNode-1)]-Cos[Pi k/(nNode-1)])/(2*(tf))//First)/(mass*grav*\[ScriptH][1]);


cost=ObjectiveFunc["scot"];
ExportWithGradient["scotCost_"<>nameStr,Flatten[cost],{tf,dQe,\[ScriptCapitalU],\[ScriptH]},{k,nNode}];


cost=ObjectiveFunc["power"];
ExportWithGradient["powerCost_"<>nameStr,Flatten[cost],{tf,dQe,\[ScriptCapitalU]},{k,nNode}];
cost=ObjectiveFunc["HumanData"];
(*ExportWithGradient["humanCost_"<>nameStr,Flatten[cost],{Qe,\[ScriptCapitalP]}];*)(*comment by W.Ma for less trouble of single leg hopper*)
cost=ObjectiveFunc["torque"];
ExportWithGradient["torqueCost_"<>nameStr,Flatten[cost],{\[ScriptCapitalU]}];
cost=ObjectiveFunc["zero"];
ExportWithGradient["zeroCost_"<>nameStr,Flatten[cost],{\[ScriptCapitalU]}];
cost=ObjectiveFunc["scot"];
ExportWithGradient["scotCost_"<>nameStr,Flatten[cost],{tf,dQe,\[ScriptCapitalU],\[ScriptCapitalH]},{k,nNode}];

If[optOptions["UseLIPMConstraints"],
	cost=ObjectiveFunc["ZMP"];
	ExportWithGradient["zmpCost_"<>nameStr,Flatten[cost],{Qe,Xc,Subscript[z, 0]}];
];


(*cost=ObjectiveFunc[optOptions["ObjectiveType"]];
Switch[optOptions["ObjectiveType"],
	"power",
		ExportWithGradient["powerCost_"<>nameStr,Flatten[cost],{tf,dQe,\[ScriptCapitalU]},{k,nNode}];
	,
	"ZMP",
		ExportWithGradient["zmpCost_"<>nameStr,Flatten[cost],{Qe,Xc,Subscript[z, 0]}];
	,
	"HumanData",
		ExportWithGradient["humanCost_"<>nameStr,Flatten[cost],{Qe,\[ScriptCapitalP]}];
	,
	"torque",
		ExportWithGradient["torqueCost_"<>nameStr,Flatten[cost],{\[ScriptCapitalU]}];
	];*)
