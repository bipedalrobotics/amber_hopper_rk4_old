(* ::Package:: *)

(* ::Title:: *)
(*Durus Behavior*)


(* ::Section:: *)
(*Load configuration*)


domains=robot["domains"];


(* ::Section:: *)
(*Build domain expressions*)


Block[
	{domainConfigFile,name,constraints,guardStruct,guardType,guardName,guardCond,outputs,
	 actual,actualRD1,actualRD2,phaseVar,paramsSubs,desired,qaIndices,actuatedJoints,Be,resetMapStruct,
	 ikConfig,ikJoints,ikConstraints,ikRemaining,ikRemainingJ,optConstr,posHip},
	Table[
		(* Load default domain configuration file *)
		domainConfigFile=FileNameJoin[{$DurusConfigPath,"domain",domain["name"]<>".yaml"}];
		
		(* Extract default domain configurations *)
		name=ToExpression[domain["name"]];
		Subscript[domainConfig, name]=Apply[Association,Association@SnakeYaml`YamlReadFile[domainConfigFile],{2}];
		

		(* holonomic constraints *)
		constraints=ToExpression[Subscript[domainConfig, name]["constraints"]];
		Subscript[hol, name]=Table[Flatten[Subscript[h, constr]],{constr,constraints}];
		Subscript[Jhol, name]=Table[Flatten[Subscript[Jh, constr]],{constr,constraints}];
		Subscript[dJhol, name]=Table[Flatten[Subscript[dJh, constr]],{constr,constraints}];
		Subscript[nHolConstr, name]=Length[constraints];

		(* holonomic constraints using angular velocity, used in optimizatin to make sure to have correct ZMP 
			constraints forces*)
		(*
		optConstr=ToExpression@StringReplace[Subscript[domainConfig, name]["constraints"],{"CartX"->"Roll","CartY"->"Pitch","CartZ"->"Yaw"}];
		
		Subscript[opthol, name]=Table[Flatten[Subscript[h, constr]],{constr,optConstr}];
		Subscript[Jopthol, name]=Table[Flatten[Subscript[Jh, constr]],{constr,optConstr}];
		Subscript[dJopthol, name]=Table[Flatten[Subscript[dJh, constr]],{constr,optConstr}];
		*)
		(* guard condition *)
		guardStruct = Subscript[domainConfig, name]["guard"]//First;
		guardType   = guardStruct["type"];
		guardName   = guardStruct["name"];
		If[MemberQ[definedPositions,guardName],
			guardCond=ToExpression[guardName];
			Subscript[guard, name]={Subscript[h, guardCond]};
			Subscript[Jguard, name]=Subscript[Jh, guardCond];
			,
			Subscript[guard, name]={1};
			Subscript[Jguard, name] = ConstantArray[0,{1,ndof}];
		];

		resetMapStruct = Subscript[domainConfig, name]["resetMap"]//First;
		If[KeyExistsQ[resetMapStruct,"constraints"],
			constraints=ToExpression[resetMapStruct["constraints"]];
			Subscript[Jimp, name]=Table[Flatten[Subscript[Jh, constr]],{constr,constraints}];
			,
			Subscript[Jimp, name]={};
		];
		(* actual non-linear hip position and velocity w.r.t. the origin*)
		posHip = ToExpression[(Subscript[domainConfig,name]["phip"]//First)["pos"]];
		(*Subscript[phip, name] = Subscript[h, posHip[[1]]]-Subscript[h, posHip[[2]]];*)
		Subscript[phip, name] = Subscript[h, posHip[[1]]];
		Subscript[vhip, name] = D[Subscript[phip, name],t];

		(* default controller definition *)
		outputs=Subscript[domainConfig, name]["outputs"]//First;
		(* new controller definition in behavior config file, if field exists *)
		(* then overwrite the default definition. *)
		(* controllerNew=domain["controller"]//First//Association; *)

		(* actual outputs *)
		actual=outputs["actual"]//Association;
		Subscript[outputName, name]=actual["degreeTwoOutput"];
		actualRD1=ToExpression[actual["degreeOneOutput"]];
		actualRD2=ToExpression[actual["degreeTwoOutput"]];
		If[EmptyQ[{actualRD1}],
			Subscript[ya1, name]={};
			Subscript[Dya1, name]={}
			,
			Subscript[ya1, name]=Subscript[ya1, actualRD1];
			Subscript[Dya1, name]=Subscript[Dya1, actualRD1]
		];
		
		Subscript[ya2, name]=Flatten[Table[Subscript[ya2, output],{output,actualRD2}]];
		Subscript[Dya2, name]=Table[Flatten[Subscript[Dya2, output]],{output,actualRD2}];
		Subscript[DLfya2, name]=Table[Flatten[Subscript[DLfya2, output]],{output,actualRD2}];
		
		Subscript[nOutputRD2, name]=Length[actualRD2];
		(* phase variables *)
		
		phaseVar=ToExpression[outputs["phaseVariable"]];
		Subscript[deltaphip, name]=Subscript[\[Delta]p, phaseVar];
		Subscript[Jdeltaphip, name]=Subscript[J\[Delta]p, phaseVar];
        (* if tau starts from 0 for both domains.
		If[StringMatchQ[domain["name"],"SingleSupport"],
			Subscript[tau, name]=(Subscript[\[Delta]p, phaseVar]-p[2])/(p[3]-p[2]),
			Subscript[tau, name]=(Subscript[\[Delta]p, phaseVar]-p[3])/(p[1]-p[3])
		];*)
		Subscript[tau, name]=(Subscript[\[Delta]p, phaseVar]-p[2])/(p[1]-p[2]);
		Subscript[dtau, name]=D[Subscript[tau, name],t];
		Subscript[Jtau, name]=J[Subscript[tau, name],\[ScriptCapitalX]];
		Subscript[Jdtau, name]=J[Subscript[dtau, name],\[ScriptCapitalX]];

		(* desired outputs *)
		desired=outputs["desired"]//Association;
		(* desiredNew=controllerNew["desired"]//Association; *)

		(* If desired behaviors are re-defined in behavior configuration, then *)
		(* overwrite the default behavior defined in domain configuration. *)
		(*
		If[KeyExistsQ[desiredNew,"degreeOneOutput"],
			If[!EmptyQ[{desiredNew["degreeOneOutput"]}],
				desired["degreeOneOutput"]=desiredNew["degreeOneOutput"];
			];
		];
		If[KeyExistsQ[desiredNew,"degreeTwoOutput"],
			If[!EmptyQ[{desiredNew["degreeTwoOutput"]}],
				desired["degreeTwoOutput"]=desiredNew["degreeTwoOutput"];
			];
		];
		*)
		Subscript[nParamPhaseVar, name] = Subscript[domainConfig, name]["numDomainsInStep"] + 1;
		If[EmptyQ[{desired["degreeOneOutput"]}],
			Subscript[nParamRD1, name] = 0;
			Subscript[yd1, name]= {};
			Subscript[dyd1, name]= {}
			,
			Subscript[nParamRD1, name] = NumOfParams[desired["degreeOneOutput"]];
			(*relative degree one output*)
			paramsSubs=Table[a[j]->v[j],{j,Subscript[nParamRD1, name]}];	
			Subscript[yd1, name]=DesiredFunction[desired["degreeOneOutput"]]/.paramsSubs;
			Subscript[dyd1, name]=D[Subscript[yd1, name],\[Sigma][t]]
		];
		Subscript[nParamRD2, name] = NumOfParams[desired["degreeTwoOutput"]];
		
		
	
		Subscript[yd2, name]=Table[
			paramsSubs=Table[a[j]->a[Subscript[nOutputRD2, name](j-1)+i],{j,Subscript[nParamRD2, name]}];
			DesiredFunction[desired["degreeTwoOutput"]]/.paramsSubs
			,
			{i,Subscript[nOutputRD2, name]}
		];
		Subscript[dyd2, name]=D[Subscript[yd2, name],\[Sigma][t]];
		Subscript[ddyd2, name]=D[Subscript[yd2, name],{\[Sigma][t],2}];	
	
		(*
		Subscript[Dyd1, name]={D[Subscript[yd1, name],\[Sigma][t]].Subscript[Jtau, name]}; (*function of tau,a,v,p,q(if tau if nonlinear function of qe)*)
		Subscript[Dyd2, name]=D[Subscript[yd2, name],\[Sigma][t]].Subscript[Jtau, name]; (*function of tau,a,v,p,q(if tau if nonlinear function of qe)*)
		(*function of tau,dtau,a,v,p,q(if tau if nonlinear function of qe)*)
		Subscript[DLfyd2, name]=({D[Subscript[yd2, name],{\[Sigma][t],2}].Subscript[dtau, name]}//Transpose).Subscript[Jtau, name]+D[Subscript[yd2, name],\[Sigma][t]].Subscript[Jdtau, name];		
		*)

		actuatedJoints = outputs["actuatedJoints"];
		qaIndices = Flatten[actuatedJoints/.dofsIndex];
		Subscript[nAct, name]=Length[qaIndices];
		Be=ConstantArray[0,{ndof,Subscript[nAct, name]}];
		Be[[qaIndices,;;]]=IdentityMatrix[Subscript[nAct, name]];
		Subscript[\[ScriptCapitalB]e, name]=Be;
		(*
		ikConfig=outputs["nonstanceIk"]//Association;
		ikJoints=Flatten[ikConfig["joints"]/.dofsIndex];
		ikConstraints=ToExpression[ikConfig["constraints"]];

		ikRemaining=Select[Qe,!MemberQ[Qe[[ikJoints]],#]&]; (* Get remaining dofs not covered by this IK *)
		ikRemainingJ=D[Flatten[ikRemaining], {Flatten[Qe], 1}]; (* Get remaining dofs not covered by this IK *)
		*)
		(* Active *)
		(*
		Subscript[ikActive, name]=Table[Flatten[Subscript[h, constr]],{constr,ikConstraints}];
		Subscript[JikActive, name]=Table[Flatten[Subscript[Jh, constr]],{constr,ikConstraints}];
		(* Inactive *)
		Subscript[ikInactive, name]=ikRemaining;
		Subscript[JikInactive, name]=ikRemainingJ;
		*)
		,
		{domain,domains}
	];
];



