clear

%%% nsf of s.s.
a = 1;
ep = 0.1;
hmax = 0.06;
h0 = 0.015;
ph0 = 0.0;
phf = 1;

%%% sf of fly
% a = 0.5;
% ep = 0.1;
% hmax = 0.1;
% h0 = 0;
% ph0 = -0.03;
% phf = 0.8;

tau = 0 : 0.01 : 1;
for i = 1:length(tau)
    sigma = tau(i) * (phf-ph0)  +  ph0;
    
    hz(i) = h0 + a*exp(ep*sigma)*( 2*hmax*sigma - 2*hmax*sigma^2 );
end

figure(1)
plot(tau, hz, '-'); ylim([-0.1,0.08]); hold on
plot(tau, zeros(1,length(hz)), '--');
