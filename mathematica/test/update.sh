#!/bin/bash

# @brief Copy generated C++ code into proxi_cpp_redo
# @todo Figure out better sync mechanism?
# Check git status? Dirty?
main()
{
    cd "$(dirname "$BASH_SOURCE")"

    ./generate.py

    # Check git status?

    pkg=durus_3d_expr
    pkg_dir=$(rospack find durus_3d_expr)
    dest=$pkg_dir/gen
    # Remove files before hand?
    c_dir=$dest/src
    h_dir=$dest/include/$pkg
    mkdir -p $c_dir
    mkdir -p $h_dir
    cp *.cc $c_dir/
    cp *.hh $h_dir/
    cp *.cmake $dest

    cp ../../matlab/include/* $c_dir

    subfix=""
    if git diff-files; then
        subfix="-dirty"
    fi

    # From: git:786a89d:git-gui/GIT-VERSION-GEN:63
    dirty=$(sh -c 'git diff-index --name-only HEAD' 2>/dev/null) || dirty=
    case "$dirty" in
    '')
        subfix= ;;
    *)
        subfix=" [dirty]" ;;
    esac

    file=$dest/git-ref.txt
    echo "[ Git Ref: ]"
    echo $(git-ref ~+/gen)$subfix | tee $file
}

# Return relative path of $1 with respect to $2
# From: git@github.com:eacousineau/util.git:c48bf22:git-submodule-ext.sh:975
rel_path()
{
    test $# -eq 2 || { echo "Must supply two paths" >&2; return 1; }
    local target=$1
    # Add trailing slash, otherwise it won't be robust to common prefixes
    # that don't begin with a /
    local base=$2/

    while test "${target%%/*}" = "${base%%/*}"
    do
        target=${target#*/}
        base=${base#*/}
    done
    # Now chop off the trailing '/'s that were added in the beginning
    target=${target%/}
    base=${base%/}

    # Turn each leading "*/" component into "../", and strip trailing '/'s
    local rel=$(echo $base | sed -e 's|[^/][^/]*|..|g' | sed -e 's|*/+$||g')
    if test -n "$rel"
    then
        echo $rel/$target
    else
        echo $target
    fi
}

git-ref () 
{ 
    use_remote=;
    if [[ "$1" == "-r" ]]; then
        use_remote=1;
        shift;
    fi;
    local file="$1";
    repo="$(git rev-parse --show-toplevel)";
    rel="$(rel_path $file $repo)";
    if [[ -n "$use_remote" ]]; then
        name="$(git config remote.${REMOTE-origin}.url)";
    else
        name="$(basename $repo)";
    fi;
    echo "$name:$(git rev-parse --short HEAD):$rel"
}

main
