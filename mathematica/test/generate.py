#!/usr/bin/env python

import os
import subprocess

from durus_codegen.third import pyratemp

file_names = subprocess.check_output('ls -1 *.cc', shell=True).strip().split()

exprs = []
for file_name in file_names:
    expr = os.path.splitext(file_name)[0]
    exprs.append(expr)

data = {
    'name': 'all',
    'package': 'durus_3d_expr',
    'exprs': exprs
    }

def do_template(template, file_name):
    t = pyratemp.Template(template.lstrip())
    text = t(**data)
    with open(file_name, 'w') as f:
        f.write(text)


template = """
#ifndef $!package!$_$!name!$_HPP_
    #define $!package!$_$!name!$_HPP_

<!--(for expr in exprs)-->
#include <$!package!$/$!expr!$.hh>
<!--(end)-->

#endif // $!package!$_$!name!$_HPP_
"""
do_template(template, 'all.hh')


template = """
set(SRCS
<!--(for expr in exprs)-->
    gen/src/$!expr!$.cc
<!--(end)-->
)
"""
do_template(template, 'sources.cmake')
