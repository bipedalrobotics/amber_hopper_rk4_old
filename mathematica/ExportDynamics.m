(* ::Package:: *)

(* ::Title:: *)
(*Export Dynamics Expressions*)


(* ::Section:: *)
(*Export Expressions*)


statesubs=Dispatch@Join[
	((\[ScriptCapitalX][[#+1,1]]-> HoldForm@x[[#]]&)/@(Range[2*ndof]-1))];


SetOptions[CseWriteCpp,
	Directory->$DurusSimExportPath,
	ArgumentLists->{x},
	ArgumentDimensions-> {{2*ndof,1}},
	SubstitutionRules-> statesubs
];


exprs={
	{"De_mat",{\[ScriptCapitalD]e}},
	{"Ve_sca",{Ve}},
	{"Ce_mat",{\[ScriptCapitalC]e}},
	{"Ge_vec",{\[ScriptCapitalG]e}},
	{"Fr_vec",{\[ScriptCapitalF]r}}
	(*{"Fs_vec",{\[ScriptCapitalF]s}}*)
};


Block[{name,sym,expr},
	Table[
		{name,sym}=expr;
		CseWriteCpp[name,sym];
		,
		{expr,exprs}
	];
];//AbsoluteTiming



