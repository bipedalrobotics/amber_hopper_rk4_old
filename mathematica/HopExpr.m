(* ::Package:: *)

(* ::Title:: *)
(*ROBOT Model*)


(* ::Section:: *)
(*Initialization*)


SetDirectory[NotebookDirectory[]]
UtilsPath=FileNameJoin[{NotebookDirectory[],"utils"}]
$Path=DeleteDuplicates[Append[$Path,UtilsPath]];


Needs["Units`"];
Needs["RobotLinks`"];
Needs["ExtraUtil`"];
Needs["AmberShared`"];
Needs["SnakeYaml`"];
Needs["CseOptimization`"];


On[Assert];


YamlInit[];


(* configuration files path *)
$DurusConfigPath=FileNameJoin[{ParentDirectory[],"config"}];


(* ::Section:: *)
(*Load Behavior Configuration*)


$BehaviorName="Periodic2DRuning";


BehaviorConfigFile = FileNameJoin[{$DurusConfigPath,"behavior",$BehaviorName<>".yaml"}];
robot=Apply[Association,Association@SnakeYaml`YamlReadFile[BehaviorConfigFile],{2}];


(* exported files for simulation path *)
$DurusSimExportPath=FileNameJoin[{ParentDirectory[],"matlab","build_sim",$BehaviorName}];
EnsureDirectoryExists[$DurusSimExportPath];
(*
EnsureDirectoryExists[FileNameJoin[{$DurusSimExportPath,"src"}]]
EnsureDirectoryExists[FileNameJoin[{$DurusSimExportPath,"include"}]]
*)


SetOptions[CseWriteCpp,
	Behavior->$BehaviorName
];


(* ::Section:: *)
(*Robot Model*)


(*Execute this package before any other sub packages.*)
(*This package loads robot model configuration and compute forward kinematics*)
<<HopModel`;//AbsoluteTiming


(* ::Section:: *)
(*Robot Kinematics*)


(*This package build expressions for kinematics elements, *)
(*e.g. positions, constraints, jacobians *)
<<HopKinematics`;//AbsoluteTiming


(* ::Section:: *)
(*Robot Dynamics*)


(*This package build symbolic expressions of robot dynamics*)
<<HopDynamics`;//AbsoluteTiming


(* ::Section:: *)
(*Robot Outputs*)


<<HopOutputs`;//AbsoluteTiming


(* ::Section:: *)
(*Robot Behaviors*)


<<HopBehavior`;//AbsoluteTiming


(* ::Section:: *)
(*Export Expressions*)


<<ExportKinematics`;//AbsoluteTiming


<<ExportDynamics`;//AbsoluteTiming


<<ExportBehavior`;//AbsoluteTiming


(*SetDirectory[NotebookDirectory[]]
SetDirectory[FileNameJoin[{ParentDirectory[],"matlab"}]]
makeCommand="make -j4 sim"
Run["xterm -e \""<>makeCommand<>"\""]
SetDirectory[NotebookDirectory[]]
*)
