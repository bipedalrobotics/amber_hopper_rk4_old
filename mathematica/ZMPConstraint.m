(* ::Package:: *)

(* ::Section:: *)
(*Load domain specified informations*)


(* ::Subsection:: *)
(*Define Variables*)


Xc=Vec[{Subscript[x, c][t],Subscript[y, c][t]}];
Xz=Vec[{Subscript[x, z][t],Subscript[y, z][t]}];
Uz=Vec[{Subscript[u, x][t],Subscript[u, y][t]}];
dXc=D[Xc,t];
dXz=D[Xz,t];
ddXc=D[dXc,t];



Xcn=Xc/.Subscript[x_,y_]:>Subscript[x,{y,n}];
dXcn=D[Xcn,t];
ddXcn=D[dXcn,t];
Xcm=Xc/.Subscript[x_,y_]:>Subscript[x,{y,m}];
dXcm=D[Xcm,t];
ddXcm=D[dXcm,t];


Xzn=Xc/.Subscript[x_,y_]:>Subscript[x,{y,n}];
dXzn=D[Xzn,t];
Xzm=Xz/.Subscript[x_,y_]:>Subscript[x,{y,m}];
dXzm=D[Xzm,t];


grav = 9.81;


(* time interval *)
\[Delta]t=Switch[optOptions["NodeDistribution"],
	"CGL",tf (Cos[(Pi (k-1)/(nNode-1))]-Cos[(Pi/(nNode-1)) k ])/2,
	"Uniform",tf/(nNode-1),
	"Variable",tf
];


(* ::Subsection:: *)
(*Define Functions*)


GetNodeTimeSubs["CGL"]:={t->(-Cos[Pi (k-1)/(nNode-1)]tf+tf)/2};
GetNodeTimeSubs["Uniform"]:={t-> ((k-1)/(nNode-1))*tf};
GetNodeTimeSubs["Variable"]:=$Failed; (*no definition available*)
GetMidTimeSubs["CGL"]:={t->(-Cos[Pi (k-1)/(nNode-1)]tf-Cos[Pi (k)/(nNode-1)]tf+2tf)/4};
GetMidTimeSubs["CGL"]:={t-> ((k-1/2)/(nNode-1))*tf};
GetMidTimeSubs["Variable"]:=$Failed; (* no definition available *)


(* ::Section:: *)
(*ZMP Dynamics*)


(* ::Subsection:: *)
(*LIPM Dynamics*)


LipmDynamics=Join[
	{Derivative[2][Subscript[x, c]][t]-(mass*grav/Subscript[z, 0])*(Subscript[x, c][t]-Subscript[x, z][t])},
	{Derivative[2][Subscript[y, c]][t]-(mass*grav/Subscript[z, 0])*(Subscript[y, c][t]-Subscript[y, z][t])},
	{Derivative[1][Subscript[x, z]][t]-Subscript[u, x][t]},
	{Derivative[1][Subscript[y, z]][t]-Subscript[u, y][t]}];

ExportWithGradient["LipmDynamics_"<>nameStr,LipmDynamics,
		{Xc,ddXc,Xz,dXz,Uz,Subscript[z, 0]}];


(* ::Subsection:: *)
(*Integration Condition*)


Block[
	{IntPos,IntVel,IntZMP},
	Switch[optOptions["IntegrationScheme"],
		"Hermite-Simpson",
			(* position level *)
			IntPos=Xcn-Xc-\[Delta]t (dXcn+4dXcm+dXc)/6;
			ExportWithGradient["LipmIntPos_"<>nameStr,IntPos,
				{tf,Xc,dXc,dXcm,Xcn,dXcn},{k,nNode}];
			
			(* velocity level *)
			IntVel=dXcn-dXc-\[Delta]t (ddXcn+4ddXcm+ddXc)/6;
			ExportWithGradient["LipmIntVel_"<>nameStr,IntVel,
				{tf,dXc,ddXc,ddXcm,dXcn,ddXcn},{k,nNode}];
			
			(* zmp point *)
			IntZMP=Xzn-Xz-\[Delta]t (dXzn+4dXzm+dXz)/6;
			ExportWithGradient["LipmIntZMP_"<>nameStr,IntZMP,
				{tf,Xz,dXz,dXzm,Xzn,dXzn},{k,nNode}];

		,
		"Trapezoidal",
			(* position level *)
			IntPos=Xcn-Xc-\[Delta]t (dXcn+dXc)/2;
			ExportWithGradient["LipmIntPos_"<>nameStr,IntPos,
				{tf,Xc,dXc,Xcn,dXcn},{k,nNode}];
			(* velocity level *)
			IntVel=dXcn-dXc-\[Delta]t (ddXcn+ddXc)/2;
			ExportWithGradient["LipmIntVel_"<>nameStr,IntVel,
				{tf,dXc,ddXc,dXcn,ddXcn},{k,nNode}];
			(* zmp point *)
			IntZMP=Xzn-Xz-\[Delta]t (dXzn+dXz)/2;
			ExportWithGradient["LipmIntZMP_"<>nameStr,IntZMP,
				{tf,Xz,dXz,Xzn,dXzn},{k,nNode}];
		
	];
];


(* ::Subsection:: *)
(*Midpoint Constraints (if using hermite-Simpson)*)


Block[
	{MidPos,MidVel,MidZMP},
	If[optOptions["IntegrationScheme"] == "Hermite-Simpson",
		(* position level *)
		MidPos=Xcm-(Xc+Xcn)/2-\[Delta]t (dXc-dXcn)/8;
		ExportWithGradient["LipmMidPos_"<>nameStr,MidPos,
			{tf,Xc,dXc,Xcm,Xcn,dXcn},{k,nNode}];
		(* velocity level *)
		MidVel=dXcm-(dXc+dXcn)/2-\[Delta]t (ddXc-ddXcn)/8;
		ExportWithGradient["LipmMidVel_"<>nameStr,MidVel,
			{tf,dXc,ddXc,dXcm,dXcn,ddXcn},{k,nNode}];
		(* position level *)
		MidZMP=Xzm-(Xz+Xzn)/2-\[Delta]t (dXz-dXzn)/8;
		ExportWithGradient["LipmMidZMP_"<>nameStr,MidZMP,
			{tf,Xz,dXz,Xzm,Xzn,dXzn},{k,nNode}];
	];
];


(* ::Subsection:: *)
(*Center of Mass Position*)


comDiff=Subscript[pe, com]-Vec[{Subscript[x, c][t],Subscript[y, c][t],Subscript[z, 0]}];
ExportWithGradient["pcom_"<>nameStr,comDiff,
			{Qe,Xc,Subscript[z, 0]}];


(* ::Subsection:: *)
(*Center of Pressure Position*)


lt=footDimension["lt"];
lh=footDimension["lh"];
wf=footDimension["wf"];



GetCenterOfPressureConstraints["RightDS3DFlatWalking"]:=
	Flatten[Join[
		{-Subscript[x, z][t]-\[ScriptH][1]-lh},
		{Subscript[x, z][t]-lt},
		{-Subscript[y, z][t]-0.5*(\[ScriptH][2]+wf)},
		{Subscript[y, z][t]-0.5*(\[ScriptH][2]+wf)},
		{-Subscript[y, z][t]-0.5*(\[ScriptH][2]+wf)-(\[ScriptH][2]/\[ScriptH][1])*(Subscript[x, z][t]+lh)},		
		{Subscript[y, z][t]+0.5*(\[ScriptH][2]-wf)+(\[ScriptH][2]/\[ScriptH][1])*(Subscript[x, z][t]+lt)}]];
GetCenterOfPressureConstraints["RightSS3DFlatWalking"]:=
	Flatten[Join[
		{-Subscript[x, z][t]-lh},
		{Subscript[x, z][t]-lt},
		{-Subscript[y, z][t]-0.5*(\[ScriptH][2]+wf)},
		{Subscript[y, z][t]+0.5*(\[ScriptH][2]-wf)}]];
GetCenterOfPressureConstraints["RightSS3DStepping"]:=
	Flatten[Join[
		{-Subscript[x, z][t]-lh},
		{Subscript[x, z][t]-lt},
		{-Subscript[y, z][t]-0.5*(\[ScriptH][2]+wf)},
		{Subscript[y, z][t]+0.5*(\[ScriptH][2]-wf)}]];
GetCenterOfPressureConstraints["RightDS3DStepping"]:=
	Flatten[Join[
		{-Subscript[x, z][t]-\[ScriptH][1]-lh},
		{Subscript[x, z][t]-lt},
		{-Subscript[y, z][t]-0.5*(\[ScriptH][2]+wf)},
		{Subscript[y, z][t]-0.5*(\[ScriptH][2]+wf)}]];



copConstr = GetCenterOfPressureConstraints[domain["name"]];
ExportWithGradient["pcop_"<>nameStr,{copConstr},
			{Xz,\[ScriptCapitalH]}];


ExportWithGradient["z0const_"<>nameStr,{z0-z0n},{z0,z0n}];


ExportWithGradient["LipmCont_"<>nameStr,Flatten[Xc-Xcn],{Xc,Xcn}];
