(* ::Package:: *)

(* ::Title:: *)
(*Durus Dynamics Expression*)


(*This package build symbolic expressions of robot dynamics*)
(*The following functions will be exported:
  @Inertia Matrix - De
  @Corilios Matrix - Ce
  @Gravity Vector - Ge
  @Spring Force Vector - Fs
  @Friction Force Vector - Fr
  @Potential Energy - Ve
*)


(* ::Section:: *)
(*Generate Symbolic Expressions*)


(* ::Subsection:: *)
(*Mass & Inertia*)


MM=Block[
	{dof,mass,inertia},
	Table[
		mass=dof["mass"];
		inertia=dof["inertia"];
		BlockDiagonalMatrix[{I3*mass,inertia}]
		,
		{dof,dofs}
	]
];
masses=Block[{dof},Table[dof["mass"],{dof,dofs}]];
MMBoom=BlockDiagonalMatrix[{boom["mass"],boom["inertia"]}];


(* ::Subsection:: *)
(*Potential Energyp*)


grav = 9.81;
Ve={Sum[grav*masses[[i]]*Subscript[p, Subscript[sl, i]][\[Theta]][[3]],{i,ndof}]};
\[ScriptCapitalG]e=Vec[D[Ve,{Flatten[Qe],1}]];


(* ::Subsection:: *)
(*Inertia Matrix*)


\[ScriptCapitalD]eBody=Sum[Subscript[Je, Subscript[sl, i]]\[Transpose].MM[[i]].Subscript[Je, Subscript[sl, i]],{i,ndof}];
\[ScriptCapitalD]eBoom=Subscript[Je, b]\[Transpose].MMBoom.Subscript[Je, b];

\[ScriptCapitalD]eMotor=ConstantArray[0,{ndof,ndof}];
Block[{actuator},
	Table[
		actuator=Association@dofs[[i]]["actuator"];
		\[ScriptCapitalD]eMotor[[i,i]]=actuator["inertia"] * actuator["ratio"]^2 ;(*reflected inertia on joint side*)
		,
		{i,ndof}
	];
];
(* For 3D gait, remove Boom dynamics*)
(*\[ScriptCapitalD]e=\[ScriptCapitalD]eBody + \[ScriptCapitalD]eMotor;*)
(*\[ScriptCapitalD]e=\[ScriptCapitalD]eBody + \[ScriptCapitalD]eMotor + \[ScriptCapitalD]eBoom;*)
\[ScriptCapitalD]e=\[ScriptCapitalD]eBody + \[ScriptCapitalD]eMotor;
\[ScriptCapitalD]e/.qe0subs//MatrixForm


(* ::Subsection:: *)
(*Coriolis Matrix*)


\[ScriptCapitalC]eBody=InertiaToCoriolis[\[ScriptCapitalD]eBody,Flatten[Qe],Flatten[dQe]];
\[ScriptCapitalC]eBoom=InertiaToCoriolis[\[ScriptCapitalD]eBoom,Flatten[Qe],Flatten[dQe]];

\[ScriptCapitalC]e=\[ScriptCapitalC]eBody+\[ScriptCapitalC]eBoom;


(* ::Subsection:: *)
(*Spring Forces, N/A for hopper*)


(*springs=First[model["springs"]]//RationalizeEx;
(*find indices of spring joints*)
springIndices =Block[{actuator},
	Select[
		Table[
			actuator=Association@dofs[[i]]["actuator"];
			If[MatchQ[actuator["type"],"spring"],
				i,
				0
			]
			,
			{i,ndof}
		],
	#!=0&]
];
(*construct spring stiffness matrix and spring damping matrix*)
Ks=ConstantArray[0,{ndof,ndof}];
Kb=ConstantArray[0,{ndof,ndof}];
Table[
	If[MemberQ[springIndices,i],
		Ks[[i,i]]=springs["stiffness"];
		Kb[[i,i]]=springs["damping"];
	]
	,
	{i,ndof}
];
\[ScriptCapitalF]s = Ks.Qe+Kb.dQe;*)


(* ::Subsection:: *)
(*Friction Forces*)


\[ScriptCapitalF]r = boom["friction"]*(Subscript[Je, b]\[Transpose].Subscript[Je, b].dQe);
