(* ::Package:: *)

(* ::Title:: *)
(*Export Kinematics Expressions*)


(* ::Section:: *)
(*Export Symbolic Expressions*)


statesubs=Dispatch@Join[
	((\[ScriptCapitalX][[#+1,1]]-> HoldForm@x[[#]]&)/@(Range[2*ndof]-1))];


SetOptions[CseWriteCpp,
	Directory->$DurusSimExportPath,
	ArgumentLists->{x},
	ArgumentDimensions-> {{2*ndof,1}},
	SubstitutionRules-> statesubs
];


(* Center of Mass Position *)
CseWriteCpp["pe_com_vec",{Subscript[pe, com]}];
CseWriteCpp["Je_com_mat",{Subscript[Je, com]}];
CseWriteCpp["dJe_com_mat",{Subscript[dJe, com]}];

(* positions for animation *)
CseWriteCpp["jpos_mat",{jpos}];


(*Block[
	{name,syms,filename},
	Table[
		name=pos["name"];		
		syms=StringJoin[name,#]&/@{"PosX","PosY","PosZ","Roll","Pitch","Yaw"}//ToExpression;
		
		Table[
			SetOptions[CseWriteCpp,
				ArgumentLists->{qe},
				ArgumentDimensions-> {{ndof,1}}
			];
			filename="h_"<>ToString[syms[[i]]];
			CseWriteCpp[filename,{{Subscript[h, syms[[i]]]}}];
			filename="Jh_"<>ToString[syms[[i]]];
			CseWriteCpp[filename,{Subscript[Jh, syms[[i]]]}];
			SetOptions[CseWriteCpp,
				ArgumentLists->{qe,dqe},
				ArgumentDimensions-> {{ndof,1},{ndof,1}}
			];
			filename="dJh_"<>ToString[syms[[i]]];
			CseWriteCpp[filename,{Subscript[dJh, syms[[i]]]}];
			,
			{i,6}
		];
		,
		{pos,contactPoints}
	];
];*)


(*Block[
	{name,filename,syms},
	Table[
		name=pos["name"];
		syms=ToExpression[name];
		SetOptions[CseWriteCpp,
			ArgumentLists->{qe},
			ArgumentDimensions-> {{ndof,1}}
		];
		filename="h_"<>ToString[syms];
		CseWriteCpp[filename,{{Subscript[h, syms]}}];
		filename="Jh_"<>ToString[syms];
		CseWriteCpp[filename,{Subscript[Jh, syms]}];
		SetOptions[CseWriteCpp,
			ArgumentLists->{qe,dqe},
			ArgumentDimensions-> {{ndof,1},{ndof,1}}
		];
		filename="dJh_"<>ToString[syms];
		CseWriteCpp[filename,{Subscript[dJh, syms]}];
		,
		{pos,joints}
	];
];*)
