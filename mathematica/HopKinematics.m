(* ::Package:: *)

(* ::Title:: *)
(*DurusExprKinematics*)


(* ::Section:: *)
(*Center of Mass Position*)


masses=Block[{dof},Table[dof["mass"],{dof,dofs}]];
mass=Total[masses];
Subscript[pe, com]=Transpose[{ \!\(
\*UnderoverscriptBox[\(\[Sum]\), \(i = 1\), \(ndof\)]\(masses[\([i]\)]\ 
\(\*SubscriptBox[\(p\), 
SubscriptBox[\(sl\), \(i\)]]\)[\[Theta]]/mass\)\)}];
Subscript[Je, com]=D[Flatten[Subscript[pe, com]],{Flatten[Qe],1}];
Subscript[dJe, com]=D[Subscript[Je, com],t];


name="Com";
syms=StringJoin[name,#]&/@{"PosX","PosY","PosZ"}//ToExpression;
Table[
	Subscript[h, syms[[i]]]=Subscript[pe, com][[i]];
	Subscript[Jh, syms[[i]]]=Subscript[pe, com][[i]];
	Subscript[dJh, syms[[i]]]=\!\(
\*SubscriptBox[\(\[PartialD]\), \(t\)]
\*SubscriptBox[\(Jh\), \(syms[\([i]\)]\)]\);
,
	{i,3}
];


(* ::Section:: *)
(*Animation positions*)


positions=model["positions"]//RationalizeEx;
jpos=Block[
	{sym,lambda,offset,pos},
	Table[
		sym=ToExpressionEx[pos["name"]];
		lambda=pos["lambda"];
		offset=pos["offset"];
		Subscript[g, sym][\[Theta]]=Subscript[g, Subscript[sj, lambda]][\[Theta]].RPToHomogeneous[I3,offset];
		RigidPosition[Subscript[g, sym][\[Theta]]]
		(*Subscript[R, sym]=RigidOrientation[Subscript[g, sym][\[Theta]]];*)
		,
		{pos,positions}
	]
]//Transpose;


(* ::Section:: *)
(*Load Kinematics Configuration *)


KinConfigFile=FileNameJoin[{$DurusConfigPath,"controller","kinematicsDefs.yaml"}]


kin = Apply[Association,Association@SnakeYaml`YamlReadFile[KinConfigFile],{2}];


kinConsts = model["kinConst"]//Normal//First//ToExpressionEx;


(* ::Section:: *)
(*Generate expressions (h,Jh,Jhdot)*)


contactPoints=kin["contact points"];
joints=kin["joints"];
orientations=kin["orientations"];


Block[
	{name,lambda,offset,p,\[ScriptCapitalR],\[ScriptCapitalR]0,\[ScriptCapitalR]w,\[Omega],syms,roll,pitch,yaw,h6dof,Jh6dof},
	Table[
		name=pos["name"];
		lambda=(pos["lambda"]/.dofsIndex)[[1,1]];
		offset=ToExpression[pos["offset"]]/.kinConsts;
		p=RigidPosition[Subscript[g, Subscript[sj, lambda]][\[Theta]].RPToHomogeneous[I3,offset]];
		
		
		\[ScriptCapitalR]=RigidOrientation[Subscript[g, Subscript[sj, lambda]][\[Theta]].RPToHomogeneous[I3,offset]];
		\[ScriptCapitalR]0=RigidOrientation[Subscript[g, Subscript[sj, lambda]][0].RPToHomogeneous[I3,offset]];
		\[ScriptCapitalR]w=\[ScriptCapitalR].Transpose[\[ScriptCapitalR]0];
		\[Omega]=\[ScriptCapitalR].CrossProd[Transpose[\[ScriptCapitalR]].\!\(
\*SubscriptBox[\(\[PartialD]\), \(t\)]\[ScriptCapitalR]\)];
		syms=StringJoin[name,#]&/@{"PosX","PosY","PosZ","Roll","Pitch","Yaw"}//ToExpression;
		yaw=ArcTan[\[ScriptCapitalR]w[[1,1]],\[ScriptCapitalR]w[[2,1]]];
		roll=ArcTan[\[ScriptCapitalR]w[[3,3]],\[ScriptCapitalR]w[[3,2]]];
		pitch=ArcTan[\[ScriptCapitalR]w[[3,3]],-\[ScriptCapitalR]w[[3,1]]Cos[roll]];
		h6dof=Join[p,{roll,pitch,yaw}];
		Jh6dof=Join[\!\(
\*SubscriptBox[\(\[PartialD]\), \({Flatten[Qe], 1}\)]p\),\!\(
\*SubscriptBox[\(\[PartialD]\), \({Flatten[dQe], 1}\)]\[Omega]\)];
		Table[
			Subscript[h, syms[[i]]]=h6dof[[i]];
			Subscript[Jh, syms[[i]]]=Jh6dof[[i]];
			Subscript[dJh, syms[[i]]]=\!\(
\*SubscriptBox[\(\[PartialD]\), \(t\)]
\*SubscriptBox[\(Jh\), \(syms[\([i]\)]\)]\);
			,
			{i,6}
		];
		,
		{pos,contactPoints}
	];
];


Block[
	{name,lambda,syms,expr,subs,vars},
	Table[
		name=pos["name"];
		vars=pos["vars"];
		expr=ToExpression[pos["expr"]];
		subs = Table[
			ToExpression["var"<>ToString[i]]-> First@Qe[[(vars[[i]]/.dofsIndex),1]]
			,
			{i,Length[vars]}
		];
		syms=ToExpression[name];
		Subscript[h, syms]=expr/.subs;
		Subscript[Jh, syms]=\!\(
\*SubscriptBox[\(\[PartialD]\), \({Flatten[Qe], 1}\)]\(Subscript[h, \ syms]\)\);
		Subscript[dJh, syms]=\!\(
\*SubscriptBox[\(\[PartialD]\), \(t\)]
\*SubscriptBox[\(Jh\), \(syms\)]\);
		,
		{pos,joints}
	];
];


definedPositions=Join[
	Flatten[
		Table[
			StringJoin[name,#]&/@{"PosX","PosY","PosZ"},
			{name,Join[{"Com"},(#["name"])&/@kin["contact points"]]}
		]
	]
	,
	(#["name"])&/@kin["joints"],
	(#["name"])&/@kin["orientations"]
];


Block[
	{name,vars,expr,subs,syms},
	Table[
		name=pos["name"];
		vars=pos["vars"];
		expr=ToExpression[pos["expr"]];
		syms=ToExpression[name];
		subs = Table[
			Assert[MemberQ[definedPositions,vars[[i]]]];
			ToExpression["var"<>ToString[i]]-> Subscript[h,ToExpression[vars[[i]]]]
			,
			{i,Length[vars]}
		];
		Subscript[h, syms]=expr/.subs;
		Subscript[Jh, syms]=\!\(
\*SubscriptBox[\(\[PartialD]\), \({Flatten[Qe], 1}\)]\(Subscript[h, \ syms]\)\);
		Subscript[dJh, syms]=\!\(
\*SubscriptBox[\(\[PartialD]\), \(t\)]
\*SubscriptBox[\(Jh\), \(syms\)]\);
		,
		{pos,orientations}
	];
];
