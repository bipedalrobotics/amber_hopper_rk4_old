(* ::Package:: *)

(* ::Title:: *)
(*2D Hopping Optimization*)


(* ::Section:: *)
(*Initialization*)


ClearAll;


SetDirectory[NotebookDirectory[]];
UtilsPath=FileNameJoin[{NotebookDirectory[],"utils"}];
$Path=DeleteDuplicates[Append[$Path,UtilsPath]];


Needs["Units`"];
Needs["RobotLinks`"];
Needs["ExtraUtil`"];
Needs["AmberShared`"];
Needs["SnakeYaml`"];
Needs["CseOptimization`"];


On[Assert];


YamlInit[];


(* configuration files path *)
$DurusConfigPath=FileNameJoin[{ParentDirectory[],"config"}];


(* ::Chapter:: *)
(*Compute necessary pre-expressions for optimziation*)


(* ::Section:: *)
(*Load Behavior Configuration*)


$OptConfigName="opt_2DRuning";(*"opt_2DFlatWalking";*)
OptConfigFile = FileNameJoin[{$DurusConfigPath,"opt",$OptConfigName<>".yaml"}];
robot=Apply[Association,Association@SnakeYaml`YamlReadFile[OptConfigFile],{2}];


(* exported files for optimization path *)
$DurusOptExportPath=FileNameJoin[{ParentDirectory[],"matlab","build_opt",$OptConfigName}];
EnsureDirectoryExists[$DurusOptExportPath];
SetOptions[CseWriteCpp,
	Directory->$DurusOptExportPath
];
(* 
EnsureDirectoryExists[FileNameJoin[{$DurusOptExportPath,"src"}]]
EnsureDirectoryExists[FileNameJoin[{$DurusOptExportPath,"include"}]]
*)


(* ::Section:: *)
(*Robot Model*)


(*Execute this package before any other sub packages.*)
(*This package loads robot model configuration and compute forward kinematics*)
<<HopModel`;//AbsoluteTiming


(* ::Section:: *)
(*Robot Kinematics*)


(*This package build expressions for kinematics elements, *)
(*e.g. positions, constraints, jacobians *)
<<HopKinematics`;//AbsoluteTiming


(* ::Section:: *)
(*Robot Dynamics*)


(*This package build symbolic expressions of robot dynamics*)
<<HopDynamics`;//AbsoluteTiming


(* ::Section:: *)
(*Robot Outputs*)


<<HopOutputs`;//AbsoluteTiming


(* ::Section:: *)
(*Robot Behaviors*)


<<HopBehavior`;//AbsoluteTiming


(* ::Chapter:: *)
(*Optimization Expressions*)


(* ::Section:: *)
(*Natural Dynamics*)


(*naturalDynamics=Table[\[ScriptCapitalD]e[[i,;;]].ddQe + \[ScriptCapitalC]e[[i,;;]].dQe+\[ScriptCapitalG]e[[i,1]]+\[ScriptCapitalF]s[[i,1]]+\[ScriptCapitalF]r[[i,1]],{i,ndof}];
ExportWithGradient["naturalDynamics",naturalDynamics,{Qe,dQe,ddQe}];//AbsoluteTiming*)
naturalDynamics = Table[\[ScriptCapitalD]e[[i,;;]].ddQe + \[ScriptCapitalC]e[[i,;;]].dQe+\[ScriptCapitalG]e[[i,1]]+\[ScriptCapitalF]r[[i,1]],{i,ndof}];
ExportWithGradient["naturalDynamics",naturalDynamics,{Qe,dQe,ddQe}];//AbsoluteTiming


(* ::Section:: *)
(*Domain Specified Expressoins of Constraints.*)


(* ::Subsection:: *)
(*Load optimization options and domains specification*)


optOptions = robot["options"]//First;
domains = robot["domains"];


domain = domains[[2]];


nDomains=Length[domains];
Table[
	domain=domains[[i]];
	<<HopOptDomain`;
	,
	{i,nDomains}
];


(* ::Section:: *)
(*Export Expressions*)


(*SetDirectory[NotebookDirectory[]]
SetDirectory[FileNameJoin[{ParentDirectory[],"matlab"}]]
makeCommand="make -j4 opt"
Run["xterm -e \""<>makeCommand<>"\""]
SetDirectory[NotebookDirectory[]]*)
