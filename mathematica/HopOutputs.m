(* ::Package:: *)

(* ::Title:: *)
(*Durus Outputs*)


(* ::Section:: *)
(*Functions Definition*)


(* ::Subsection:: *)
(*Desired Functions*)


(*Define desired functions here*)
DesiredFunction::badargs="Undefined Function Type";
DesiredFunction["Constant"]:={a[1]};
DesiredFunction["CWF"]:={(a[1] Cos[a[2] \[Sigma][t]]+a[3] Sin[a[2] \[Sigma][t]])/Exp[a[4]\[Sigma][t]]+a[5]};
DesiredFunction["ECWF"]:={
	(a[1] Cos[a[2] \[Sigma][t]]+a[3] Sin[a[2] \[Sigma][t]])/Exp[a[4]\[Sigma][t]]+
	(2*a[4]*a[5]*a[6])/(a[4]^2+a[2]^2-a[6]^2) Sin[a[6]*\[Sigma][t]]+a[7]};

DesiredFunction["Bezier5thOrder"]:={Sum[a[j+1]*Binomial[4,j]*\[Sigma][t]^j*(1-\[Sigma][t])^(4-j),{j,0,4}]};
DesiredFunction["Bezier6thOrder"]:={Sum[a[j+1]*Binomial[5,j]*\[Sigma][t]^j*(1-\[Sigma][t])^(5-j),{j,0,5}]};
DesiredFunction["Bezier7thOrder"]:={Sum[a[j+1]*Binomial[6,j]*\[Sigma][t]^j*(1-\[Sigma][t])^(6-j),{j,0,6}]};
DesiredFunction["MinJerk"]:={a[2]+(a[1]-a[2])*(10*(\[Sigma][t]/a[3])^3-15*(\[Sigma][t]/a[3])^4+6*(\[Sigma][t]/a[3])^5)};
DesiredFunction[type_?StringQ]:=(Message[DesiredFunction::badargs];$Failed);


NumOfParams::badargs="Undefined Function Type";
NumOfParams["Constant"]:=1;
NumOfParams["CWF"]:=5;
NumOfParams["ECWF"]:=7;

NumOfParams["Bezier5thOrder"]:=5;
NumOfParams["Bezier6thOrder"]:=6;
NumOfParams["Bezier7thOrder"]:=7;
NumOfParams["MinJerk"]:=3;
NumOfParams[type_?StringQ]:=(Message[DesiredFunction::badargs];$Failed);


(* ::Subsection:: *)
(*Actual Functions*)


GetVarSubs["position",vars_]:=
	Block[{},		
		Table[
			Assert[MemberQ[definedPositions,vars[[i]]]];
			ToExpression["var"<>ToString[i]]-> Subscript[h,ToExpression[vars[[i]]]]
			,
			{i,Length[vars]}
		]
	];
GetVarSubs["joint",vars_]:=
	Block[{},
		Table[
			Assert[KeyExistsQ[dofsIndex,vars[[i]]]];
			ToExpression["var"<>ToString[i]]-> Qe[[First@(vars[[i]]/.dofsIndex),1]]
			,
			{i,Length[vars]}
		]
	];
GetRD2Output[output_?AssociationQ]:=
	Block[
		{expr,subs},
		expr=ToExpression[output["expr"]];
		subs=GetVarSubs[output["type"],output["vars"]];
		If[output["linearize"],
			(D[expr/.subs,{Flatten[Qe],1}]/.qe0subs).Qe,
			{expr/.subs}
		]
	];
GetRD1Output[output_?AssociationQ]:=
	Block[
		{expr,subs},
		expr=ToExpression[output["expr"]];
		subs=GetVarSubs[output["type"],output["vars"]];
		If[output["linearize"],
			(D[expr/.subs,{Flatten[Qe],1}]/.qe0subs).dQe,
			{D[expr/.subs,t]}
		]
	];


(* ::Section:: *)
(*Build Expressions for Outputs*)


(* ::Subsection:: *)
(*Load Configuration Files*)


OutputConfigFile=FileNameJoin[{$DurusConfigPath,"controller","outputDefs.yaml"}];
outputDefs=Apply[Association,Association@SnakeYaml`YamlReadFile[OutputConfigFile],{2}];


(* ::Subsection:: *)
(*Relative Degree One Outputs*)


outputRD1=outputDefs["degreeOneOutputs"];
Block[
	{name},
	Table[
		name=ToExpression[output["name"]];
		Subscript[ya1, name]=GetRD1Output[output];
		Subscript[Dya1, name]=J[Subscript[ya1, name],\[ScriptCapitalX]];
		,
		{output,outputRD1}
	];
];


(* ::Subsection:: *)
(*Relative Degree Two Outputs*)


outputRD2=outputDefs["degreeTwoOutputs"];
Block[
	{name},
	Table[
		name=ToExpression[output["name"]];
		Subscript[ya2, name]=GetRD2Output[output];
		Subscript[Dya2, name]=J[Subscript[ya2, name],\[ScriptCapitalX]];
		Subscript[DLfya2, name]=J[Subscript[Dya2, name].d\[ScriptCapitalX],\[ScriptCapitalX]];
		,
		{output,outputRD2}
	];
];



(* ::Subsection:: *)
(*Phase variables*)


phaseVars=outputDefs["phaseVariables"];
Block[
	{name},
	Table[
		name=ToExpression[output["name"]];
		Subscript[\[Delta]p, name]=GetRD2Output[output];
		Subscript[J\[Delta]p, name]=J[Subscript[\[Delta]p, name],Qe];
		(*Subscript[dJ\[Delta]p, name]=D[Subscript[J\[Delta]p, name],t];*)
		,
		{output,phaseVars}
	];
];
