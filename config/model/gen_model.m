(* ::Package:: *)

(* ::Title:: *)
(*Convert Model data to YAML Model File	*)


ClearAll;


(* ::Section:: *)
(*Initialization*)


SetDirectory[NotebookDirectory[]];
UtilsPath=FileNameJoin[{ParentDirectory[ParentDirectory[]],"mathematica","utils"}];
$Path=DeleteDuplicates[Append[$Path,UtilsPath]];
Needs["SnakeYaml`"];
Needs["ExtraUtil`"];
Needs["Units`"];
Needs["AmberShared`"];
YamlInit[];


(*Coordinate transformation*)
Rx[q_]:={{1,0,0},{0,Cos[q],-Sin[q]},{0,Sin[q],Cos[q]}};
Ry[q_]:={{Cos[q],0,Sin[q]},{0,1,0},{-Sin[q],0,Cos[q]}};
Rz[q_]:={{Cos[q],-Sin[q],0},{Sin[q],Cos[q],0},{0,0,1}};


I3=IdentityMatrix[3];
Z3=ConstantArray[0,{3,3}];


(* ::Section:: *)
(*Hopper Model*)


constsubs={
	mt ->4.71126748,  ctx->0,  ctz->0,           Lt->0.4064,(*torso*)
	mth->0.92176969, cthx->0,  cthz->0.01791239, Lth->0.224,(*thigh*)
	mc ->0.33106856,  ccx->0 , ccz ->0.28010834, Lc-> 0.572,(*calf*)
	Lboom-> 83.5*.0254,cBx-> 0,cBz -> 0, (* Length of boom [in \[Rule] m] *)
	mxxb2 ->  0, (* Mass of boom 2 in the horizontal direction *)
	mzzb2 ->  0, (* Mass of boom 2 in the vertical direction *)
	mb1  ->  0  (* Mass of boom 1 *)
}//RationalizeEx;


Subscript[\[ScriptCapitalI], lt]=  ({
 { 0.20072776, -0.00000000, -0.00000000},
 {-0.00000000,  0.20297720, -0.04236786},
 {-0.00000000, -0.04236786,  0.03341820}
})//Rationalize;

Subscript[\[ScriptCapitalI], lth]=({
	{ 0.01063420,   -0.00015843,   -0.00033004},
	{-0.00015843,	0.01480092,   -0.00006338},
	{-0.00033004,   -0.00006338,	0.00612958}
}) //Rationalize;

Subscript[\[ScriptCapitalI], lc]=({
	{ 0.04454910,	-0.00000000,	-0.00212442},
	{-0.00000000,	 0.04458955,	-0.00000000},
    {-0.00212442,	-0.00000000, 	0.00033416}
}) //Rationalize;

(*
Subscript[\[ScriptCapitalI], lc]=(12149428/477973453	2367662/5761557953	-(6308981/1241439485)
2367662/5761557953	133/5000	-(4288536/2923117781)
-(6308981/1241439485)	-(4288536/2923117781)	2029417/886524856

)//Rationalize;*)


Subscript[\[ScriptCapitalI], boom]=({
 {340.49, -95.85, 0.05},
 {-95.85, 751.4, -0.01},
 {0.05, -0.01, 473.31}
})*0.000292639653//RationalizeEx;


(*Imirror=({
 {1, -1, 1},
 {-1, 1, -1},
 {1, -1, 1}
});*)


IT={
	Z3,
	Z3,
	Subscript[\[ScriptCapitalI], lt],
	Subscript[\[ScriptCapitalI], lth],
	Subscript[\[ScriptCapitalI], lth],
	Subscript[\[ScriptCapitalI], lc],
	Subscript[\[ScriptCapitalI], lc]
};


masses={0,0,mt,mth,mth,mc,mc}/.constsubs;


offsets={
	{0,0,0},(*px*)
	{0,0,0},(*pz*)
	{0,0,0},(*theta1*)
	{0,0,0},(*theta2*)
	{0,0,0},(*theta3*)
	{0,0,Lth}, (* theta4*)
	{0,0,Lth}, (* theta5*)
}/.constsubs;


coms={
{0,0,0},(*px*)
{0,0,0},(*pz*)
{0,0,0},(*torso, theta1*)
{0,0,cthz}, (* thigh, theta2*)
{0,0,cthz}, (* thigh, theta3 *)
{0,0,ccz}, (* calf, theta4 *)
{0,0,ccz}, (* calf, theta5 *)
}/.constsubs;


(* {symbol, parent joint, offset} *)
extraPositions={
{"FrontTorso",  3, {0,0,Lt/2}}, (* center of front motor *)
{"RearTorso",   3, {0,0,-Lt/2}}, (* center of rear motor *)
{"Torso",       3, {0,0,0}}, (* center of torso *)
{"Frontknee",   6, {0,0,0}},
{"Rearknee",    7, {0,0,0}},
{"FrontFoot",   6, {0,0,Lc}},
{"RearFoot",    7, {0,0,Lc}}
}/.constsubs;
(*extraPositions={
{"Foot",        7, {0,0,Lc}},
{"Frontknee",   6, {0,0,0}},
{"Torso",       3, {0,0,0}}, (* center of torso *)
{"FrontTorso",  3, {0,0,Lt/2}}, (* center of front motor *)
{"RearTorso",   3, {0,0,-Lt/2}}, (* center of rear motor *)
{"Rearknee",    7, {0,0,0}}
}/.constsubs;*)


(* ::Section:: *)
(*Base Configuration*)


modelType="planar";
(*basePosition="FootBased";
modelName="DURUS_2D";*)
basePosition="HipBased";
modelName="HOPPER";
IsSwappingLegs=True;
nBase=2;
nRobot=5;
nDof = nBase + nRobot;


(*DofName={
	{"BasePosX","px"},
	{"BasePosZ","pz"},
	{"BaseRotY","ry"},
	{"StanceSpring","sp"},
	{"StanceAnkle","sa"},
	{"StanceKnee","sk"},
	{"StanceHip","sh"},
	{"NonstanceHip","nsh"},
	{"NonstanceKnee","nsk"},
	{"NonstanceAnkle","nsa"},
	{"NonstanceSpring","nsp"}
};*)
DofName={
	{"BasePosX", "px"},
	{"BasePosZ", "pz"},
	{"Torso",    "tor"},
	{"FrontHip",  "fh"},
	{"RearHip",  "rh"},
	{"FrontKnee","fk"},
	{"RearKnee", "rk"}
};


DofAxis={
	{0,0,0,1,0,0},
	{0,0,0,0,0,1},
	{0,1,0,0,0,0},
	{0,1,0,0,0,0},
	{0,1,0,0,0,0},
	{0,1,0,0,0,0},
	{0,1,0,0,0,0}
};


DofType={
	"prismatic",
	"prismatic",
	"revolute",
	"revolute",
	"revolute",
	"revolute",
	"revolute"
};


rpy={0,0,0};


(* ::Section:: *)
(*Create Kinematic Tree Table*)


Block[{\[Phi],\[Theta],\[Psi]},
	ExDofs = Table[
		{\[Phi],\[Theta],\[Psi]}=rpy;	
		{
			"name"-> DofName[[i]],
			"axis"-> DofAxis[[i,;;]],
			"type"-> DofType[[i]],
			"mass"-> CRoundEx[masses[[i]]]//N,
			"com"-> CRoundEx[coms[[i,;;]]]//N,
			"inertia"-> CRoundEx[IT[[i]]]//N,
			"E"-> CRoundEx[Rz[\[Psi]].Ry[\[Theta]].Rx[\[Phi]]]//N,
			"r"-> CRoundEx[offsets[[i,;;]]]//N,
			If[i<5, "lambda"-> i-1,
					"lambda"-> i-2
			   ],
			"actuator"-> {{
				"inertia"-> If[StringMatchQ[DofName[[i,1]],StartOfString~~"Base"~~__],
							0.0,
							If[StringMatchQ[DofName[[i,1]],__~~"Hip"~~EndOfString], 0.00200305,
							 0.0]],
				"type"-> If[StringMatchQ[DofName[[i,1]],StartOfString~~"Base"~~__],
							"base",
							If[StringMatchQ[DofName[[i,1]],__~~"Hip"~~EndOfString],"rotor",
							"none"]],
				"ratio"-> If[StringMatchQ[DofName[[i,1]],__~~"Hip"~~EndOfString],4,
							0]
				(*"ratio"-> 4.0*)
			}}
		},
		{i,1,nDof}
	];
];


ExDofs//MatrixForm


(* ::Section:: *)
(*Others*)


posStruct=Block[{},
Table[
	{
	"name"-> pos[[1]],
	"lambda"-> pos[[2]],
	"offset"-> CRoundEx[pos[[3]]]//N
	}
	,
	{pos, extraPositions}
]];
posStruct//MatrixForm;


BoomModel={
	"mass"->CRoundEx[mb1*I3+ {{mxxb2,0,0},{0,0,0},{0,0,mzzb2}}/.constsubs]//N,
	"inertia"->CRoundEx[Subscript[\[ScriptCapitalI], boom]]//N,
	"lambda"->"FrontHip",
	"offset"->{0,0,0},
	"friction"->0
};


kinematicConstant=CRoundEx[{
	"Lt"->Lt, "Lth"->Lth, "Lc"->Lc}/.constsubs]//N;


(* ::Section:: *)
(*Export*)


yamlStruct={
	"type"-> modelType,
	"nDoF"-> nDof,
	"nBase"-> nBase,
	"name"-> modelName,
	"base"-> basePosition,
	"IsSwappingLegs"-> IsSwappingLegs,
	"dofs"-> ExDofs,	
	"boom"-> {BoomModel},
	"kinConst"->{kinematicConstant},
	"positions"-> posStruct
};


SetDirectory[NotebookDirectory[]];
YamlWriteFile[yamlStruct,"hopper_model.yaml"];
