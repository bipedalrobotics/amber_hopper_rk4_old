
setup;
behaviorName = 'Periodic2DFlatWalking'
[model, domains, controller, simOptions, behaviorConfig] = loadBehavior(behaviorName);

[model, domains, calcs_steps] = simulate_new
out = horzcat_fields_domains([calcs_steps{:}],true);


domain = domains{2};
for i =  1:numel(out.t)
    xi = out.x(:,i);
    [~, y_a2, ~, Dy_a2, ~] = calcActualOutputs(domain, xi, model);
    [~, y_d2, ~, Dy_d2, ~, ~, ~, ~, ~] = ...
        calcDesiredOutputs(domain, 0, xi, model, controller);
    %     y2    = y_a2 - y_d2;
    %     Dy2   = Dy_a2 - Dy_d2;
    %
    %     vfc =
    %     Lfy2  = Dy2*vfc;
    
    ya2(:,i) = y_a2;
    yd2(:,i) = y_d2;
    
end