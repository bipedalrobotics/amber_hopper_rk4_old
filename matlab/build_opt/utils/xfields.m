function [obj] = xfields(obj, func, fields)
if nargin < 3
    fields = fieldnames(obj);
end
for i = 1:length(fields)
    field = fields{i};
    obj.(field) = func(obj.(field));
end
end