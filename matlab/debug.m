%%% my debug for simulation.
%% tau
index = 1;
tau1 = calcs_steps{index}.tau;
t1   = calcs_steps{index}.t;
index = 2;
tau2 = calcs_steps{index}.tau;
t2   = calcs_steps{index}.t;

figure(11)
plot(t1, tau1, t2, tau2); grid on


%% animate the simulation 
anim_plot_3D(model, calcs_steps, true, 1000);


%% plot data
savePlot = false;
warning off;
plotStates(model, calcs_steps, savePlot);


%% plot outputs
%plotOutputs(domains, calcs_steps, savePlot);


%% plot holonomic constraints
clear Ho_s Ho_f;
for i = 1:size(calcs_steps{1}.x,2)
    x_tmp = calcs_steps{1}.x(1:7, i);
    Ho_s(:, i) = f_holonomicPos_SingleSupport(x_tmp);
end
for i = 1:size(calcs_steps{2}.x,2)
    x_tmp = calcs_steps{1}.x(1:7, i);
    Ho_f(:, i) = f_holonomicPos_fly(x_tmp);
end


figure(123412); clf
subplot(3,1,1)
plot([Ho_s(3,:), Ho_f(1,:)]); 
subplot(3,1,2)
plot([Ho_s(4,:), Ho_f(2,:)]);
subplot(3,1,3)
plot(Ho_s(1,:)); hold on;
plot(Ho_s(2,:));legend('foot x', 'foot z')

%%
warning on;
