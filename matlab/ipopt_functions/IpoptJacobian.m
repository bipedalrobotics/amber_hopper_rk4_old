function [J] = IpoptJacobian(x, dimsConstr, constrArray, constrRows, constrCols, nOptVar)
    % nonlinear constraints of the optimization problem
    
    
    
    nConstr = numel(constrArray);
    nzmaxConstr = numel(constrRows);
    
    % preallocation
    J_val   = zeros(nzmaxConstr,1);
    for i = 1:nConstr
        constr = constrArray(i);
        var = x(constr.deps); % dependent variables
        
        % calculate constraints value
        J_val(constr.j_index) = constr.jac(var); 
        
        
    end
    
    J = sparse2(constrRows,constrCols,J_val,dimsConstr,...
        nOptVar);
    
end