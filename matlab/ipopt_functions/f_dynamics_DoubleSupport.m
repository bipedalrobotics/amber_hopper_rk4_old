function [f] = f_dynamics_DoubleSupport(var)
%DYNAMCIS_RIGHTDS2DFLAT Summary of this function goes here
%   Detailed explanation goes here

var1 = var(1:33);

[fn] = f_naturalDynamics(var1);
[fc] = f_controlDynamics_DoubleSupport(var);

f = fn + fc;

end