function [grad] = J_dynamics_fly(var)
%DYNAMCIS_RIGHTDS2DFLAT Summary of this function goes here
%   Detailed explanation goes here

%var1 = var(1:33);
var1 = var(1:21);

[grad_fn] = J_naturalDynamics(var1);
[grad_fc] = J_controlDynamics_fly(var);



n = size(grad_fn,1);
grad = grad_fc;
grad(1:n) = grad(1:n) + grad_fn;


end

