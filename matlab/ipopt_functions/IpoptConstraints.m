function [C] = IpoptConstraints(x, constrArray, dimsConstr, debugMode)
    % nonlinear constraints of the optimization problem

    tol = 0.05;
    
    nConstr = numel(constrArray);
    
    % preallocation
    C   = zeros(dimsConstr,1);
    for i = 1:nConstr
        constr = constrArray(i);
        var    = x(constr.deps); % dependent variables
        
        % calculate constraints values
        C(constr.c_index) = constr.f(var); 
        if debugMode
            val = C(constr.c_index);
            cl  = constr.cl;
            cu  = constr.cu;
            
            if min(val - cl) < -tol
                disp(['Lower Bound Violated:  ', func2str(constr.f), ' ===']);
                disp(num2str(val - cl, 5));
                keyboard;
            end
            
            if max(val - cu) > tol
                disp(['Upper Bound Violated:  ', func2str(constr.f), ' :']);
                disp(num2str(val - cu, 5));
                keyboard;
            end
        
        end
%         if constr.cl > -1
%             if max(abs(C(constr.c_index))) > 1e-3
%                 disp(func2str(constr.f));
%                 max(abs(C(constr.c_index)))
%             end
%         end
        
    end
    
end