function [f] = f_dynamics_fly(var)
%DYNAMCIS_RIGHTDS2DFLAT Summary of this function goes here
%   Detailed explanation goes here

var1 = var(1:21);

[fn] = f_naturalDynamics(var1);
[fc] = f_controlDynamics_fly(var);

f = fn + fc;

end