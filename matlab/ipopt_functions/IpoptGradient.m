function [J] = IpoptGradient(x, costArray, costRows, costCols, nOptVar)
    % objective function of the optimization problem
    
    
    
    nNode = numel(costArray);
    nzmaxCost = numel(costRows);
    
    % preallocation
    J_val   = zeros(nzmaxCost,1);
    for i = 1:nNode
        constr = costArray(i);
        var = x(constr.deps); % dependent variables
        
        % calculate constraints value
        J_val(constr.j_index) = constr.jac(var); 
        
        
    end
    
    J = sparse2(costRows,costCols,J_val,1,...
        nOptVar,nzmaxCost);
    
end