% poolobj = gcp('nocreate'); % If no pool, do not create new one.
% if isempty(poolobj)
%     parpool(4);
% end

setup;
addpath('addon');

optName = 'opt_2DRuning';
[opt,model] = loadOptProblem(optName);

opt.options

% add optimization variables
opt = configureOptVariables(opt);

% generate optimization boundaries
opt = genBoundaries(opt);


%% generate initial guess
% load('../results/Periodic2DRuning/optResult_2016-04-20T11-02-Atlanta')
% opt = generateZ0(opt, calcs_steps, model);
% checkZ0(opt);

opt = generateZ0(opt);

%% add constraints
opt = configureConstraints(opt);

%% add cost function
opt = configureObjective(opt);
x0  = opt.Z0;

%%% debug
% c = IpoptConstraints(x0, opt.constrArray, opt.dimsConstr, true);


%% IPOPT options
options.lb = opt.lb;
options.ub = opt.ub;
options.cl = opt.cl;
options.cu = opt.cu;

options.ipopt.mu_strategy      = 'adaptive';
options.ipopt.max_iter         = 2100;
options.ipopt.tol              = 1e-3;
options.ipopt.hessian_approximation = 'limited-memory';
options.ipopt.limited_memory_update_type = 'bfgs';  % {bfgs}, sr1
options.ipopt.limited_memory_max_history = 6;  % {6}
% options.ipopt.recalc_y = 'yes';
% options.ipopt.recalc_y_feas_tol = 1e-3;

% options.ipopt.bound_relax_factor = 1e-3;
options.ipopt.linear_solver = 'ma57';   
% options.ipopt.fixed_variable_treatment = 'RELAX_BOUNDS';
% options.ipopt.derivative_test = 'first-order';
% options.ipopt.point_perturbation_radius = 0;

% options.ipopt.derivative_test_perturbation = 1e-8;
% options.ipopt.derivative_test_print_all = 'yes';
options.ipopt.ma57_automatic_scaling = 'yes';
options.ipopt.linear_scaling_on_demand = 'no';


%% The callback functions.
funcs.objective    = @(x)IpoptObjective(x, opt.costArray);
funcs.constraints  = @(x)IpoptConstraints(x, opt.constrArray, opt.dimsConstr, false);
funcs.gradient     = @(x)IpoptGradient(x, opt.costArray, ...
                            opt.costRows, opt.costCols, opt.nOptVar);
funcs.jacobian     = @(x)IpoptJacobian(x, opt.dimsConstr, ...
                            opt.constrArray, opt.constrRows, opt.constrCols, opt.nOptVar);
funcs.jacobianstructure = @()IpoptJacobianStructure(opt.dimsConstr, ...
                            opt.constrRows, opt.constrCols, opt.nOptVar);
% funcs.hessian           = @hessian;
% funcs.hessianstructure  = @hessianstructure;

diary log.yaml;
tic
    [x info] = ipopt(x0,funcs,options);
toc

%%
[calcs_steps] = extractResult(opt, x, model);
param_name = exportParameters(opt, x, model)

diary off;
save_opt;