% Run MATLAB default 'startup' first, since it won't excute when you start
% from the current directory that contains a 'startup' file.
% run('~/matlab/startup.m')

% STARTUP - Script that setup all the environment variables for the project

% Add system related functions paths
cur = [fileparts(mfilename('fullpath')), '/'];

addpath([cur, 'graphics/']);
addpath([cur, 'build_sim/']);
addpath([cur, 'build_opt/']);
addpath([cur, 'build/']);
addpath([cur, 'ipopt_functions/']);
% addpath([cur, 'sys/']);
% Add hardware tools
addpath([cur, 'hardware/']);

% Add data and figures directories
if ~exist('../data/','dir')
    mkdir('../data/');
end
addpath(genpath([cur, '../data/']));

if ~exist('../results/','dir')
    mkdir('../results/');
end
addpath(genpath([cur, '../results/']));

% Add third party libraries and other useful custom functions path
addpath([cur, 'utils/'], [cur, 'utils/matlab_utilities/']);
addpath(genpath([cur, 'utils/third/']));
addpath_matlab_utilities('general', 'strings', 'yaml', 'ros', 'sim', 'plot', 'validation');
