a    = 0.5;
ep   = 0.06;
hmax = 0.3;
h0   = -0.03;

tau = 0:0.01:1;
h   = zeros(1, length(tau));

for i = 1:length(tau)
    
    h(i) = a*(-2*hmax*tau(i)^2 + 2*hmax*tau(i))*exp(ep*tau(i))+h0;

end

figure(3212)
plot(tau,h); grid on;



%%
t  = calcs_steps{2}.tau;
for i = 1 : length(t)  
    xx = [calcs_steps{2}.qe(:,i); zeros(7,1)]; 
    pos = jpos_mat(xx);
    foot(:, i) = pos([1,3], end);
end
figure(117); clf
subplot(2,1,1)
    plot(t, foot(2,:)); xlim([min(t),max(t)]); grid on
    legend('foot Z')
subplot(2,1,2)
    plot(tau,h); xlim([min(tau), max(tau)]); ylim([0, max(h)]); grid on; legend('clearance')