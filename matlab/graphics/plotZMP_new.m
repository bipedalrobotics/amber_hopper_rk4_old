function plotZMP_new(domains, model, calcs_steps,isSavePlot,behaviorName)

if nargin < 4
    isSavePlot = false;
end


if nargin < 5
    behaviorName = 'tmp';
end


[nDomains,nSteps] = size(calcs_steps);

%% Ground Reaction Forces
% Foot dimensions
wf = model.widthFoot;
lh = model.lengthHeel;
lt = model.lengthToe;

%% right foot ZMP condition
h = figure(8); clf
if ~strcmp(get(0,'DefaultFigureWindowStyle'),'docked')
    set(h,'position',[200 200 1080 640]);
end
for k = 1:nSteps
    for j = 1:nDomains
        
        switch domains{j}.domainName
            case {'RightDS2DFlatWalking','RightDS3DFlatWalking'} % right double support
                t  = calcs_steps{j,k}.t;
                Fe = calcs_steps{j,k}.Fe;
                ah_rp(1) = plot(t,Fe(5,:)*(lt+lh),'c','LineWidth',2); hold on;
                ah_rp(2) = plot(t,Fe(3,:)*lt,'r--','LineWidth',2); hold on;
                ah_rp(3) = plot(t,-Fe(3,:)*lh,'b--','LineWidth',2); hold on;
            case {'RightSS2DFlatWalking','RightSS3DFlatWalking'} % right single supoort
                t  = calcs_steps{j,k}.t;
                Fe = calcs_steps{j,k}.Fe;
                plot(t,Fe(5,:)*(lt+lh),'c','LineWidth',2); hold on;
                plot(t,Fe(3,:)*lt,'r--','LineWidth',2); hold on;
                plot(t,-Fe(3,:)*lh,'b--','LineWidth',2); hold on;
            case {'LeftDS2DFlatWalking','LeftDS3DFlatWalking'} % left double support
                t  = calcs_steps{j,k}.t;
                Fe = calcs_steps{j,k}.Fe;
                plot(t,Fe(11,:)*(lt+lh),'c','LineWidth',2); hold on;
                plot(t,Fe(9,:)*lt,'r--','LineWidth',2); hold on;
                plot(t,-Fe(9,:)*lh,'b--','LineWidth',2); hold on;
            case {'RightTL3DThreeDomains'} % right single support
                t  = calcs_steps{j,k}.t;
                Fe = calcs_steps{j,k}.Fe;
                ah_rp(1) = plot(t,Fe(5,:)*(lt+lh),'c','LineWidth',2); hold on;
                ah_rp(2) = plot(t,Fe(3,:)*lt,'r--','LineWidth',2); hold on;
                ah_rp(3) = plot(t,-Fe(3,:)*lh,'b--','LineWidth',2); hold on;
        end
    end
end
if exist('ah_rp','var')
    ax = gca;
    legend(ax,ah_rp,{'Pitch Moment','Upper Bound','Lower Bound'},1,...
        'FontSize',10,'Interpreter','Latex','Location','eastoutside','Box','Off');
end
grid on
title('Right Foot ZMP Condition (Pitch)')
xlabel('t (s)', 'Interpreter','latex','Fontsize',14);
ylabel('Fe (Nm)','Interpreter','latex','Fontsize',14);

if isSavePlot
    print(h,'-dpng','-r864',['../results/',behaviorName,'/simulation/right_pitch_ZMP']);
end


%%
h = figure(9); clf
if ~strcmp(get(0,'DefaultFigureWindowStyle'),'docked')
    set(h,'position',[200 200 1080 640]);
end
for k = 1:nSteps
    for j = 1:nDomains
        switch domains{j}.domainName
            case {'RightDS2DFlatWalking','RightDS3DFlatWalking'} % right double support
                t  = calcs_steps{j,k}.t;
                Fe = calcs_steps{j,k}.Fe;
                ah_rr(1) = plot(t,Fe(4,:)*wf,'c','LineWidth',2); hold on;
                ah_rr(2) = plot(t,Fe(3,:)*wf/2,'r--','LineWidth',2); hold on;
                ah_rr(3) = plot(t,-Fe(3,:)*wf/2,'b--','LineWidth',2); hold on;
            case {'RightSS2DFlatWalking','RightSS3DFlatWalking'} % right single supoort
                t  = calcs_steps{j,k}.t;
                Fe = calcs_steps{j,k}.Fe;
                plot(t,Fe(4,:)*wf,'c','LineWidth',2); hold on;
                plot(t,Fe(3,:)*wf/2,'r--','LineWidth',2); hold on;
                plot(t,-Fe(3,:)*wf/2,'b--','LineWidth',2); hold on;
            case {'LeftDS2DFlatWalking','Left2DFlatWalking'} % left double support
                t  = calcs_steps{j,k}.t;
                Fe = calcs_steps{j,k}.Fe;
                plot(t,Fe(10,:)*wf,'c','LineWidth',2); hold on;
                plot(t,Fe(9,:)*wf/2,'r--','LineWidth',2); hold on;
                plot(t,-Fe(9,:)*wf/2,'b--','LineWidth',2); hold on;
            case {'RightTL3DThreeDomains'}
                t  = calcs_steps{j,k}.t;
                Fe = calcs_steps{j,k}.Fe;
                ah_rr(1) = plot(t,Fe(4,:)*wf,'c','LineWidth',2); hold on;
                ah_rr(2) = plot(t,Fe(3,:)*wf/2,'r--','LineWidth',2); hold on;
                ah_rr(3) = plot(t,-Fe(3,:)*wf/2,'b--','LineWidth',2); hold on;
        end
    end
end
if exist('ah_rr','var')
    ax = gca;
    legend(ax,ah_rr,{'Roll Moment','Upper Bound','Lower Bound'},1,...
        'FontSize',10,'Interpreter','Latex','Location','eastoutside','Box','Off');
end
grid on
title('Right Foot ZMP Condition (Roll)')
xlabel('t (s)', 'Interpreter','latex','Fontsize',14);
ylabel('Fe (Nm)','Interpreter','latex','Fontsize',14);
if isSavePlot
    print(h,'-dpng','-r864',['../results/',behaviorName,'/simulation/right_roll_ZMP']);
end


%% left foot ZMP condition
h = figure(10); clf
if ~strcmp(get(0,'DefaultFigureWindowStyle'),'docked')
    set(h,'position',[200 200 1080 640]);
end
for k = 1:nSteps
    for j = 1:nDomains
        if isempty(calcs_steps{j,k})
            continue;
        end
        switch domains{j}.domainName
            case {'RightDS2DFlatWalking','RightDS3DFlatWalking'} % right double support
                t  = calcs_steps{j,k}.t;
                Fe = calcs_steps{j,k}.Fe;
                ah_lp(1) = plot(t,Fe(11,:)*(lt+lh),'c','LineWidth',2); hold on;
                ah_lp(2) = plot(t,Fe(9,:)*lt,'r--','LineWidth',2); hold on;
                ah_lp(3) = plot(t,-Fe(9,:)*lh,'b--','LineWidth',2); hold on;
            case {'LeftDS2DFlatWalking','LeftDS3DFlatWalking'} % left double support
                t  = calcs_steps{j,k}.t;
                Fe = calcs_steps{j,k}.Fe;
                plot(t,Fe(5,:)*(lt+lh),'c','LineWidth',2); hold on;
                plot(t,Fe(3,:)*lt,'r--','LineWidth',2); hold on;
                plot(t,-Fe(3,:)*lh,'b--','LineWidth',2); hold on;
                
            case {'LeftSS2DFlatWalking','LeftSS3DFlatWalking'} % left single supoort
                t  = calcs_steps{j,k}.t;
                Fe = calcs_steps{j,k}.Fe;
                plot(t,Fe(5,:)*(lt+lh),'c','LineWidth',2); hold on;
                plot(t,Fe(3,:)*lt,'r--','LineWidth',2); hold on;
                plot(t,-Fe(3,:)*lh,'b--','LineWidth',2); hold on;
            case {'LeftTL3DThreeDomains'} % right single support
                t  = calcs_steps{j,k}.t;
                Fe = calcs_steps{j,k}.Fe;
                ah_lp(1) = plot(t,Fe(5,:)*(lt+lh),'c','LineWidth',2); hold on;
                ah_lp(2) = plot(t,Fe(3,:)*lt,'r--','LineWidth',2); hold on;
                ah_lp(3) = plot(t,-Fe(3,:)*lh,'b--','LineWidth',2); hold on;
        end
    end
end
if exist('ah_lp','var')
    ax = gca;
    legend(ax,ah_lp,{'Pitch Moment','Upper Bound','Lower Bound'},1,...
        'FontSize',10,'Interpreter','Latex','Location','eastoutside','Box','Off');
end
grid on
title('Left Foot ZMP Condition (Pitch)')
xlabel('t (s)', 'Interpreter','latex','Fontsize',14);
ylabel('Fe (Nm)','Interpreter','latex','Fontsize',14);
if isSavePlot
    print(h,'-dpng','-r864',['../results/',behaviorName,'/simulation/left_pitch_ZMP']);
end

%%
h = figure(11); clf
if ~strcmp(get(0,'DefaultFigureWindowStyle'),'docked')
    set(h,'position',[200 200 1080 640]);
end
for k = 1:nSteps
    for j = 1:nDomains
        if isempty(calcs_steps{j,k})
            continue;
        end
        switch domains{j}.domainName
            case {'RightDS2DFlatWalking','RightDS3DFlatWalking'} % right double support
                t  = calcs_steps{j,k}.t;
                Fe = calcs_steps{j,k}.Fe;
                ah_lr(1) = plot(t,Fe(10,:)*wf,'c','LineWidth',2); hold on;
                ah_lr(2) = plot(t,Fe(9,:)*wf/2,'r--','LineWidth',2); hold on;
                ah_lr(3) = plot(t,-Fe(9,:)*wf/2,'b--','LineWidth',2); hold on;
            case {'LeftDS2DFlatWalking','LeftDS3DFlatWalking'} % left double support
                t  = calcs_steps{j,k}.t;
                Fe = calcs_steps{j,k}.Fe;
                plot(t,Fe(4,:)*wf,'c','LineWidth',2); hold on;
                plot(t,Fe(3,:)*wf/2,'r--','LineWidth',2); hold on;
                plot(t,-Fe(3,:)*wf/2,'b--','LineWidth',2); hold on;
            
            case {'LeftSS2DFlatWalking','LeftSS3DFlatWalking'} % left single supoort
                t  = calcs_steps{j,k}.t;
                Fe = calcs_steps{j,k}.Fe;
                plot(t,Fe(4,:)*wf,'c','LineWidth',2); hold on;
                plot(t,Fe(3,:)*wf/2,'r--','LineWidth',2); hold on;
                plot(t,-Fe(3,:)*wf/2,'b--','LineWidth',2); hold on;
            case {'LeftTL3DThreeDomains'} % right single support
                t  = calcs_steps{j,k}.t;
                Fe = calcs_steps{j,k}.Fe;
                ah_lr(1) = plot(t,Fe(4,:)*wf,'c','LineWidth',2); hold on;
                ah_lr(2) = plot(t,Fe(3,:)*wf/2,'r--','LineWidth',2); hold on;
                ah_lr(3) = plot(t,-Fe(3,:)*wf/2,'b--','LineWidth',2); hold on;
        end
    end
end
if exist('ah_lr','var')
    ax = gca;
    legend(ax,ah_lr,{'Roll Moment','Upper Bound','Lower Bound'},1,...
        'FontSize',10,'Interpreter','Latex','Location','eastoutside','Box','Off');
end
grid on
title('Left Foot ZMP Condition (Roll)')
xlabel('t (s)', 'Interpreter','latex','Fontsize',14);
ylabel('Fe (Nm)','Interpreter','latex','Fontsize',14);
if isSavePlot
    print(h,'-dpng','-r864',['../results/',behaviorName,'/simulation/left_roll_ZMP']);
end

%%
mu = model.frictionCoeff;
h = figure(12); clf
if ~strcmp(get(0,'DefaultFigureWindowStyle'),'docked')
    set(h,'position',[200 200 1080 640]);
end
for k = 1:nSteps
    for j = 1:nDomains
        if isempty(calcs_steps{j,k})
            continue;
        end
        switch domains{j}.domainName
            case {'RightDS2DFlatWalking','RightDS3DFlatWalking'} % right double support
                t  = calcs_steps{j,k}.t;
                Fe = calcs_steps{j,k}.Fe;
                ah_lf(1) = plot(t,Fe(7,:),'c','LineWidth',2); hold on;
                ah_lf(2) = plot(t,Fe(8,:),'g','LineWidth',2); hold on;
                ah_lf(3) = plot(t,Fe(9,:)*(mu/sqrt(2)),'r--','LineWidth',2); hold on;
                ah_lf(4) = plot(t,-Fe(9,:)*(mu/sqrt(2)),'b--','LineWidth',2); hold on;
            case {'LeftDS2DFlatWalking','LeftDS3DFlatWalking'} % left double support
                t  = calcs_steps{j,k}.t;
                Fe = calcs_steps{j,k}.Fe;
                plot(t,Fe(1,:),'c','LineWidth',2); hold on;
                plot(t,Fe(2,:),'g','LineWidth',2); hold on;
                plot(t,Fe(3,:)*(mu/sqrt(2)),'r--','LineWidth',2); hold on;
                plot(t,-Fe(3,:)*(mu/sqrt(2)),'b--','LineWidth',2); hold on;
            
            case {'LeftSS2DFlatWalking','LeftSS3DFlatWalking'} % left single supoort
                t  = calcs_steps{j,k}.t;
                Fe = calcs_steps{j,k}.Fe;
                plot(t,Fe(1,:),'c','LineWidth',2); hold on;
                plot(t,Fe(2,:),'g','LineWidth',2); hold on;
                plot(t,Fe(3,:)*(mu/sqrt(2)),'r--','LineWidth',2); hold on;
                plot(t,-Fe(3,:)*(mu/sqrt(2)),'b--','LineWidth',2); hold on;
            case {'LeftTL3DThreeDomains'} % right single support
                t  = calcs_steps{j,k}.t;
                Fe = calcs_steps{j,k}.Fe;
                ah_lf(1) = plot(t,Fe(1,:),'c','LineWidth',2); hold on;
                ah_lf(2) = plot(t,Fe(2,:),'g','LineWidth',2); hold on;
                ah_lf(3) = plot(t,Fe(3,:)*(mu/sqrt(2)),'r--','LineWidth',2); hold on;
                ah_lf(4) = plot(t,-Fe(3,:)*(mu/sqrt(2)),'b--','LineWidth',2); hold on;
        end
    end
end
if exist('ah_lf','var')
    ax = gca;
    legend(ax,ah_lf,{'Fx','Fy','Upper Bound','Lower Bound'},1,...
        'FontSize',10,'Interpreter','Latex','Location','eastoutside','Box','Off');
end
grid on
title('Left Foot Friction Condition')
xlabel('t (s)', 'Interpreter','latex','Fontsize',14);
ylabel('Fe (N)','Interpreter','latex','Fontsize',14);
if isSavePlot
    print(h,'-dpng','-r864',['../results/',behaviorName,'/simulation/left_foot_friction']);
end


%%
h = figure(13); clf
if ~strcmp(get(0,'DefaultFigureWindowStyle'),'docked')
    set(h,'position',[200 200 1080 640]);
end
for k = 1:nSteps
    for j = 1:nDomains
        if isempty(calcs_steps{j,k})
            continue;
        end
        switch domains{j}.domainName
            case {'RightDS2DFlatWalking','RightDS3DFlatWalking'} % right double support
                t  = calcs_steps{j,k}.t;
                Fe = calcs_steps{j,k}.Fe;
                ah_rf(1) = plot(t,Fe(1,:),'c','LineWidth',2); hold on;
                ah_rf(2) = plot(t,Fe(2,:),'g','LineWidth',2); hold on;
                ah_rf(3) = plot(t,Fe(3,:)*(mu/sqrt(2)),'r--','LineWidth',2); hold on;
                ah_rf(4) = plot(t,-Fe(3,:)*(mu/sqrt(2)),'b--','LineWidth',2); hold on;
            
            case {'RightSS2DFlatWalking','RightSS3DFlatWalking'} % right single supoort
                t  = calcs_steps{j,k}.t;
                Fe = calcs_steps{j,k}.Fe;
                plot(t,Fe(1,:),'c','LineWidth',2); hold on;
                plot(t,Fe(2,:),'g','LineWidth',2); hold on;
                plot(t,Fe(3,:)*(mu/sqrt(2)),'r--','LineWidth',2); hold on;
                plot(t,-Fe(3,:)*(mu/sqrt(2)),'b--','LineWidth',2); hold on;
                
            case {'LeftDS2DFlatWalking','LeftDS3DFlatWalking'} % left double support
                t  = calcs_steps{j,k}.t;
                Fe = calcs_steps{j,k}.Fe;
                plot(t,Fe(7,:),'c','LineWidth',2); hold on;
                plot(t,Fe(8,:),'g','LineWidth',2); hold on;
                plot(t,Fe(9,:)*(mu/sqrt(2)),'r--','LineWidth',2); hold on;
                plot(t,-Fe(9,:)*(mu/sqrt(2)),'b--','LineWidth',2); hold on;
            case {'RightTL3DThreeDomains'} % right double support
                t  = calcs_steps{j,k}.t;
                Fe = calcs_steps{j,k}.Fe;
                ah_rf(1) = plot(t,Fe(1,:),'c','LineWidth',2); hold on;
                ah_rf(2) = plot(t,Fe(2,:),'g','LineWidth',2); hold on;
                ah_rf(3) = plot(t,Fe(3,:)*(mu/sqrt(2)),'r--','LineWidth',2); hold on;
                ah_rf(4) = plot(t,-Fe(3,:)*(mu/sqrt(2)),'b--','LineWidth',2); hold on;
        end
    end
end
if exist('ah_rf','var')
    ax = gca;
    legend(ax,ah_rf,{'Fx','Fy','Upper Bound','Lower Bound'},1,...
        'FontSize',10,'Interpreter','Latex','Location','eastoutside','Box','Off');
end
grid on
title('Right Foot Friction Condition')
xlabel('t (s)', 'Interpreter','latex','Fontsize',14);
ylabel('Fe (N)','Interpreter','latex','Fontsize',14);
if isSavePlot
    print(h,'-dpng','-r864',['../results/',behaviorName,'/simulation/right_foot_friction']);
end

end