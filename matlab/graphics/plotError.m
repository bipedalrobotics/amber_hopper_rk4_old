function plotError(domains,calcs_steps,isSavePlot)

if nargin < 3
    isSavePlot = false;
end

calcs_full = [calcs_steps{:}];
out = horzcat_fields_domains(calcs_full, true);
jointName = domains{1}.jointName;

%% plot joint position
is = domains{1}.qrIndices;

count = numel(calcs_steps);
t0s = zeros(1, count);
for i = 1:count
    t0s(i) = calcs_steps{i}.t(1);
end

t = out.t;
q = out.qe(is, :);
dq = out.dqe(is, :);

qd = out.qd(is, :);
dqd = out.dqd(is, :);

name = jointName(is);

h = figure(3);clf;
plot_compare('position error', t, 'actual', q, 'desired', qd);
add_vline();
legend(name);

h = figure(4);clf;
plot_compare('velocity error', t, 'actual', dq, 'desired', dqd);
add_vline();
legend(name);

    function [] = add_vline()
        subplot(2, 1, 1);
        hold('on');
        vline(t0s, 'k:');
        subplot(2, 1, 2);
        hold('on');
        vline(t0s, 'k:');
    end

end
