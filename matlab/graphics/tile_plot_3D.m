function tile_plot_3D(domains, calcs_steps, frameRate)

if nargin < 3
    frameRate = 7;
end
nDomains = numel(domains)/2;

calcs_step1 = [calcs_steps{nDomains+1:end,1}];
calc = horzcat_fields_domains(calcs_step1,true);
calc.t = calc.t - calc.t(1);
calc.x(1,:) = calc.x(1,:) - calc.x(1,1) - 0.2;
[ts,xs] = even_sample(calc.t,calc.x,frameRate/calc.t(end));


n_pts = numel(ts);

x_min = -0.55;
x_max = 5.5;




%% initialize the plot
h=figure(1000); clf
set(h,'Position', [20 80 900 300])
clf;

hold all
xlim([x_min x_max]);
ylim([-0.5 0.5]);
zlim([0 1.5]);
axis off
axis equal
view(0,5)
% view(15,5)
% ground
cdata = [30 20 20 20]';
set(gca,'CLim',[0 40])
patch([x_min x_min x_max x_max],[-0.5 0.5 0.5 -0.5],[0 0 0 0],...
    'FaceColor','interp','FaceVertexCData',cdata,...
    'EdgeColor','interp',...
    'LineWidth',5,...
    'CDataMapping','scaled');
colormap gray




grid
% set(gcf,'Resize','off');
% set(gcf,'Renderer','zbuffer');
apc = [0.025 0.025 0.95 0.95];
set(gca,'position',apc);


pyr_face = [1,2,3;
    1,3,4;
    1,4,5;
    1,5,2];
% face2 = [2 3 4 5];
rec_face = [2 3 4 5;
    2 3 7 6;
    3 4 8 7;
    4 5 9 8;
    5 2 6 9];
% face4 = [6 7 8 9];




%% draw now
stepLength = 0.7;
for i = 1:n_pts
    x = xs(:,i);
    x(1) = x(1) + (i-1)*stepLength;
    pos = jpos_mat(x);
    lleg = pos(:,[1 11:19]);
    rleg = pos(:,[1 20:28]);
    tor = pos(:,1:10);
    
    plot3(lleg(1,1:6),lleg(2,1:6),lleg(3,1:6),'-bo',...
        'MarkerFaceColor','k',...
        'MarkerSize',4,...
        'LineWidth',2);
    lvert = lleg(:,6:end)';
    
    patch('Faces',pyr_face,'Vertices',lvert,'FaceColor','b');
    % patch('Faces',face2,'Vertices',lvert,'FaceColor','b')
    
    
    plot3(rleg(1,1:6),rleg(2,1:6),rleg(3,1:6),'-ro',...
        'MarkerFaceColor','k',...
        'MarkerSize',4,...
        'LineWidth',2);
    rvert = rleg(:,6:end)';
    patch('Faces',pyr_face,'Vertices',rvert,'FaceColor','r');
    % patch('Faces',face2,'Vertices',rvert,'FaceColor','r')
    
    
    
    plot3(tor(1,1:2),tor(2,1:2),tor(3,1:2),'-go',...
        'MarkerFaceColor','k',...
        'MarkerSize',4,...
        'LineWidth',2);
    tvert = tor(:,2:end)';
    patch('Faces',pyr_face,'Vertices',tvert,'FaceColor','c');
    patch('Faces',rec_face,'Vertices',tvert,'FaceColor','c');
    % patch('Faces',face4,'Vertices',tvert,'FaceColor','y')
    
    
end

% print(h,'-dpng','-r864',['../results/tile_plots']);

end