function plotOutputs(domains, calcs_steps, isSavePlot)
if nargin < 3
    isSavePlot = false;
end

[nDomains,nSteps] = size(calcs_steps);
%% plot outputs
cmap   = colormap('lines');
domain = domains{end};

%%
h = figure(41); clf
clear ah

y1plot_exist = false;
if isempty(domains{1}.outputs.actual.degreeOneOutput)
    close(h);
else
    for k = 1:nSteps
        for j = 1:nDomains
            t = calcs_steps{j,k}.t;
            if ~isempty(domains{j}.outputs.actual.degreeOneOutput)
                y1plot_exist = true;
                ya1 = calcs_steps{j,k}.ya1;
                yd1 = calcs_steps{j,k}.yd1;
                plot(t,ya1,'b--','Linewidth',2);
                hold on;
                ah(1) = plot(t,yd1,'b-','Linewidth',2);
                hold on;
            end
        end
    end
end


%%
h = figure(42); clf
clear ah

for k = 1:nSteps
    for j = 1:nDomains
        t = calcs_steps{j,k}.t;
        ya2 = calcs_steps{j,k}.ya2;
        yd2 = calcs_steps{j,k}.yd2;
        
        for i = 1:domains{j}.nOutputs
            plot(t,ya2(i,:),'--','Linewidth',2,...
                'Color',cmap(i,:));
            hold on;
            ah(i) = plot(t,yd2(i,:),'-','Linewidth',2,...
                'Color',cmap(i,:));
            hold on
        end
    end
end
ax = gca;
legend(ax,ah,domain.outputs.actual.degreeTwoOutput',...
      'FontSize',10,'Interpreter','Latex',...
      'Location','eastoutside','Box','Off');

grid on
title('Outputs')
xlabel('t', 'Interpreter','latex','Fontsize',14);
ylabel('y','Interpreter','latex','Fontsize',14);
set(h, 'WindowStyle', 'Docked');
saveName = 'outputs';
if isSavePlot
    if isSavePlot == 1
        beh = 'sim/';
    else
        beh = 'opt/';
    end
    savefig(h,['../results/Periodic2DRuning/', beh, saveName]);
end

%% plot output error (position)
h = figure(43); clf
clear ah

for k = 1:nSteps
    for j = 1:nDomains
        t = calcs_steps{j,k}.t;
        y2 = calcs_steps{j,k}.y2;
        for i = 1:2
            ah(i) = plot(t,y2(i,:),'Linewidth',2,...
                'Color',cmap(i,:));
            hold on;
        end
    end
end
ax = gca;
legend(ax,ah,domain.outputs.actual.degreeTwoOutput', ...
    'FontSize',10,'Interpreter','Latex','Location','eastoutside','Box','Off');

grid on
title('Outputs Error - Position Level')
xlabel('t', 'Interpreter','latex','Fontsize',14);
ylabel('y','Interpreter','latex','Fontsize',14);

set(h, 'WindowStyle', 'Docked');
saveName = 'outputsErr_pos';
if isSavePlot
    if isSavePlot == 1
        beh = 'sim/';
    else
        beh = 'opt/';
    end
    savefig(h,['../results/Periodic2DRuning/', beh, saveName]);
end

%% plot output error (velocity)
h = figure(44); clf
clear ah

for k = 1:nSteps
    for j = 1:nDomains
        t = calcs_steps{j,k}.t;
        Lfy2 = calcs_steps{j,k}.Lfy2;
        
        for i = 1:2
            ah(i) = plot(t,Lfy2(i,:),'Linewidth',2,...
                'Color',cmap(i,:));
            hold on;
        end
    end
end
ax = gca;
legend(ax,ah,domain.outputs.actual.degreeTwoOutput', ...
       'FontSize',10,'Interpreter','Latex', ...
       'Location','eastoutside','Box','Off');

grid on
title('Outputs Error - Velocity Level')
xlabel('t', 'Interpreter','latex','Fontsize',14);
ylabel('y','Interpreter','latex','Fontsize',14);

set(h, 'WindowStyle', 'Docked');
saveName = 'outputsErr_vel';
if isSavePlot
    if isSavePlot == 1
        beh = 'sim/';
    else
        beh = 'opt/';
    end
    savefig(h,['../results/Periodic2DRuning/', beh, saveName]);
end

end