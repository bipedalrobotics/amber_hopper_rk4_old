function plot3_robot_pos(x)

pos3D = jpos_mat(x);
pos = pos3D([1 3],:);
% pcm = pe_com_vec(x,p);
ne = 4; a = 0.27; ro = 0.02;
[x_s, y_s] = spring(pos(1,1),pos(2,1),pos(1,2),pos(2,2),ne,a,ro);
sleg_pos = [x_s, pos(1,2:4); y_s, pos(2,2:4)];

[x_ns, y_ns] = spring(pos(1,8),pos(2,8),pos(1,9),pos(2,9),ne,a,ro);
nsleg_pos = [pos(1,6:8), x_ns; pos(2,6:8),y_ns];

tor_pos = pos(:,4:5);
fig_hl = figure(1000);
set(fig_hl,'Position', [20 80 400 400])
clf;
plot([-1.5 1.5], [0 0], 'k-','LineWidth',4);
hold on;
sleg = plot([0 0], 'r','LineWidth',3);
nleg = plot([0 0], 'b','LineWidth',3);
torso = plot([0 0], 'g','LineWidth',3);
hold off;
anim_axis=[-0.6 0.7 -0.1 1.5];
axis off
axis(anim_axis)
axis equal
grid
% grid on
% ground
% view(15,3)
% patch([x(1)-0.3 x(1)-0.3 x(1)+0.4 x(1)+0.4],[-0.5 0.5 0.5 -0.5],[0 0 0 0])

set(sleg, 'XData', sleg_pos(1,:), 'YData', sleg_pos(2,:));
set(nleg, 'XData', nsleg_pos(1,:), 'YData', nsleg_pos(2,:));
set(torso, 'XData', tor_pos(1,:), 'YData', tor_pos(2,:));

axis equal
axis off
% view(30,20)
% view(30,15)
% view(0,0)

end