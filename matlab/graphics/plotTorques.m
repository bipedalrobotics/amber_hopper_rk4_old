function plotTorques(model,domains,calcs_steps,isSavePlot,behaviorName)

if nargin < 3
    isSavePlot = false;
end
if nargin < 4
    behaviorName = 'tmp';
end

[nDomains,nSteps] = size(calcs_steps);

cmap = colormap('lines');
calcs_full = [calcs_steps{:}];
out = horzcat_fields_domains(calcs_full, true);

%% plot joint torques
h = figure(51); clf;
for k = 1:nSteps
    for j = 1:nDomains
      t = calcs_steps{j,k}.t;
      uq = calcs_steps{j,k}.uq;
      
      for i = 1 : numel(domains{j}.outputs.actuatedJoints)
          jointIndex = getJointIndices(model,domains{j}.outputs.actuatedJoints{i});
          ah(i) = plot(t, uq(jointIndex,:), '-', 'Linewidth', 2, ...
                       'Color', cmap(jointIndex,:));
          hold on;
      end
    end
end
grid on;
rms1 = rms( [calcs_steps{1,1}.uq(4,:), calcs_steps{2,1}.uq(4,:)] );
rms2 = rms( [calcs_steps{1,1}.uq(5,:), calcs_steps{2,1}.uq(5,:)]);

ax = gca;
legend(ax, ah, domains{end}.outputs.actuatedJoints,'Location','eastoutside',...
       'Box','Off', 'Interpreter','latex','Fontsize', 10);
xlabel('$t(s)$',  'Interpreter', 'latex', 'Fontsize', 14);
ylabel('$u(Nm)$', 'Interpreter', 'latex', 'Fontsize', 14);
title(['Joint Torques, RMS: ', int2str(rms1), ' & ', int2str(rms2)]);
xlim([out.t(1) out.t(end)])

set(h, 'WindowStyle', 'Docked');
saveName = 'torques';
if isSavePlot
    if isSavePlot == 1
        beh = 'sim/';
    else
        beh = 'opt/';
    end
    savefig(h,['../results/Periodic2DRuning/', beh, saveName]);
end

end