



h = figure(1);clf
set(h,'position',[100 100 520 380])
for i=1:5   
    ah(i) = plot(out.q(i,:)',out.dq(i,:)',...
        'color',colors_default(i,:),'LineWidth',2); hold on
    plot([out.q(i,end) out.q(i,1)],...
        [out.dq(i,end) out.dq(i,1)],'--',...
        'color',colors_default(i,:),'LineWidth',2); hold on
end
apc = [0.15 0.28 0.82 0.7];
set(gca,'position',apc);

grid on
% legend('sf','sr','sa', 'sk', 'sh', 'nsh', 'nsk','nsa','nsr');
xlabel('$q (rad)$','FontSize',20,'Interpreter','LaTex');
ylabel('$\dot{q} (rad/s)$','FontSize',20,'Interpreter','LaTex');
% xlim([-0.75 0.5]);
% ylim([-1.25 2]);
ax1 = gca;

hlgnd = legend(ax1,ah(1:5),stance_labels(1:5),1,...
    'Location', 'SouthOutside', ...
    'Orientation','Horizontal', ... 
    'Box','off',...
    'Color','none',...
    'FontSize',24,'Interpreter','LaTex');
p = get(hlgnd,'position');
if p(3) > 0.95
    p(3) = 0.95;
    p(1) = 0.03;    
end
set(hlgnd, 'position', [0.01 0.07 1 p(4)],'Box','off');
legend boxoff
% saveas(h,'../figures/icra_2015/q-stance-limit.eps','psc2')
% print(h,'-depsc2','-r864',['../figures/icra_2015/q-stance-limit']);

set(gca,'FontSize',18);
% h = figure(2);clf
for i=6:9   
    ah(i) = plot(out.q(i,:)',out.dq(i,:)',...
        'color',colors_default(i,:),'LineWidth',2); hold on
    plot([out.q(i,end) out.q(i,1)],...
        [out.dq(i,end) out.dq(i,1)],'--',...
        'color',colors_default(i,:),'LineWidth',2); hold on
end

grid on
% legend('sf','sr','sa', 'sk', 'sh', 'nsh', 'nsk','nsa','nsr');
xlabel('$q (rad)$','FontSize',20,'Interpreter','LaTex');
ylabel('$\dot{q} (rad/s)$','FontSize',20,'Interpreter','LaTex');
% xlim([-0.75 1.4]);
% ylim([-4 5]);

ax2=axes('position',get(gca,'position'), 'visible','off');
hlgnd = legend(ax2,ah(6:9),stance_labels(1:4),1,...
    'Location', 'SouthOutside', ...
    'Orientation','Horizontal', ... 
    'Box','off',...
    'Color','none',...
    'FontSize',24,'Interpreter','LaTex');
p = get(hlgnd,'position');
if p(3) > 0.95
    p(3) = 0.95;
    p(1) = 0.03;    
end
set(hlgnd, 'position', [0.01 -0.00 0.8 p(4)],'Box','off');
legend boxoff


l_child1 = get(hlgnd,'Children');
ii = 1;
for i = 1:length(l_child1)
    if strcmp( get(l_child1(i),'Type'),'text')
        set(l_child1(i),'String',nonstance_labels(ii),'Interpreter','LaTex');
        ii = ii + 1;
    end
end

% set(gca,'FontSize',18);
% saveas(h,'../figures/icra_2015/q-nonstance-limit.eps','psc2')
print(h,'-depsc2','-r864',['../figures/icra_2015/q-limit']);