projName = get_project_name();
load(['../figures/simulation_results/',projName,'/sim_data']);

colors_default = [...
    0         0    1.0000
    0    0.5000         0
    1.0000         0         0
    0    0.7500    0.7500
    0.7500         0    0.7500
    0.7500    0.5000         0
    0.2500    0.2500    0.9500
    0.7500    0.2500    0.5000
    0.25      0.75      0.5
    0.5       0.5       0.5];
nSteps = size(calcs_steps,1);
output_labels = {'$q_{sk}$','$q_{nsk}$','$q_{tor}$','$\delta m_{nsl}$','$q_{nsa}$','$q_{sa}$'};
out_limit = calcs_steps{end};
calcs_last4 = [calcs_steps{nSteps-3:nSteps}];
calcs_last2 = [calcs_steps{nSteps-1:nSteps}];
out_last2 = horzcat_fields_domains(calcs_last2);
calcs_first4 = [calcs_steps{1:4}];
out_first4 = horzcat_fields_domains(calcs_first4);
calcs = calcs_last4;
h = figure(3);clf
set(h,'position',[100,100,512,384])
t0 = calcs(1).t(1);
tf = calcs(4).t(end);
for j=1:4
    calc = calcs(j);
    for i=1:6
        t = calc.t - t0;
        ah(i) = plot(t,calc.ya(i,:),'color',colors_default(i,:),'LineWidth',2); hold on
        plot(t,calc.yd(i,:),'-.','color',colors_default(i,:),...
            'LineWidth',4); hold on
        plot(t(end),calc.ya(i,end),'o','color',colors_default(i,:),'LineWidth',2); hold on
    end
end
apc = [0.15 0.15 0.82 0.76];
set(gca,'position',apc);
set(gca,'FontSize',18);
grid on
title('Output Tracking','FontSize',20,'Interpreter','Latex')
xlabel('$t (s)$','FontSize',20,'Interpreter','Latex');
ylabel('$y (rad)$','FontSize',20,'Interpreter','Latex');
xlim([0,tf-t0]);
ylim([-0.7 1.5]);
ax1 = gca;
hlgnd = legend(ax1,ah(1:6),output_labels(1:6),1,...
    'FontSize',20,'Interpreter','Latex');

% saveas(h,'../figures/icra_2015/q-stance-limit.eps','psc2')
print(h,'-dpng','-r100',['../figures/icra_2015/output-zero']);

