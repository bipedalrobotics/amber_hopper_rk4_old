function plotZMP(model, calcs_steps,isSavePlot,behaviorName)

if nargin < 3
    isSavePlot = false;
end


if nargin < 4
    behaviorName = 'tmp';
end


[nDomains,nSteps] = size(calcs_steps);

%% Ground Reaction Forces
% Foot dimensions
wf = model.widthFoot;
lh = model.lengthHeel;
lt = model.lengthToe;

%% right foot ZMP condition
h = figure(8); clf
if ~strcmp(get(0,'DefaultFigureWindowStyle'),'docked')
    set(h,'position',[200 200 1080 640]);
end
for k = 1:nSteps
    for j = 1:nDomains
        switch j
            case 1 % right double support
                t  = calcs_steps{j,k}.t;
                Fe = calcs_steps{j,k}.Fe;
                ah(1) = plot(t,Fe(5,:)*(lt+lh),'c','LineWidth',2); hold on;
                ah(2) = plot(t,Fe(3,:)*lt,'r--','LineWidth',2); hold on;
                ah(3) = plot(t,-Fe(3,:)*lh,'b--','LineWidth',2); hold on;
            case 2 % right single supoort
                t  = calcs_steps{j,k}.t;
                Fe = calcs_steps{j,k}.Fe;
                plot(t,Fe(5,:)*(lt+lh),'c','LineWidth',2); hold on;
                plot(t,Fe(3,:)*lt,'r--','LineWidth',2); hold on;
                plot(t,-Fe(3,:)*lh,'b--','LineWidth',2); hold on;
            case 3 % left double support
                t  = calcs_steps{j,k}.t;
                Fe = calcs_steps{j,k}.Fe;
                plot(t,Fe(11,:)*(lt+lh),'c','LineWidth',2); hold on;
                plot(t,Fe(9,:)*lt,'r--','LineWidth',2); hold on;
                plot(t,-Fe(9,:)*lh,'b--','LineWidth',2); hold on;
        end
    end
end
ax = gca;
legend(ax,ah,{'Pitch Moment','Upper Bound','Lower Bound'},1,...
    'FontSize',10,'Interpreter','Latex','Location','eastoutside','Box','Off');
grid on
title('Right Foot ZMP Condition (Pitch)')
xlabel('t (s)', 'Interpreter','latex','Fontsize',14);
ylabel('Fe (Nm)','Interpreter','latex','Fontsize',14);

if isSavePlot
    print(h,'-dpng','-r864',['../results/',behaviorName,'/simulation/right_pitch_ZMP']);
end

h = figure(9); clf
if ~strcmp(get(0,'DefaultFigureWindowStyle'),'docked')
    set(h,'position',[200 200 1080 640]);
end
for k = 1:nSteps
    for j = 1:nDomains
        switch j
            case 1 % right double support
                t  = calcs_steps{j,k}.t;
                Fe = calcs_steps{j,k}.Fe;
                ah(1) = plot(t,Fe(4,:)*wf,'c','LineWidth',2); hold on;
                ah(2) = plot(t,Fe(3,:)*wf/2,'r--','LineWidth',2); hold on;
                ah(3) = plot(t,-Fe(3,:)*wf/2,'b--','LineWidth',2); hold on;
            case 2 % right single supoort
                t  = calcs_steps{j,k}.t;
                Fe = calcs_steps{j,k}.Fe;
                plot(t,Fe(4,:)*wf,'c','LineWidth',2); hold on;
                plot(t,Fe(3,:)*wf/2,'r--','LineWidth',2); hold on;
                plot(t,-Fe(3,:)*wf/2,'b--','LineWidth',2); hold on;
            case 3 % left double support
                t  = calcs_steps{j,k}.t;
                Fe = calcs_steps{j,k}.Fe;
                plot(t,Fe(10,:)*wf,'c','LineWidth',2); hold on;
                plot(t,Fe(9,:)*wf/2,'r--','LineWidth',2); hold on;
                plot(t,-Fe(9,:)*wf/2,'b--','LineWidth',2); hold on;
        end
    end
end
ax = gca;
legend(ax,ah,{'Roll Moment','Upper Bound','Lower Bound'},1,...
    'FontSize',10,'Interpreter','Latex','Location','eastoutside','Box','Off');
grid on
title('Right Foot ZMP Condition (Roll)')
xlabel('t (s)', 'Interpreter','latex','Fontsize',14);
ylabel('Fe (Nm)','Interpreter','latex','Fontsize',14);
if isSavePlot
    print(h,'-dpng','-r864',['../results/',behaviorName,'/simulation/right_roll_ZMP']);
end


%% left foot ZMP condition
h = figure(10); clf
if ~strcmp(get(0,'DefaultFigureWindowStyle'),'docked')
    set(h,'position',[200 200 1080 640]);
end
for k = 1:nSteps
    for j = 1:nDomains
        if isempty(calcs_steps{j,k})
            continue;
        end
        switch j
            case 1 % right double support
                t  = calcs_steps{j,k}.t;
                Fe = calcs_steps{j,k}.Fe;
                ah(1) = plot(t,Fe(11,:)*(lt+lh),'c','LineWidth',2); hold on;
                ah(2) = plot(t,Fe(9,:)*lt,'r--','LineWidth',2); hold on;
                ah(3) = plot(t,-Fe(9,:)*lh,'b--','LineWidth',2); hold on;
            case 3 % left double support
                t  = calcs_steps{j,k}.t;
                Fe = calcs_steps{j,k}.Fe;
                plot(t,Fe(5,:)*(lt+lh),'c','LineWidth',2); hold on;
                plot(t,Fe(3,:)*lt,'r--','LineWidth',2); hold on;
                plot(t,-Fe(3,:)*lh,'b--','LineWidth',2); hold on;
                
            case 4 % left single supoort
                t  = calcs_steps{j,k}.t;
                Fe = calcs_steps{j,k}.Fe;
                plot(t,Fe(5,:)*(lt+lh),'c','LineWidth',2); hold on;
                plot(t,Fe(3,:)*lt,'r--','LineWidth',2); hold on;
                plot(t,-Fe(3,:)*lh,'b--','LineWidth',2); hold on;
        end
    end
end
ax = gca;
legend(ax,ah,{'Pitch Moment','Upper Bound','Lower Bound'},1,...
    'FontSize',10,'Interpreter','Latex','Location','eastoutside','Box','Off');
grid on
title('Left Foot ZMP Condition (Pitch)')
xlabel('t (s)', 'Interpreter','latex','Fontsize',14);
ylabel('Fe (Nm)','Interpreter','latex','Fontsize',14);
if isSavePlot
    print(h,'-dpng','-r864',['../results/',behaviorName,'/simulation/left_pitch_ZMP']);
end

h = figure(11); clf
if ~strcmp(get(0,'DefaultFigureWindowStyle'),'docked')
    set(h,'position',[200 200 1080 640]);
end
for k = 1:nSteps
    for j = 1:nDomains
        if isempty(calcs_steps{j,k})
            continue;
        end
        switch j
            case 1 % right double support
                t  = calcs_steps{j,k}.t;
                Fe = calcs_steps{j,k}.Fe;
                ah(1) = plot(t,Fe(10,:)*wf,'c','LineWidth',2); hold on;
                ah(2) = plot(t,Fe(9,:)*wf/2,'r--','LineWidth',2); hold on;
                ah(3) = plot(t,-Fe(9,:)*wf/2,'b--','LineWidth',2); hold on;
            case 3 % left double support
                t  = calcs_steps{j,k}.t;
                Fe = calcs_steps{j,k}.Fe;
                plot(t,Fe(4,:)*wf,'c','LineWidth',2); hold on;
                plot(t,Fe(3,:)*wf/2,'r--','LineWidth',2); hold on;
                plot(t,-Fe(3,:)*wf/2,'b--','LineWidth',2); hold on;
            
            case 4 % left single supoort
                t  = calcs_steps{j,k}.t;
                Fe = calcs_steps{j,k}.Fe;
                plot(t,Fe(4,:)*wf,'c','LineWidth',2); hold on;
                plot(t,Fe(3,:)*wf/2,'r--','LineWidth',2); hold on;
                plot(t,-Fe(3,:)*wf/2,'b--','LineWidth',2); hold on;
        end
    end
end
ax = gca;
legend(ax,ah,{'Roll Moment','Upper Bound','Lower Bound'},1,...
    'FontSize',10,'Interpreter','Latex','Location','eastoutside','Box','Off');
grid on
title('Left Foot ZMP Condition (Roll)')
xlabel('t (s)', 'Interpreter','latex','Fontsize',14);
ylabel('Fe (Nm)','Interpreter','latex','Fontsize',14);
if isSavePlot
    print(h,'-dpng','-r864',['../results/',behaviorName,'/simulation/left_roll_ZMP']);
end


mu = model.frictionCoeff;
h = figure(12); clf
if ~strcmp(get(0,'DefaultFigureWindowStyle'),'docked')
    set(h,'position',[200 200 1080 640]);
end
for k = 1:nSteps
    for j = 1:nDomains
        if isempty(calcs_steps{j,k})
            continue;
        end
        switch j
            case 1 % right double support
                t  = calcs_steps{j,k}.t;
                Fe = calcs_steps{j,k}.Fe;
                ah(1) = plot(t,Fe(7,:),'c','LineWidth',2); hold on;
                ah(2) = plot(t,Fe(8,:),'g','LineWidth',2); hold on;
                ah(3) = plot(t,Fe(9,:)*(mu/sqrt(2)),'r--','LineWidth',2); hold on;
                ah(4) = plot(t,-Fe(9,:)*(mu/sqrt(2)),'b--','LineWidth',2); hold on;
            case 3 % left double support
                t  = calcs_steps{j,k}.t;
                Fe = calcs_steps{j,k}.Fe;
                plot(t,Fe(1,:),'c','LineWidth',2); hold on;
                plot(t,Fe(2,:),'g','LineWidth',2); hold on;
                plot(t,Fe(3,:)*(mu/sqrt(2)),'r--','LineWidth',2); hold on;
                plot(t,-Fe(3,:)*(mu/sqrt(2)),'b--','LineWidth',2); hold on;
            
            case 4 % left single supoort
                t  = calcs_steps{j,k}.t;
                Fe = calcs_steps{j,k}.Fe;
                plot(t,Fe(1,:),'c','LineWidth',2); hold on;
                plot(t,Fe(2,:),'g','LineWidth',2); hold on;
                plot(t,Fe(3,:)*(mu/sqrt(2)),'r--','LineWidth',2); hold on;
                plot(t,-Fe(3,:)*(mu/sqrt(2)),'b--','LineWidth',2); hold on;
        end
    end
end
ax = gca;
legend(ax,ah,{'Fx','Fy','Upper Bound','Lower Bound'},1,...
    'FontSize',10,'Interpreter','Latex','Location','eastoutside','Box','Off');
grid on
title('Left Foot Friction Condition')
xlabel('t (s)', 'Interpreter','latex','Fontsize',14);
ylabel('Fe (N)','Interpreter','latex','Fontsize',14);
if isSavePlot
    print(h,'-dpng','-r864',['../results/',behaviorName,'/simulation/left_foot_friction']);
end

h = figure(13); clf
if ~strcmp(get(0,'DefaultFigureWindowStyle'),'docked')
    set(h,'position',[200 200 1080 640]);
end
for k = 1:nSteps
    for j = 1:nDomains
        if isempty(calcs_steps{j,k})
            continue;
        end
        switch j
            case 1 % right double support
                t  = calcs_steps{j,k}.t;
                Fe = calcs_steps{j,k}.Fe;
                ah(1) = plot(t,Fe(1,:),'c','LineWidth',2); hold on;
                ah(2) = plot(t,Fe(2,:),'g','LineWidth',2); hold on;
                ah(3) = plot(t,Fe(3,:)*(mu/sqrt(2)),'r--','LineWidth',2); hold on;
                ah(4) = plot(t,-Fe(3,:)*(mu/sqrt(2)),'b--','LineWidth',2); hold on;
            
            case 2 % right single supoort
                t  = calcs_steps{j,k}.t;
                Fe = calcs_steps{j,k}.Fe;
                plot(t,Fe(1,:),'c','LineWidth',2); hold on;
                plot(t,Fe(2,:),'g','LineWidth',2); hold on;
                plot(t,Fe(3,:)*(mu/sqrt(2)),'r--','LineWidth',2); hold on;
                plot(t,-Fe(3,:)*(mu/sqrt(2)),'b--','LineWidth',2); hold on;
                
            case 3 % left double support
                t  = calcs_steps{j,k}.t;
                Fe = calcs_steps{j,k}.Fe;
                plot(t,Fe(7,:),'c','LineWidth',2); hold on;
                plot(t,Fe(8,:),'g','LineWidth',2); hold on;
                plot(t,Fe(9,:)*(mu/sqrt(2)),'r--','LineWidth',2); hold on;
                plot(t,-Fe(9,:)*(mu/sqrt(2)),'b--','LineWidth',2); hold on;
            
        end
    end
end
ax = gca;
legend(ax,ah,{'Fx','Fy','Upper Bound','Lower Bound'},1,...
    'FontSize',10,'Interpreter','Latex','Location','eastoutside','Box','Off');
grid on
title('Right Foot Friction Condition')
xlabel('t (s)', 'Interpreter','latex','Fontsize',14);
ylabel('Fe (N)','Interpreter','latex','Fontsize',14);
if isSavePlot
    print(h,'-dpng','-r864',['../results/',behaviorName,'/simulation/right_foot_friction']);
end

end