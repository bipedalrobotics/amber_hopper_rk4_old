[nDomains,nSteps] = size(calcs_steps);

%% tau
h = figure(11); clf
for k = 1 : nSteps
    for j = 1 : nDomains
        t = calcs_steps{j,k}.t;
        tau = calcs_steps{j,k}.tau;
        plot(t, tau,'-','Linewidth',2, 'Color', 'r');
        hold on; 
        grid on;
    end
end
title('tau(x)')

set(h, 'WindowStyle', 'Docked');
saveName = 'tau';
if savePlot
    if savePlot == 1
        beh = 'sim/';
    else
        beh = 'opt/';
    end
    savefig(h,['../results/Periodic2DRuning/', beh, saveName]);
end


%% com
if isfield(calcs_steps{j,k},'COM')

h = figure(12); clf;
subplot(2,1,1)
for k = 1 : nSteps
    for j = 1 : nDomains
        t = calcs_steps{j,k}.t;
        pcom = calcs_steps{j,k}.COM(1:2,:);
        plot(t, pcom(1,:),'-','Linewidth',2, 'Color', 'r'); hold on;
        plot(t, pcom(2,:),'-','Linewidth',2, 'Color', 'b'); hold on;
        grid on;
    end
end
legend('com X', 'com Z'); title('com');

subplot(2,1,2)
for k = 1 : nSteps
    for j = 1 : nDomains
        t = calcs_steps{j,k}.t;
        vcom = calcs_steps{j,k}.COM(3:4,:);
        plot(t, vcom(1,:),'-','Linewidth',2, 'Color', 'r'); hold on;
        plot(t, vcom(2,:),'-','Linewidth',2, 'Color', 'b'); hold on;
        grid on;
    end
end
legend('com X', 'com Z'); title('com');


set(h, 'WindowStyle', 'Docked');
saveName = 'com';
if savePlot
    if savePlot == 1
        beh = 'sim/';
    else
        beh = 'opt/';
    end
    savefig(h,['../results/Periodic2DRuning/', beh, saveName]);
end

end



%% plot holonomic Constraint forces/torques
t1 = calcs_steps{1}.t;
t2 = calcs_steps{2}.t;

ground_con = [calcs_steps{1}.Fe(1:2, :)];
if ~isempty(calcs_steps{2})
    knees_con = [calcs_steps{1}.Fe(3:4, :), calcs_steps{2}.Fe];
else
    knees_con = [calcs_steps{1}.Fe(3:4, :)];
end

h = figure(13); clf
plot([t1,t2], knees_con); hold on
plot(t1,      ground_con); grid on; title('holoForce')
legend('knee1', 'knee2', 'footX', 'footZ');

set(h, 'WindowStyle', 'Docked');
saveName = 'HoloForce';
if savePlot
    if savePlot == 1
        beh = 'sim/';
    else
        beh = 'opt/';
    end
    savefig(h,['../results/Periodic2DRuning/', beh, saveName]);
end