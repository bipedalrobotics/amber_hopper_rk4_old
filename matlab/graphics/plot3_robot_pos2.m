% function plot3_robot_pos2(x)
x = x0;
pos = jpos_mat(x);
lleg = pos(:,[1 11:19]);
rleg = pos(:,[1 20:28]);
tor = pos(:,1:10);
h = figure(1);clf
set(h,'position',[200 200 240 400])
hold all
xlim([-1 1]);
ylim([-0.5 0.5]);
zlim([-0.5 1.6]);
% grid on
% ground
view(15,3)
patch([x(1)-0.3 x(1)-0.3 x(1)+0.4 x(1)+0.4],[-0.5 0.5 0.5 -0.5],[0 0 0 0],...
    'FaceColor','none','EdgeColor','k')


% plot3(lleg(1,1:6),lleg(2,1:6),lleg(3,1:6),'-bo',...
%     'MarkerFaceColor','k',...
%     'MarkerSize',3,...
%     'LineWidth',2);
lvert = lleg(:,6:end)';
face1 = [1,2,3;
    1,3,4;
    1,4,5;
    1,5,2];
face2 = [2 3 4 5];
patch('Faces',face1,'Vertices',lvert,'FaceColor','b')
patch('Faces',face2,'Vertices',lvert,'FaceColor','b')


% plot3(rleg(1,1:6),rleg(2,1:6),rleg(3,1:6),'-ro',...
%     'MarkerFaceColor','k',...
%     'MarkerSize',3,...
%     'LineWidth',2);
rvert = rleg(:,6:end)';
patch('Faces',face1,'Vertices',rvert,'FaceColor','r')
patch('Faces',face2,'Vertices',rvert,'FaceColor','r')



% plot3(tor(1,1:2),tor(2,1:2),tor(3,1:2),'-go',...
%     'MarkerFaceColor','k',...
%     'MarkerSize',3,...
%     'LineWidth',2);
% tvert = tor(:,2:end)';
% face3 = [2 3 4 5;
%     2 3 7 6;
%     3 4 8 7;
%     4 5 9 8;
%     5 2 6 9];
% face4 = [6 7 8 9];
% patch('Faces',face1,'Vertices',tvert,'FaceColor','c')
% patch('Faces',face3,'Vertices',tvert,'FaceColor','c')
% patch('Faces',face4,'Vertices',tvert,'FaceColor','y')

axis equal
axis off
% view(30,20)
% view(30,15)
view(0,0)

% end