function [] = aniplot_tile(fs)

if nargin < 1
    fs = 7;
end


projName = get_project_name();
% load(['../figures/simulation_results/',projName,'/sim_data']);
load('/home/ayonga/Dropbox/Durus/figures/simulation_results/case7_pointfeet_underactuated/stable_limit_cycles_0707/sim_data.mat');
dqe = zeros(robotCfg.ndof,1);
p   = domains{1}.p;
p_ext = zeros(45,1);
p_ext(1:3) = p(1:3);
a_ext = a_expand(reshape(p(4:end),6,5));
p_ext(4:end) = a_ext(:);




ts = calcs_steps{1}.t;
ts = ts - ts(1);
qs = calcs_steps{1}.q_anim;
[Et,Eq] = even_sample(ts,qs,fs/ts(end));

ll = 1;
fig_hl = figure(1000);
set(fig_hl,'Position', [20 80 1000 250])
clf;
% plot([-1.5 1.5], [0 0], 'b:');
hold on;
% sleg = plot([0 0], 'r','LineWidth',2);
% nleg = plot([0 0], 'b','LineWidth',2);
% torso = plot([0 0], 'g','LineWidth',2);
anim_axis=[-0.6 5.5 -0.1 1.5];
axis off
axis(anim_axis)
axis equal
grid

hold on;

line([-2 12],[-0.01 -0.01],'Color','k','LineWidth',4);
step = 0.7;
    ncalcs = length(Et);
    for i = 1:ncalcs
        % position
        %                 qe = calcs.qe(:, i);
        qe = Eq(:,i);
        x  = [qe;dqe];
        pos = jpos_mat(x,p_ext);
        pcm = pe_com_vec(x,p_ext);
        p_sf = pe_sfoot_vec(x,p_ext);
        p_nsf = pe_nsfoot_vec(x,p_ext);
        
        
        pos(1,:) = pos(1,:) + (i-1)*step;
        pcm(1) = pcm(1) + (i-1)*step;
        p_sf(1) = p_sf(1) + (i-1)*step;
        p_nsf(1) = p_nsf(1) + (i-1)*step;
        
        
        plot(pos(1,1:4),pos(2,1:4),'r','LineWidth',2);        
        plot(pos(1,6:end),pos(2,6:end),'b','LineWidth',2);        
        plot(pos(1,4:5),pos(2,4:5),'g','LineWidth',2);
        
        
        %         plot([pcm(1) p_sf(1)],[pcm(2) p_sf(2)],'c--','LineWidth',1);
        %         plot([pcm(1) p_nsf(1)],[pcm(2) p_nsf(2)],'c--','LineWidth',1);
        
        % Center of mass
        w1 = linspace(0,pi/2,11);
        w2 = linspace(pi/2,pi,11);
        w3 = linspace(pi,3*pi/2,11);
        w4 = linspace(3*pi/2,2*pi,11);
        w = [w1;w2;w3;w4];
        radius=.04;
        xx = radius*[zeros(4,1) cos(w) zeros(4,1)]+pcm(1);
        yy = radius*[zeros(4,1) sin(w) zeros(4,1)]+pcm(2);
        pp1 = patch(xx(1,:),yy(1,:),'k');
        pp2 = patch(xx(2,:),yy(2,:),'k');
        pp3 = patch(xx(3,:),yy(3,:),'k');
        pp4 = patch(xx(4,:),yy(4,:),'k');
        set(pp1,'FaceColor','none');
        set(pp3,'FaceColor','none');
%         
%         set(sleg, 'XData', pos(1,1:4), 'YData', pos(2,1:4), ...
%             'erasemode', 'none');
%         set(nleg, 'XData', pos(1,6:end), 'YData', pos(2,6:end), ...
%             'erasemode', 'none');
%         set(torso, 'XData', pos(1,4:5), 'YData', pos(2,4:5), ...
%             'erasemode', 'none');
        
        %         new_axis=anim_axis+[pos(1,4) pos(1,4) 0 0];
        %         axis(new_axis)
        
        
        %         drawnow();
        
    end
    
    
print(fig_hl,'-depsc2','-r100',['../figures/simulation_results/',projName,'/anim_tiles']);
    
end