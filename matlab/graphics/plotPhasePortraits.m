function plotPhasePortraits(model, calcs_steps,isSavePlot)

if nargin < 3
    isSavePlot = false;
end

calcs_full = [calcs_steps{:}];
out = horzcat_fields_domains(calcs_full, true);
jointName = model.jointName;

% joint_lists = [13 20]

%% plot phase portrait
h = figure(21);clf;
for i = model.qIndices'
    plot(out.qe(i,1:end-1),out.dqe(i,1:end-1),'LineWidth',2);
    hold on;
end

grid on
legend(jointName(model.qIndices),'Location','eastoutside','Box','Off',...
    'Interpreter','latex','Fontsize',10);
xlabel('$q$','Interpreter','latex','Fontsize',14);
ylabel('$\dot{q}$','Interpreter','latex','Fontsize',14);
title('Joint Phase Portrait')

set(h, 'WindowStyle', 'Docked');
saveName = 'phase_portrait';
if isSavePlot
    if isSavePlot == 1
        beh = 'sim/';
    else
        beh = 'opt/';
    end
    savefig(h,['../results/Periodic2DRuning/', beh, saveName]);
end

end