function [] = plotDomainLines(calcs_steps)

calcs_steps_flat = calcs_steps(:);
for i = 1:length(calcs_steps_flat)
    calcs = calcs_steps_flat{i};
    if isempty(calcs) || isempty(calcs.t) || ~isfield(calcs, 'vlineSpec')
        continue;
    end
    t0 = calcs.t(1);
    spec = calcs.vlineSpec{1};
    vline(t0, spec{:});
end

end
