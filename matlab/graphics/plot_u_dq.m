function plot_u_dq(model, domains, calcs_steps, isSavePlot, behaviorName)

if nargin < 3
    isSavePlot = false;
end
if nargin < 4
    behaviorName = 'tmp';
end

[nDomains,nSteps] = size(calcs_steps);

cmap = colormap('lines');
calcs_full = [calcs_steps{:}];
out = horzcat_fields_domains(calcs_full, true);

u  = out.u / 4;
dq = out.dqe(4:5, :) * 4;
pow = [u(1,:).*dq(1,:); u(2,:).*dq(2,:)];

h = figure(61); clf;
plot(u(1,:), dq(1,:)); hold on;
plot(u(2,:), dq(2,:)); 
title('motor torqur & speed'); grid on;
legend('fh', 'rh')

set(h, 'WindowStyle', 'Docked');
saveName = 'torques_velocity';
if isSavePlot
    if isSavePlot == 1
        beh = 'sim/';
    else
        beh = 'opt/';
    end
    savefig(h,['../results/Periodic2DRuning/', beh, saveName]);
end

%% plot joint torques
h = figure(62); clf;
plot(out.t, pow(1,:)); hold on;
plot(out.t, pow(2,:)); 
title('motor power'); grid on;
legend('fh', 'rh')

set(h, 'WindowStyle', 'Docked');
saveName = 'power';
if isSavePlot
    if isSavePlot == 1
        beh = 'sim/';
    else
        beh = 'opt/';
    end
    savefig(h,['../results/Periodic2DRuning/', beh, saveName]);
end

end