function anim_plot_3D(model, calcs_step, isSaveMovie, frameRate, doSleep)
% modified by Wen-loong Ma for hopper running.

if nargin < 3
    isSaveMovie = false;
end
if nargin < 4
    frameRate = 60;
end
if nargin < 5
    doSleep = true;
end

if isSaveMovie
    writerObj = VideoWriter('../results/slip.avi');
    writerObj.FrameRate = frameRate;
    open(writerObj);
end
ne = 4; a = 0.27; ro = 0.02;
%% compute the trajectory data
[nDomains, nSteps] = size(calcs_step);
rLeg_steps = cell(nDomains, nSteps);
fLeg_steps = cell(nDomains, nSteps);
tor_steps  = cell(nDomains, nSteps);
ts = cell(nDomains, nSteps);
calc_last = [];
for k = 1 : nSteps
    for j = 1:nDomains
        if isempty(calcs_step{j, k})
            continue;
        end
        t_domain = calcs_step{j,k}.t;
        x_domain = calcs_step{j,k}.x;
        [ts{j,k},xs] = even_sample(t_domain,x_domain,frameRate);
        %         xs = calcs_step{j,k}.x;
        n_pts = size(xs,2);
        rLeg_data = cell(n_pts,1);
        fLeg_data = cell(n_pts,1);
        tor_data  = cell(n_pts,1);        
        for i = 1:n_pts
            x = xs(:,i);
            pos3D = jpos_mat(x);
            pos = pos3D([1 3],:);
            % [x_s, y_s] = spring(pos(1,1),pos(2,1),pos(1,2),pos(2,2),ne,a,ro);
            % [x_ns, y_ns] = spring(pos(1,8),pos(2,8),pos(1,9),pos(2,9),ne,a,ro);
            % sleg = [x_s, pos(1,2:4); y_s, pos(2,2:4)];
            rLeg = [pos(1, [1,3,5,7]); pos(2, [1,3,5,7])];
            % nsLeg = [pos(1,6:8), x_ns; pos(2,6:8),y_ns];
            fLeg = [pos(1, [2,3,4,6]); pos(2, [2,3,4,6])];
            
            tor  = [pos(1,3), pos(2,3)]; %center of torso
            
            rLeg_data{i} = rLeg;
            fLeg_data{i} = fLeg;
            tor_data{i}  = tor;
        end
        rLeg_steps{j,k} = rLeg_data;
        fLeg_steps{j,k} = fLeg_data;
        tor_steps{j,k}   = tor_data;
        
        calc_last = calcs_step{j,k};
    end
    
end

x0 = calcs_step{1,1}.x(:,1);
xf = calc_last.x(:,end);

x_min = x0(1) - 0.3;
x_max = xf(1) + 1;




%% plot initial position
% sleg = sleg_steps{1,1}{1};
% nsleg = nsleg_steps{1,1}{1};
% tor  = tor_steps{1,1}{1};

%% initialize the plot
fig_hl = figure(1000); clf
set(fig_hl, 'WindowStyle', 'normal');
set(fig_hl,'Position', [200 80 560 720]); % for making movies
clf;

hold all
plot([-1.5 2.0], [0 0], 'k-','LineWidth',4);
hold on;
rLeg_line = plot([0 0], 'r','LineWidth',3);
fLeg_line = plot([0 0], 'b','LineWidth',3);
%torso_line = plot([0 0], 'g','LineWidth',3);
hold off;
anim_axis = [-0.6 1.5 -0.1 1.5];
axis off
axis(anim_axis)
axis equal
grid
    

%% ground
%patch([x_min x_min x_max x_max],[-0.5 0.5 0.5 -0.5],[-0.1 -0.1 -0.1 -0.1],[0.2 0.5 0.4]); hold on;
%patch([x_min x_min x_max x_max],[-0.5 0.5 0.5 -0.5],[0 0 0 0],[0.2 0.5 0.4]); hold on;
%patch([x_min x_min x_max x_max],[0.5 0.5 0.5 0.5],[0 -0.1 -0.1 0],[0.2 0.5 0.4]); hold on;
%patch([x_min x_min x_max x_max],[-0.5 -0.5 -0.5 -0.5],[0 -0.1 -0.1 0],[0.2 0.5 0.4]); hold on;

%ground_front = patch([-0.6 -0.6 -0.6 -0.6],...
%    [0.5 0.5 -0.5 -0.5],[0 -0.1 -0.1 0],[0.2 0.5 0.4]); hold on
%ground_back = patch([0.7 0.7 0.7 0.7],...
%    [0.5 0.5 -0.5 -0.5],[0 -0.1 -0.1 0],[0.2 0.5 0.4]); hold on

% set(gcf,'Resize','off');
%set(gcf,'Renderer','zbuffer');


% patch('Faces',face4,'Vertices',tvert,'FaceColor','y')


%% draw now
tic();
lastTime = toc();
frameDt = 1 / frameRate;

for k = 1:nSteps
    for j = 1:nDomains
        if isempty(calcs_step{j, k})
            continue;
        end
        
        rLeg_data = rLeg_steps{j,k};
        fLeg_data = fLeg_steps{j,k};
        tor_data  = tor_steps{j,k}; 
        %ts_data = ts{j, k};
        n_pts     = numel(rLeg_data);
        
        for i = 1:n_pts
            rLeg = rLeg_data{i};
            fLeg = fLeg_data{i};
            tor  = tor_data{i};
            
            set(rLeg_line, 'XData', rLeg(1,:), 'YData', rLeg(2,:));
            set(fLeg_line, 'XData', fLeg(1,:), 'YData', fLeg(2,:));
            %set(torso_line, 'XData', tor(1,:), 'YData', tor(2,:));
            
            new_axis = anim_axis + [tor(1,1) tor(1,1) 0 0];
            %set(ground_back,  'XData', new_axis(1)*ones(1,4));
            %set(ground_front, 'XData', new_axis(2)*ones(1,4));
            axis(new_axis);
            
            drawnow;
            
            if doSleep
                dt = toc() - lastTime;
                sleepTime = frameDt - dt;
                if sleepTime > 0
                    pause(sleepTime);
                end
            end
            lastTime = lastTime + frameDt;
            
            if isSaveMovie
                frame = getframe(gcf);
                writeVideo(writerObj,frame);
            end
        end
    end
    
end


if isSaveMovie
    close(writerObj);
end
end