function plotStates(model, calcs_steps, isSavePlot, behaviorName)

if nargin < 3
    isSavePlot = false;
end

if nargin < 4
    behaviorName = 'tmp';
end

calcs_full = [calcs_steps{:}];
out = horzcat_fields_domains(calcs_full, true);
jointName = model.jointName;

index = [1,2];
plot_position(311, 'Base position', index, behaviorName, jointName, out, isSavePlot)
plot_vel(312, 'Base velocity', index, behaviorName, jointName, out, isSavePlot)

index = [3:7];
plot_position(313, 'Joint position', index, behaviorName, jointName, out, isSavePlot)
plot_vel(314, 'Joint velocity', index, behaviorName, jointName, out, isSavePlot)

%% plot nsf_x & nsf_z
for i = 1 : length(out.t)
    xx = [out.qe(:,i); zeros(7,1)];
    pos = jpos_mat(xx);
    foot(:, i) = pos([1,3], end);
end
h = figure(317); clf
plot(out.t, foot(1,:), out.t, foot(2,:)); grid on
legend('foot X', 'foot Z')
set(h, 'WindowStyle', 'Docked');


end


%%
function [] = plot_vel(n, name, index, behaviorName, jointName, out, isSavePlot)

h = figure(n); clf;

plot(out.t, out.dqe(index,:),'LineWidth',2);
grid on;
legend(jointName(index),'Location','eastoutside','Box','Off',...
       'Interpreter','latex','Fontsize',10);
title(name)
ylabel('$q$','Interpreter','latex','Fontsize',14);
xlabel('t', 'Interpreter','latex','Fontsize',14);

set(h, 'WindowStyle', 'Docked');
saveName = ['dq', int2str(index(1))];
if isSavePlot
    if isSavePlot == 1
        beh = 'sim/';
    else
        beh = 'opt/';
    end
    savefig(h,['../results/Periodic2DRuning/', beh, saveName]);
end

end

%%
function [] = plot_position(n, name, index, behaviorName, jointName, out, isSavePlot)

h = figure(n); clf;

plot(out.t, out.qe(index,:),'LineWidth',2);
grid on;
legend(jointName(index),'Location','eastoutside','Box','Off',...
       'Interpreter','latex','Fontsize',10);
title(name)
ylabel('$q$','Interpreter','latex','Fontsize',14);
xlabel('t', 'Interpreter','latex','Fontsize',14);

set(h, 'WindowStyle', 'Docked');
saveName = ['q', int2str(index(1))];
if isSavePlot
    if isSavePlot == 1
        beh = 'sim/';
    else
        beh = 'opt/';
    end
    savefig(h,['../results/Periodic2DRuning/', beh, saveName]);
end

end