function [] = aniplot_limitcyles_outputs(fs, saveMovie)

if nargin < 1
    fs = 1000;
end

if nargin < 2
    saveMovie = false;
end
projName = get_project_name();
load(['../figures/simulation_results/',projName,'/sim_data']);

dqe = zeros(robotCfg.ndof,1);
p   = domains{1}.p;
ll = 1;

    colors_default = [...
    0         0    1.0000
    0    0.5000         0
    1.0000         0         0
    0    0.7500    0.7500
    0.7500         0    0.7500
    0.7500    0.5000         0
    0.2500    0.2500    0.9500
    0.7500    0.2500    0.5000
    0.25      0.75      0.5
    0.5       0.5       0.5];
    
    ne = 4; a = 0.27; ro = 0.02;
    
    nSteps = numel(calcs_steps);
    sampled_calcs = cell(nSteps,1);
    pe_nsf = [0;0];
    for k = 1:nSteps
        calc = struct();
        ts = calcs_steps{k}.t;
        qs = calcs_steps{k}.q_anim(3:11,:);
        dqs = calcs_steps{k}.dq_anim(3:11,:);
        [~,calc.qs] = even_sample(ts,qs,fs);
        [~,calc.dqs] = even_sample(ts,dqs,fs);
        [~,calc.ya] = even_sample(ts,calcs_steps{k}.ya,fs);
        [calc.ts,calc.qe] = even_sample(ts,calcs_steps{k}.q_anim,fs);
        
        
        ncalcs = length(calc.ts);
        calc.set_point = zeros(1,ncalcs);
        calc.pcm  = zeros(2,ncalcs);
        calc.sleg_pos = zeros(ncalcs,2,13);
        calc.nsleg_pos = zeros(ncalcs,2,13);
        calc.tor_pos = zeros(ncalcs,2,2);
        for i = 1:ncalcs
            x  = [calc.qe(:,i);dqe];
            pos = jpos_mat(x,p);
            pos(1,:) = pos(1,:) + pe_nsf(1);
            calc.set_point(i) = pos(1,4);
            calc.pcm(:,i) = pe_com_vec(x,p);
            
            [x_s, y_s] = spring(pos(1,1),pos(2,1),pos(1,2),pos(2,2),ne,a,ro);
            calc.sleg_pos(i,:,:) = [x_s, pos(1,2:4); y_s, pos(2,2:4)];
            
            [x_ns, y_ns] = spring(pos(1,8),pos(2,8),pos(1,9),pos(2,9),ne,a,ro);
            calc.nsleg_pos(i,:,:) = [pos(1,6:8), x_ns; pos(2,6:8),y_ns];
            
            calc.tor_pos(i,:,:) = pos(:,4:5);
        end
        pe_nsf = pos(:,end);
        sampled_calcs{k} = calc;
    end
    
    sampled_out =  horzcat_fields_domains([sampled_calcs{:}]);
    qs  = sampled_out.qs;
    dqs = sampled_out.dqs;
    ya  = sampled_out.ya;
    ts  = sampled_out.ts;
    %     set_point = sampled_out.set_point;
    sleg_pos = [];
    nsleg_pos = [];
    tor_pos = [];
    for k = 1:nSteps
        sleg_pos = [sleg_pos;sampled_calcs{k}.sleg_pos];
        nsleg_pos = [nsleg_pos;sampled_calcs{k}.nsleg_pos];
        tor_pos = [tor_pos;sampled_calcs{k}.tor_pos];
    end
    
    
    [~,q_a] = even_sample(out.t,out.q_anim(3:11,:),60);
    [~,dq_a] = even_sample(out.t,out.dq_anim(3:11,:),60);
    [t_a,y_a] = even_sample(out.t,out.ya,60);
    
    ncalcs = length(ts);
    fig_hl = figure(1000);
    set(fig_hl,'Position', [20 80 1280 720])
    clf;
    
    subplot('Position',[0.05 0.6 0.4 0.35]);
    hold on;
    plot(q_a',dq_a','LineWidth',2); 
    points = plot(qs(:,1),dqs(:,1),'o','MarkerSize',6,'LineWidth',4);    
    hold off;
    anim_axis=[-0.9 1.5 -6 6];
    axis(anim_axis)
    title('Limit Cycles','FontSize',20,'Interpreter','Latex');
    xlabel('$q(rad)$','FontSize',20,'Interpreter','Latex');
    ylabel('$\dot{q}(rad/s)$','FontSize',20,'Interpreter','Latex');
    set(gca,'FontSize',16);
    grid
    
    subplot('Position',[0.55 0.6 0.4 0.35]);
    hold on;
    plot(t_a,y_a,'LineWidth',2); 
    outputs = plot(ts(1),ya(:,1),'bo','MarkerSize',6,'LineWidth',4);
    title('Outputs','FontSize',20,'Interpreter','Latex');
    xlabel('$t(s)$','FontSize',20,'Interpreter','Latex');
    ylabel('$y(rad)$','FontSize',20,'Interpreter','Latex');
    set(gca,'FontSize',16);
    hold off;
    anim_axis=[0 ts(end) -0.5 1.2];
    axis(anim_axis)
    grid
    
%     set(gca,'position',[0.5,0.02,0.7,0.96]);
    subplot('Position',[0.05 0.05 0.9 0.5]);
    plot([-1 4], [0 0], 'k-','LineWidth',4);
    hold on;
    sleg = plot([0 0], 'r','LineWidth',3);
    nleg = plot([0 0], 'b','LineWidth',3);
    torso = plot([0 0], 'g','LineWidth',3);
    hold off;
    anim_axis=[-.6 4 -0.1 1.5];
    axis off
    axis(anim_axis)
    axis equal
    grid
%     set(gca,'position',[0.4,0.02,0.7,0.96]);
    tic
    for i = 1:ncalcs
        % position
        %                 qe = calcs.qe(:, i);
        
        set(points, 'XData', qs(:,i)', 'YData', dqs(:,i)');
        set(outputs, 'XData', ts(i)*ones(1,4), 'YData', ya(:,i)');
        
        set(sleg, 'XData', sleg_pos(i,1,:), 'YData', sleg_pos(i,2,:));
        set(nleg, 'XData', nsleg_pos(i,1,:), 'YData', nsleg_pos(i,2,:));
        set(torso, 'XData', tor_pos(i,1,:), 'YData', tor_pos(i,2,:));
        
%         new_axis=anim_axis+[set_point(i) set_point(i) 0 0];
%         axis(new_axis)

%         
        
        pause(1/fs);
        if 1
            % Regulate frameIndex rate, use timeFactor to slow down / speed up
            pauseTime = ts(i) - toc();
            if pauseTime >= 0
                pause(pauseTime);
                % Shouldn't be non-zero, except in maybe rare slowdowns
%             else
                continue
            end
            % Skip frames otherwise?
        else
            % Mini pause for funsies
            pause(0.00001);
        end
        if saveMovie
            M(ll)= getframe(gcf);
            %             frame = getframe();
            if ll == 1
                % Setup - not sure if there's a better way to actually have
                % it work
                [gifImg, gifMap] = rgb2ind(M(ll).cdata, 256, 'nodither');
                % Set length... somehow
                % Just use small increment since we're dynamically sizing
                % Gonna be inefficient, but oh wells
                gifImg(1, 1, 1, 2) = 0;
            else
                gifImg(:, :, 1, ll) = rgb2ind(M(ll).cdata, gifMap, 'nodither');
            end
            ll = ll+1;
        end
        
%         if i < ncalcs
%             set(pp1,'Visible','off')
%             set(pp2,'Visible','off')
%             set(pp3,'Visible','off')
%             set(pp4,'Visible','off')
%         end
        drawnow();
    end
    
    
    if saveMovie
        %         movie2avi(M,'proxi_slip','fps',60,'compression','None','quality',100);
        imwrite(gifImg, gifMap, ...
            ['../figures/simulation_results/',projName,'/limit_cycles.gif'], ...
            'DelayTime', 1/60, 'LoopCount', Inf);
    end
    
end