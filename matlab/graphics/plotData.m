% set(0,'DefaultFigureWindowStyle','normal')
savePlot = 0; %1 for sim; 2 for opt

%% 1
plotExtra;

%% 2
plotPhasePortraits(model, calcs_steps, savePlot);

%% 3
plotStates(model, calcs_steps, savePlot);

%% 4
plotOutputs(domains, calcs_steps, savePlot);

%% 5
plotTorques(model, domains, calcs_steps, savePlot);

%% 6
plot_u_dq(model, domains, calcs_steps, savePlot);
