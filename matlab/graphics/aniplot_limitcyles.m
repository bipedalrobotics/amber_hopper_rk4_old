function [] = aniplot_limitcyles(fs, saveMovie)

if nargin < 1
    fs = 1000;
end

if nargin < 2
    saveMovie = false;
end
projName = get_project_name();
load(['../figures/simulation_results/',projName,'/sim_data']);

dqe = zeros(robotCfg.ndof,1);
p   = domains{1}.p;
ll = 1;

    colors_default = [...
    0         0    1.0000
    0    0.5000         0
    1.0000         0         0
    0    0.7500    0.7500
    0.7500         0    0.7500
    0.7500    0.5000         0
    0.2500    0.2500    0.9500
    0.7500    0.2500    0.5000
    0.25      0.75      0.5
    0.5       0.5       0.5];
    
    ne = 4; a = 0.27; ro = 0.02;
    ts = out.t;
    ts = ts - ts(1);
    qs = out.q_anim(3:11,:)';
    dqs = out.dq_anim(3:11,:)';
    [~,qs] = even_sample(ts,qs',fs);
    [~,dqs] = even_sample(ts,dqs',fs);
    [ts,qe] = even_sample(ts,out.q_anim,fs);
    %     ncalcs = length(Et);
    ncalcs = length(ts);
    qs = qs';
    dqs = dqs';
    sleg_pos = cell(ncalcs,1);
    nsleg_pos = cell(ncalcs,1);
    tor_pos = cell(ncalcs,1);
    set_point = zeros(ncalcs,1);
    pcm  = zeros(2,ncalcs);
    for i = 1:ncalcs
        %         qe = out.q_anim(:,i);
        x  = [qe(:,i);dqe];
        pos = jpos_mat(x,p);
        set_point(i) = pos(1,4);
        pcm(:,i) = pe_com_vec(x,p);
        [x_s, y_s] = spring(pos(1,1),pos(2,1),pos(1,2),pos(2,2),ne,a,ro);
        sleg_pos{i} = [x_s, pos(1,2:4); y_s, pos(2,2:4)];
        
        [x_ns, y_ns] = spring(pos(1,8),pos(2,8),pos(1,9),pos(2,9),ne,a,ro);
        nsleg_pos{i} = [pos(1,6:8), x_ns; pos(2,6:8),y_ns];
        
        tor_pos{i} = pos(:,4:5);
    end
    
    fig_hl = figure(1000);
    set(fig_hl,'Position', [20 80 840 360])
    clf;
    %     plot([-1.5 1.5], [0 0], 'k-','LineWidth',4);
    
    subplot(1,2,1);
    hold on;
%     set(gca,'position',[0.02,0.02,0.96,0.96]);
    plot(qs,dqs,'LineWidth',2); 
    points = plot(qs(1,:),dqs(1,:),'o','MarkerSize',6,'LineWidth',2);
    xlim([-0.7 1.5]);
    ylim([-4 5]);
    
    hold off;
    anim_axis=[-0.7 1.5 -4 5];
%     axis off
    axis(anim_axis)
%     axis equal
    grid
%     set(gca,'position',[0.5,0.02,0.7,0.96]);
    subplot(1,2,2);
    plot([-1.5 1.5], [0 0], 'k-','LineWidth',4);
    hold on;
    sleg = plot([0 0], 'r','LineWidth',3);
    nleg = plot([0 0], 'b','LineWidth',3);
    torso = plot([0 0], 'g','LineWidth',3);
    hold off;
    anim_axis=[-0.6 0.7 -0.1 1.5];
    axis off
    axis(anim_axis)
    axis equal
    grid
%     set(gca,'position',[0.4,0.02,0.7,0.96]);
    tic
    for i = 1:ncalcs
        % position
        %                 qe = calcs.qe(:, i);
        
        set(points, 'XData', qs(i,:), 'YData', dqs(i,:), ...
            'erasemode', 'normal');
        
        set(sleg, 'XData', sleg_pos{i}(1,:), 'YData', sleg_pos{i}(2,:), ...
            'erasemode', 'normal');
        set(nleg, 'XData', nsleg_pos{i}(1,:), 'YData', nsleg_pos{i}(2,:), ...
            'erasemode', 'normal');
        set(torso, 'XData', tor_pos{i}(1,:), 'YData', tor_pos{i}(2,:), ...
            'erasemode', 'normal');
        
        new_axis=anim_axis+[set_point(i) set_point(i) 0 0];
        axis(new_axis)

%         
        
        pause(1/fs);
        if 1
            % Regulate frameIndex rate, use timeFactor to slow down / speed up
            pauseTime = ts(i) - toc();
            if pauseTime >= 0
                pause(pauseTime);
                % Shouldn't be non-zero, except in maybe rare slowdowns
%             else
%                 continue
            end
            % Skip frames otherwise?
        else
            % Mini pause for funsies
            pause(0.00001);
        end
        if saveMovie
            M(ll)= getframe(gcf);
            %             frame = getframe();
            if ll == 1
                % Setup - not sure if there's a better way to actually have
                % it work
                [gifImg, gifMap] = rgb2ind(M(ll).cdata, 256, 'nodither');
                % Set length... somehow
                % Just use small increment since we're dynamically sizing
                % Gonna be inefficient, but oh wells
                gifImg(1, 1, 1, 2) = 0;
            else
                gifImg(:, :, 1, ll) = rgb2ind(M(ll).cdata, gifMap, 'nodither');
            end
            ll = ll+1;
        end
        
%         if i < ncalcs
%             set(pp1,'Visible','off')
%             set(pp2,'Visible','off')
%             set(pp3,'Visible','off')
%             set(pp4,'Visible','off')
%         end
        drawnow();
    end
    
    
    if saveMovie
        %         movie2avi(M,'proxi_slip','fps',60,'compression','None','quality',100);
        imwrite(gifImg, gifMap, ...
            ['../figures/simulation_results/',projName,'/limit_cycles.gif'], ...
            'DelayTime', 1/60, 'LoopCount', Inf);
    end
    
end