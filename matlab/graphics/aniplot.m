function [] = aniplot(model, calcs_steps, fs, saveMovie)

if nargin < 2 || isempty(fs)
    fs = 1000;
end

if nargin < 3 || isempty(saveMovie)
    saveMovie = false;
end
% projName = get_project_name();
% if nargin < 3 || isempty(in_file)
%     in_file = ['../figures/simulation_results/',projName,'/sim_data'];
% end


% load(in_file);
dqe = zeros(robotCfg.ndof,1);
p   = domains{1}.p;

ll = 1;

    
    

    ne = 4; a = 0.27; ro = 0.02;
    
    ts = out.t;
    ts = ts - ts(1);
    qs = out.q_anim;
    %             [Et,Ex] = even_sample(ts,qs,100);
    %             ncalcs = length(Et);
    ncalcs = length(ts);
    sleg_pos = cell(ncalcs,1);
    nsleg_pos = cell(ncalcs,1);
    tor_pos = cell(ncalcs,1);
    set_point = zeros(ncalcs,1);
    pcm  = zeros(2,ncalcs);
    for i = 1:ncalcs
        qe = qs(:,i);
        x  = [qe;dqe];
        pos3D = jpos_mat(x);
        pos = pos3D([1 3],:);
        set_point(i) = pos(1,4);
        pcm(:,i) = pe_com_vec(x,p);
        [x_s, y_s] = spring(pos(1,1),pos(2,1),pos(1,2),pos(2,2),ne,a,ro);
        sleg_pos{i} = [x_s, pos(1,2:4); y_s, pos(2,2:4)];
        
        [x_ns, y_ns] = spring(pos(1,8),pos(2,8),pos(1,9),pos(2,9),ne,a,ro);
        nsleg_pos{i} = [pos(1,6:8), x_ns; pos(2,6:8),y_ns];
        
        tor_pos{i} = pos(:,4:5);
    end
    
    
    
    fig_hl = figure(1000);
    set(fig_hl,'Position', [20 80 400 400])
    clf;
    plot([-1.5 1.5], [0 0], 'k-','LineWidth',4);
    hold on;
    sleg = plot([0 0], 'r','LineWidth',3);
    nleg = plot([0 0], 'b','LineWidth',3);
    torso = plot([0 0], 'g','LineWidth',3);
    hold off;
    anim_axis=[-0.6 0.7 -0.1 1.5];
    axis off
    axis(anim_axis)
    axis equal
    grid
    tic
    for i = 1:ncalcs
        % position
        %                 qe = calcs.qe(:, i);
        
        set(sleg, 'XData', sleg_pos{i}(1,:), 'YData', sleg_pos{i}(2,:));
        set(nleg, 'XData', nsleg_pos{i}(1,:), 'YData', nsleg_pos{i}(2,:));
        set(torso, 'XData', tor_pos{i}(1,:), 'YData', tor_pos{i}(2,:));
        
        new_axis=anim_axis+[set_point(i) set_point(i) 0 0];
        axis(new_axis)
        
%         w1 = linspace(0,pi/2,11);
%         w2 = linspace(pi/2,pi,11);
%         w3 = linspace(pi,3*pi/2,11);
%         w4 = linspace(3*pi/2,2*pi,11);
%         w = [w1;w2;w3;w4];
%         radius=.03;
%         xx = radius*[zeros(4,1) cos(w) zeros(4,1)]+pcm(1,i);
%         yy = radius*[zeros(4,1) sin(w) zeros(4,1)]+pcm(2,i);
%         pp1 = patch(xx(1,:),yy(1,:),'k');
%         pp2 = patch(xx(2,:),yy(2,:),'k');
%         pp3 = patch(xx(3,:),yy(3,:),'k');
%         pp4 = patch(xx(4,:),yy(4,:),'k');
%         set(pp1,'FaceColor','none');
%         set(pp3,'FaceColor','none');
%         
        
%         pause(1/fs);
        if 1
            % Regulate frameIndex rate, use timeFactor to slow down / speed up
            pauseTime = ts(i) - toc();
            if pauseTime >= 0
                pause(pauseTime);
                % Shouldn't be non-zero, except in maybe rare slowdowns
            else
                continue
            end
            % Skip frames otherwise?
        else
            % Mini pause for funsies
            pause(0.00001);
        end
        if saveMovie
            %             M(ll)= getframe();
            frame = getframe();
            if ll == 1
                % Setup - not sure if there's a better way to actually have
                % it work
                [gifImg, gifMap] = rgb2ind(frame.cdata, 256, 'nodither');
                % Set length... somehow
                % Just use small increment since we're dynamically sizing
                % Gonna be inefficient, but oh wells
                gifImg(1, 1, 1, 2) = 0;
            else
                gifImg(:, :, 1, ll) = rgb2ind(frame.cdata, gifMap, 'nodither');
            end
            ll = ll+1;
        end
        
%         if i < ncalcs
%             set(pp1,'Visible','off')
%             set(pp2,'Visible','off')
%             set(pp3,'Visible','off')
%             set(pp4,'Visible','off')
%         end
        drawnow();
    end
    
    
    if saveMovie
        %         movie2avi(M,'proxi_slip','fps',60,'compression','None','quality',100);
        imwrite(gifImg, gifMap, ...
            ['../figures/simulation_results/',projName,'/proxi_slip.gif'], ...
            'DelayTime', 1/60, 'LoopCount', Inf);
    end
    
end