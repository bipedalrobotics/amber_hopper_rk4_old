%%% my debug for simulation.
%% tau
index = 1;
tau1 = calcs_steps{index}.tau;
t1   = calcs_steps{index}.t;
index = 2;
tau2 = calcs_steps{index}.tau;
t2   = calcs_steps{index}.t;

figure(11)
plot(t1, tau1, t2, tau2); grid on


%% animate the simulation 
anim_plot_3D(model, calcs_steps, true, 1000);


%% plot data
savePlot = false;
warning off;
plotStates(model, calcs_steps, savePlot);


%% plot outputs
%plotOutputs(domains, calcs_steps, savePlot);



