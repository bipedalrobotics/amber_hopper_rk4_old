function aniplot_part(out,p)

ll = 1;
fig_hl = figure(1000);
set(fig_hl,'Position', [20 80 400 600])
    clf;
    plot([-1.5 1.5], [0 0], 'b:');
    hold on;
    sleg = plot([0 0], 'r','LineWidth',2);
    nleg = plot([0 0], 'b','LineWidth',2);
    torso = plot([0 0], 'g','LineWidth',2);
    hold off;
    anim_axis=[-0.6 0.7 -0.1 1.5];
    axis off
    axis(anim_axis)
    axis equal
    grid
    
    tic;

    
    
    ts = out.t;
    ts = ts - ts(1);
    xs = out.x;
    %             [Et,Ex] = even_sample(ts,qs,100);
    %             ncalcs = length(Et);
    ncalcs = length(ts);
    for i = 1:ncalcs
        % position
        %                 
        x  = xs(:,i);
        pos = jpos_mat(x,p);
        
        
        set(sleg, 'XData', pos(1,1:4), 'YData', pos(2,1:4), ...
            'erasemode', 'normal');
        set(nleg, 'XData', pos(1,6:end), 'YData', pos(2,6:end), ...
            'erasemode', 'normal');
        set(torso, 'XData', pos(1,4:5), 'YData', pos(2,4:5), ...
            'erasemode', 'normal');
        
        new_axis=anim_axis+[pos(1,4) pos(1,4) 0 0];
        axis(new_axis)
        
        
        drawnow();
        pause(0.1);
        if 1
            % Regulate frameIndex rate, use timeFactor to slow down / speed up
            pauseTime = ts(i) - toc();
            if pauseTime >= 0
                pause(pauseTime);
                % Shouldn't be non-zero, except in maybe rare slowdowns
            end
            % Skip frames otherwise?
        else
            % Mini pause for funsies
            pause(0.00001);
        end
        
    end
    
end