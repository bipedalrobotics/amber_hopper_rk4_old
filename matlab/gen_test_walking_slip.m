function [] = gen_test_walking_slip(opts_in)
addpath('../matlab_utilities/matlab');
addpath_matlab_utilities('general', 'sim', 'yaml', 'ros');

if nargin < 1
    opts_in = [];
end
opts_default = struct(...
    'controller', 'IO' ...
    );
opts = struct_overlay(opts_default, opts_in);

tmpfile = get_tmp_file(opts);
sim = load(tmpfile);

calc_last = sim.calcs_steps{end}; % Get near steady-state
gait = struct();
gait.q_min = min(sim.calcs.qe, [], 2);
gait.q_max = max(sim.calcs.qe, [], 2);
gait.dtau_min = min(calc_last.dtau);
gait.dtau_max = max(calc_last.dtau);
gait.dtau_coeffs = polyfit(calc_last.tau, calc_last.dtau, 7);

init = sim.init;

doc = struct();
[~, txt] = system('git rev-parse --short HEAD');
sha = strtrim(txt);
doc.source = {struct('repo', 'durus_2d', 'sha', sha)};

doc.controller = opts.controller;
doc.tolerance = 1e-7;
doc.robot_cfg = init.robotCfg;
[~, doc.robot_cfg.R] = apply_relabel();
doc.robot_cfg.nb = 2;

doc.robot_cfg = xfields(doc.robot_cfg, @int32, ...
    {'ndof', 'nb', 'spring_joint_indices', 'motor_joint_indices'});

doc.init = struct( ...
    'input', struct( ...
        'x_minus', init.x_minus ...
    ), ...
    'output', struct( ...
        'x_plus', init.x_plus ...
    ) ...
);

ndomains = sim.ndomains;
doc.domains = cell(ndomains, 1);
k = 1; % step_index

ntests = 7;

% ref = Ref();
% ref.h = struct();
% ref.h.calcs = {};
% ref.h.calc = {};

for j = 1:ndomains
    tests = cell(ntests, 1);
    
    calcs = sim.calcs_domain_raw{k, j};
    count = length(calcs);
    indices = floor(linspace(1, count, ntests));
    
    for i = 1:ntests
        index = indices(i);
        % expected
        ex = calcs(index);
        in = struct('t', ex.t, 'x', ex.x);
        tests{i} = struct('input', in, 'output', ex);
    end
    %> @todo Make format_domain() function thing
    domain_raw = init.domains{j};
    domain = domain_raw;
    % Remove excess QP stuff
    domain.ep = domain.qp.ep;
    domain = rmfield(domain, 'qp');
    % Add in some more domain info
    domain.nc = size(ex.Je, 1);
    domain.Be = ex.Be;
    [~, ~, ~, domain.actuator_subindices] = calcConstraintJacobian(sim.robotCfg, domain_raw, ex.x);
    % Segments for tau switching
    domain.tau_max = sim.calcs_domain_raw{end, j}(end).tau;
    domain = xfields(domain, @int32, {'output_indices', 'nc', 'actuator_subindices'});
    % Add in gait info
    domain = struct_overlay(domain, gait, struct('Recursive', false, 'AllowNew', true));
    % Output
    domain_struct = struct('config', domain, 'tests', {tests});
    
    doc.domains{j} = domain_struct;
end

name = sprintf('test_walking_slip_%s', opts.controller);
file_path = ros_resolve_local_url(sprintf('package://proxi_cpp_redo/gen/share/%s.yaml', name));
fprintf('Writing test: %s\n', file_path);
yaml_write_file(file_path, doc);

end

function [obj] = xfields(obj, func, fields)
if nargin < 3
    fields = fieldnames(obj);
end
for i = 1:length(fields)
    field = fields{i};
    obj.(field) = func(obj.(field));
end
end

