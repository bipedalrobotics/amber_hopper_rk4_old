function [] = compile
%> To Compile C/C++ files to .mex for all OS
%> Copyright: Georgia Tech, AMBER LAB
%> Authors: Wen-Loong Ma <wenloong.ma@gmail.com>
%
%%% type 1: windows
%%% type 2: mac

%%
warning off;
if ispc == 1
    type = 1; %windows
elseif ismac == 1
    type = 2; %mac
else
    type = 3; %ubuntu
end

path_com = {'build_sim/Periodic2DRuning/',...
            'build_opt/opt_2DRuning/'};
comNature = 1;

%%
t1 = tic;
for k = 1:2
    path_comp = path_com{k};
    
    cd(path_comp);
    all = dir('*.cc');
    n = length(all);

    t2 = tic;
    for i = 1 : n
        file = all(i).name;
        [~, fileName, ~] = fileparts(file);
        disp(['i = ', num2str(i), ', Compiling: ', fileName]);

        if strncmp(fileName, 'J_naturalDynamics', 17) && ~comNature
            continue;
        else
            try
                comp_g(type, file)
            catch
                comp(type, file)
            end
        end

        movefile([fileName,'.cc'], './src/');
        movefile([fileName,'.hh'], './src/');

    end
    toc(t2);

    delete('*.pdb');
    delete('*.lib');
    cd ..
    cd ..

end
beep;
toc(t1);

%%
% load handel; sound(y,Fs)
msgbox('MEX Compile Complete');
warning on;
end



%%
function [] = comp(type, file)
    tic
        if type == 1 
            mex(...
            '-I..\..\include', ...
            '-I..\..\build\eiquadprog', ...
            ['-I', getenv('EIGEN_PATH')], ...
            '-IC:\Program Files\Wolfram Research\Mathematica\10.0\SystemFiles\IncludeFiles\C', ...
            file ...
            );
        elseif type == 2
            mex(...
            '-I../../include', ...
            '-I../../build/eiquadprog', ...
            '-I/Users/WenLoong/Dropbox/1myResearch/LIB/Eigen3.2.5', ...
            '-I/Applications/Mathematica.app/SystemFiles/IncludeFiles/c', ...
            file ...
            );
        else
            mex(...
            '-I../../include', ...
            '-I../../build/eiquadprog', ...
            '-I/home/wma60/Dropbox/1myResearch/LIB/Eigen3.2.5', ...
            '-I/usr/local/Wolfram/Mathematica/10.3/SystemFiles/IncludeFiles/C', ...
            file ...
            );
        end
    toc
end


%%
function [] = comp_g(type, file)
    tic
        if type == 1 
            mex(...
            '-g', ...
            '-I..\..\include', ...
            '-I..\..\build\eiquadprog', ...
            ['-I', getenv('EIGEN_PATH')], ...
            '-IC:\Program Files\Wolfram Research\Mathematica\10.0\SystemFiles\IncludeFiles\C', ...
            file ...
            );
        elseif type == 2
            mex(...
            '-g',...
            '-I../../include', ...
            '-I../../build/eiquadprog', ...
            '-I/Users/WenLoong/Dropbox/1myResearch/LIB/Eigen3.2.5', ...
            '-I/Applications/Mathematica.app/SystemFiles/IncludeFiles/c', ...
            file ...
            );
        else
            mex(...
            '-g',...
            '-I../../include', ...
            '-I../../build/eiquadprog', ...
            '-I/home/wenlong/Dropbox/1myResearch/LIB/Eigen3.2.5', ...
            '-I/usr/local/Wolfram/Mathematica/10.0/SystemFiles/IncludeFiles/C', ...
            file ...
            );
        end
    toc
end