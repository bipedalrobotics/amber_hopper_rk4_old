function [model, domains, controller, simOptions, behaviorConfig] = ...
         loadBehavior(behaviorName, doLoadParams)
% loadBehavior - needs to be rewrite
if nargin < 2 || isempty(doLoadParams)
   doLoadParams = true;
end

cur = [fileparts(mfilename('fullpath')), '/'];
addpath([cur, 'build_sim/', behaviorName]);
   

%% load domain configuration
parent_path = fileparts(pwd);
config_file = fullfile(parent_path,'config','behavior',...
                       strcat(behaviorName,'.yaml'));

% check if the file exists
assert(exist(config_file,'file')==2,...
       'The configuration file not found.\n');

% if exists, read the content and return as a structure
behaviorConfig = cell_to_matrix_scan(yaml_read_file(config_file));


%% create model object
model      = modelConfig(behaviorConfig.model);
controller = behaviorConfig.controller;
simOptions = behaviorConfig.options;

% load domains
nDomains = numel(behaviorConfig.domains);
domains  = cell(nDomains,1);

vlineColorDefaults = get(0, 'DefaultAxesColorOrder');

for i = 1 : nDomains
    domainConfig = behaviorConfig.domains(i);
    % create domain object
    domains{i} = domain(domainConfig.name, i, model);
    if strcmp(controller.type,'QP-CLF')
        domains{i} = configureCLF(domains{i},controller);
    end
    vlineSpec = getfieldd(domainConfig, 'vlineSpec');
    if isempty(vlineSpec)
        index     = mod(i - 1, size(vlineColorDefaults, 1)) + 1;
        vlineSpec = {'Color', vlineColorDefaults(i, :)};
    end
    domains{i}.vlineSpec = vlineSpec;
end
    
domainNames = horzcat({behaviorConfig.domains.name});
for i = 1:nDomains
    domainConfig = behaviorConfig.domains(i);
    if isempty(domainConfig.nextDomain)
        next_domain = struct();
        next_domain.index = -1;
        next_domain.name  = [];
        domains{i} = setNextDomain(domains{i},next_domain);
    else
        next_index = strcmp(domainNames, domainConfig.nextDomain);
        next_domain = domains{next_index};
        domains{i} = setNextDomain(domains{i},next_domain);
    end
end


%% load parameters
if ~doLoadParams
    return;
end

param_config_file = fullfile(parent_path,'config','parameters',behaviorName,...
                             strcat(behaviorConfig.parameters,'.yaml'));    
fprintf('parameters: %s\n',behaviorConfig.parameters);

% check if the file exists
if (exist(param_config_file,'file')==2)

    % if exists, read the content and return as a structure
    params = cell_to_matrix_scan(yaml_read_file(param_config_file));
    if isfield(params,'gait_metrics')
        fprintf(yaml_dump(params.gait_metrics));
    end
    if isfield(params(1),'domain')
        param_domain_names = horzcat({params.domain.name});
    else
        param_domain_names = horzcat({params.name});
    end
    for i=1:nDomains
        param_index = strcmp(param_domain_names, domains{i}.domainName);
        if isfield(params(1),'domain')
            param_domain = params.domain(param_index);
        else
            param_domain = params(param_index);
        end
        domains{i}  = setParameters(domains{i},param_domain);

        % compute p0 and pdot0
        x_plus = domains{i}.params.x_plus;
        p0 = domains{i}.deltaphip(x_plus);
        pdot0 = domains{i}.Jdeltaphip(x_plus)*x_plus(model.dqeIndices);

        domains{i} = setParameters(domains{i},'p0',p0,'pdot0',pdot0);
    end
else
    warning('The configuration file "%s" could not be found.\n', param_config_file);
end

end