function [prob,model] = loadOptProblem(optName)
    % loadBehavior - needs to be rewrite
    % Authors: Ayonga Hereid<ayonga@tamu.edu>
    %          Wen-Loong Ma<wenloong.ma@gmail.com>[Modified for running]
    
    
    %% load domain configuration
    parent_path = fileparts(pwd);
    
    config_file = fullfile(parent_path,'config','opt',...
                           strcat(optName,'.yaml'));
    
    % check if the file exists
    assert(exist( config_file,'file') == 2, ...
           'The configuration file could not be found.\n');
    
    % if exists, read the content and return as a structure
    optConfig = cell_to_matrix_scan(yaml_read_file(config_file));
    
    % add build paths
    addpath(['./build_opt/', optName])
    addpath(['./build_sim/', optConfig.export.behavior]);
    
    % create model object
    model = modelConfig(optConfig.model);
    
    prob = optProblem(optConfig);
    prob = initializeDomains(prob,optConfig.domains,model);
    
    %%% load domains
    domains = prob.domains;
       
    domainNames = horzcat({optConfig.domains.name});
    for i = 1 : prob.nDomain
        domains{i}.nDof = model.nDof;
        
        domainConfig = optConfig.domains(i);
        transition   = domainConfig.transition;
        
        domains{i}.hasImpact = transition.impact;
        domains{i}.isSwapping = transition.relabel;
        
        if isempty(transition.nextDomain)
            next_domain = struct();
            next_domain.index = -1;
            next_domain.name  = [];
            domains{i} = setNextDomain(domains{i},next_domain);
        else
            next_index = strcmp(domainNames, transition.nextDomain);
            next_domain = domains{next_index};
            domains{i} = setNextDomain(domains{i},next_domain);
            
            if transition.impact
                domains{i}.holConstrName = transition.constraints;
                domains{i}.nImpConstr = numel(transition.constraints);
            end
        end
        
        
    end
    
    %% setup variable boudaies
    %  based on config/opt/*.yaml & config/model/*.yaml
    for i = 1 : prob.nDomain
        domainConfig = optConfig.domains(i);
        cBoundary    = optConfig.commonBoundaries;
        dBoundary    = optConfig.domainBoundaries(i);
        
        %%% domain specified boundaries %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        domains{i}.minJointAngles = dBoundary.minDofPos;
             %model.angleLimitMin(model.nBase+1 : model.nDof)];
        domains{i}.maxJointAngles = dBoundary.maxDofPos;
             %model.angleLimitMax(model.nBase+1:model.nDof)];
        
        domains{i}.minJointVelocities = dBoundary.minDofVel;
            % [dBoundary.minBaseDofVel;
            %  dBoundary.minSpringVel(1);
            %  cBoundary.minJointVel * ones(model.nDof-5, 1);
            %  dBoundary.minSpringVel(2);
            % ];
        domains{i}.maxJointVelocities = dBoundary.maxDofVel;
            % [dBoundary.maxBaseDofVel;
            % dBoundary.maxSpringVel(1);
            % cBoundary.maxJointVel * ones(model.nDof-5, 1);
            % dBoundary.maxSpringVel(2);
            % ];
            % model.velocityLimitMax(model.nBase+1 : model.nDof)];
        
        domains{i}.minTimeInterval = dBoundary.minTimeInterval;
        domains{i}.maxTimeInterval = dBoundary.maxTimeInterval;
        
        
        %%% common boundary %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        domains{i}.minJointAccels = cBoundary.minJointAccel;
        domains{i}.maxJointAccels = cBoundary.maxJointAccel;
        domains{i}.minConstrForces = cBoundary.minConstrForces;
        domains{i}.maxConstrForces = cBoundary.maxConstrForces;
        domains{i}.minTorques = cBoundary.minTorque;
        %model.torqueLimitMin(domains{i}.qaIndices);
        domains{i}.maxTorques = cBoundary.maxTorque;
        %model.torqueLimitMax(domains{i}.qaIndices);
        
        domains{i}.minStepLength = cBoundary.minStepLength;
        domains{i}.maxStepLength = cBoundary.maxStepLength;        
        domains{i}.minStepWidth  = cBoundary.minStepWidth;
        domains{i}.maxStepWidth  = cBoundary.maxStepWidth;
        
        domains{i}.minParamPhaseVar = cBoundary.minParamPhaseVar;
        domains{i}.maxParamPhaseVar = cBoundary.maxParamPhaseVar;
        
        domains{i}.minParamRD1 = cBoundary.minParamRD1;
        domains{i}.maxParamRD1 = cBoundary.maxParamRD1;
 
        nParam = domains{i}.nParamRD2/domains{i}.nOutputs;
        minParamRD2 = zeros(domains{i}.nOutputs, nParam);
        maxParamRD2 = zeros(domains{i}.nOutputs, nParam);
        
        for k = 1:domains{i}.nOutputs
            if ismember(k,domains{i}.outputs.actual.nonZeroOutputIndices)
                if isscalar(cBoundary.minParamRD2)
                    minParamRD2(k,:) = cBoundary.minParamRD2 * ones(1,nParam);
                else
                    minParamRD2(k,:) = reshape(cBoundary.minParamRD2,1,nParam);
                end
                if isscalar(cBoundary.maxParamRD2)
                    maxParamRD2(k,:) = cBoundary.maxParamRD2 * ones(1,nParam);
                else
                    maxParamRD2(k,:) = reshape(cBoundary.maxParamRD2,1,nParam);
                end
            end
        end
        domains{i}.minParamRD2 = minParamRD2(:);
        domains{i}.maxParamRD2 = maxParamRD2(:);
        
        domains{i}.auxiliaryConstr = domainConfig.constraints;
        
        %%% target initial states and final states %%%%%%%%%%%%
        if ~isempty(domainConfig.target_q0)
            domains{i}.target_q0 = domainConfig.target_q0;
        else
            domains{i}.target_q0 = [];
        end
        
        if ~isempty(domainConfig.target_dq0)
            domains{i}.target_dq0 = domainConfig.target_dq0;
        else
            domains{i}.target_dq0 = [];
        end
        
        if ~isempty(domainConfig.target_qf)
            domains{i}.target_qf = domainConfig.target_qf;
        else
            domains{i}.target_qf = [];
        end
        
        if ~isempty(domainConfig.target_dqf)
            domains{i}.target_dqf = domainConfig.target_dqf;
        else
            domains{i}.target_dqf = [];
        end
        
    end %/end of boundary setup
        
    prob.domains = domains;
end