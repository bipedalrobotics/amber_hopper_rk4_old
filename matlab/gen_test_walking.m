% generating yaml file for running
sim = horzcat_fields_domains([calcs_steps{:}],true);

calc_last = horzcat_fields_domains([calcs_steps{:,end}],true);

gait = struct();
gait.c = min(sim.qe, [], 2);
gait.q_max = max(sim.qe, [], 2);
gait.dq_min = min(sim.dqe, [], 2);
gait.dq_max = max(sim.dqe, [], 2);

%make sure with shishir the following are used right in c code -w.ma
gait.tau_min = min(calc_last.tau);
gait.tau_max = max(calc_last.tau);
gait.dtau_min = min(calc_last.dtau);
gait.dtau_max = max(calc_last.dtau);
% 
% gait.tau_coeffs_wrt_time  = polyfit(calc_last.t-calc_last.t(1), calc_last.tau, 5);
% gait.dtau_coeffs_wrt_time = polyfit(calc_last.t-calc_last.t(1), calc_last.dtau, 5);
% gait.dtau_coeffs_wrt_tau = polyfit(calc_last.tau, calc_last.dtau, 5);
gait.vcom_average = (calc_last.pcom(1, end) - calc_last.pcom(1, 1)) / ...
                    (calc_last.t(end) - calc_last.t(1));

%%% single support domain
domain_raw = domains{1};

doc = struct();
[~, txt] = system('git rev-parse --short HEAD');
sha = strtrim(txt);
doc.source = {struct('repo', 'durus_2d', 'sha', sha)};

doc.controller = controller.type;
doc.tolerance = 1e-7;
doc.robot_cfg = struct;
doc.robot_cfg.qb_indices  = model.qbIndices;
doc.robot_cfg.dqb_indices = model.dqbIndices;
doc.robot_cfg.qe_indices  = model.qeIndices;
doc.robot_cfg.dqe_indices = model.dqeIndices;
doc.robot_cfg.q_indices   = model.qIndices;
doc.robot_cfg.dq_indices  = model.dqIndices;
nb = 2;
doc.robot_cfg.nb = nb;
doc.robot_cfg.ndof = model.nDof;
doc.robot_cfg.R  = model.footSwappingMatrix((nb+1):end,(nb+1):end);
doc.robot_cfg.spring_joint_indices  = model.qsIndices;
doc.robot_cfg.motor_joint_indices   = domain_raw.qaIndices;

%%% some constants, assign them directly
doc.robot_cfg.L0 = 1.03;
doc.robot_cfg.mTotal = 34.0;
doc.robot_cfg.grav = 9.81;
doc.robot_cfg.kspring = 10000.0;
doc.robot_cfg.Energy0 = 354; %????WTF???
doc.robot_cfg.kdamping = 50;
doc.robot_cfg.Ls = 0.178;

%%
%make sure with shishir the following are used right in c code -w.ma
doc.robot_cfg.p0   = domain_raw.params.p(2); 
doc.robot_cfg.vhip = domain_raw.params.p(1) - domain_raw.params.p(2);

doc.robot_cfg = xfields(doc.robot_cfg, @int32, ...
            {'ndof', 'nb', 'spring_joint_indices', 'motor_joint_indices'});

x_minus  = calc_last.x(:,end);
x_plus   = calc_last.x(:,1);
doc.init = struct('input', struct('x_minus', x_minus), ...
                  'output', struct('x_plus', x_plus) );

ndomains = numel(domains);
for j = 1:ndomains
    
    % ignoring all tests data generation
    t_min = 0;
    t_max = calcs_steps{j, end}.t(end) - calcs_steps{j, end}.t(1); 
    if j == 1
        vhip  = domain_raw.params.p(2) - domain_raw.params.p(3);
        p_opt = domain_raw.params.p(2);
    else
        vhip  = domain_raw.params.p(3) - domain_raw.params.p(1);
        p_opt = domain_raw.params.p(3);
    end
    a_opt = domains{j}.params.a;
    %> this is to make c code won�t complain of using/notUsing ECWF hack
    %a_opt(:,[end+1: end+2]) = 0; 
    
    step_length         = 0; % abs(calcs_steps{1,end}.hd(3,1)); 
                             % no step length considered for running.
    output_indices      = [1, 2, 3, 4];
    actuator_subindices = [1, 2, 3, 4];
    
    tau_max  = max(calcs_steps{j}.tau);
    tau_min  = min(calcs_steps{j}.tau);
    dtau_max = max(calcs_steps{j}.dtau);
    dtau_min = min(calcs_steps{j}.dtau);
    tau_coeffs_wrt_time  = polyfit(calcs_steps{j}.t, calcs_steps{j}.tau, 5);
    dtau_coeffs_wrt_time = polyfit(calcs_steps{j}.t, calcs_steps{j}.dtau, 5);
    dtau_coeffs_wrt_tau  = polyfit(calcs_steps{j}.tau, calcs_steps{j}.dtau, 5);
    
    nc       = numel(domains{j}.holConstrName);
    Ie       = eye(model.nDof);
    Be       = Ie(:,domains{j}.qaIndices);
    SwapLegs = domains{j}.isSwapping;
    
    params = struct('vhip',vhip,...
                    'p_opt',p_opt,...
                    'step_length',step_length,...
                    'a_opt',a_opt);
    
    p_vec = [vhip, p_opt, step_length, a_opt(:)'];
    
    config = struct('type',int8(j), ...
                    'p', p_vec, ...
                    'discrete',params, ...
                    'output_indices',output_indices, ...
                    'nc',nc, ...
                    'Be',Be, ...
                    'SwapLegs',SwapLegs, ...
                    'actuator_subindices',actuator_subindices, ...
                    'ep',controller.ep, ...
                    'tau_max', tau_max, ...
                    'tau_min', tau_min, ...
                    't_max', t_max, ...
                    't_min', t_min, ...
                    'tau_coeffs_wrt_time', tau_coeffs_wrt_time, ...
                    'dtau_coeffs_wrt_time', dtau_coeffs_wrt_time, ...
                    'dtau_coeffs_wrt_tau', dtau_coeffs_wrt_tau ...
                    );
    
    config = struct_overlay(config, gait, struct('Recursive', false, 'AllowNew', true));
    config = xfields(config, @int32, {'output_indices', 'nc', 'actuator_subindices'});
    
    domain_struct  = struct( 'config',config);
    doc.domains{j} = domain_struct;
    
end

%% write the yaml file.
name = sprintf(['../experiment/test_running_', behaviorConfig.parameters]);
file_path = [name,'.yaml'];
fprintf('Writing test: %s\n', file_path);
yaml_write_file(file_path, doc);