function [indices] = getJointIndices(obj, joint_name)
    % getJointIndices - return indices of joints specified by joints name
    % in the input 'joint_name'
    
    if iscell(joint_name) 
        % when specified more than one joints
        N = numel(joint_name);
        
        indices = zeros(1,N);
        
        for i=1:N
            indices(i) = find(strcmpi(obj.jointName,joint_name{i}));
        end
    elseif ischar(joint_name)
        % specified only one joint
        indices = find(strcmpi(obj.jointName,joint_name));
    else
        error('please provide correct information (Joint Name)');
    end
end