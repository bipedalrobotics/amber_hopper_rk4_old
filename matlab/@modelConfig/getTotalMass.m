%% getTotalMass(obj)
function mass = getTotalMass(obj)
% GETTOTALMASS - Return the total mass of the robot model

mass = sum(obj.jointMass);
end
