%% set joint indices
%  this is used in both opt and sim!!!!! -wl.ma

function obj = setJointIndices(obj)
    % SETJOINTINDICES - Setup joint indices for faster operation
    %
    % Copyright 2014 Texas A&M University AMBER Lab
    % Author: Ayonga Hereid <ayonga@tamu.edu>

    
    obj.nState = 2*obj.nDof;
    obj.qeIndices  = cumsum(ones(obj.nDof,1));
    obj.dqeIndices = obj.qeIndices + obj.nDof;


    % Number and indices of floating base coordinates
    obj.qbIndices  = find(strcmp(obj.actuatorType,'base'));
    obj.dqbIndices = obj.qbIndices + obj.nDof;
    obj.nBase       = length(obj.qbIndices);

    % Number and indices of rotary joints
    obj.qrIndices  = find(strcmp(obj.actuatorType,'rotor'));
    obj.dqrIndices = obj.qrIndices + obj.nDof;
    obj.nRotor       = length(obj.qrIndices);

    % Number and indices of spring joints
    obj.qsIndices  = find(strcmp(obj.actuatorType,'spring'));
    obj.dqsIndices = obj.qsIndices + obj.nDof;
    obj.nSpring    = length(obj.qsIndices);

    % Indices of body coordinates
    obj.qIndices   = find(~strcmp(obj.actuatorType,'base'));
    obj.dqIndices  = obj.qIndices + obj.nDof;

    % Indices of waist joints
    %     obj.qWaistIndices = find(strncmpi(obj.jointName,'Waist',5));
    %     % Indices of right leg joints
    %
    %     obj.qRLegIndices = find(strncmpi(obj.jointName,'Right',5));
    %     % Indices of left leg joints
    %
    %     obj.qLLegIndices = find(strncmpi(obj.jointName,'Left',4));
    
    
    %     swappingIndices = [obj.qbIndices;...
    %         obj.qWaistIndices;...
    %         obj.qRLegIndices;...
    %         obj.qLLegIndices];
    
    % find roll joints of both legs
    %     rollJoints = strfind(obj.jointName,...
    %         'Roll');
    %     rollJointIndices = [obj.qbIndices(4);...
    %         find(~cellfun(@isempty,rollJoints))];
    
    
    % find yaw joints of both legs
    %     yawJoints = strfind(obj.jointName,...
    %         'Yaw');
    %     yawJointIndices = [obj.qbIndices(6);...
    %         find(~cellfun(@isempty,yawJoints))];
    
    %     swappingSign = ones(obj.nDof,1);
    %     swappingSign(obj.qbIndices(2)) = -1; % switch sign of y axis position
    %     swappingSign(rollJointIndices) = -1*ones(numel(rollJointIndices),1);
    %     swappingSign(yawJointIndices) = -1*ones(numel(yawJointIndices),1);
    
    %     relabel = diag(swappingSign);
    %     obj.footSwappingMatrix = sparse(relabel(swappingIndices,:));
    %% HACK!!!! Only for hopper spring leg model
    R1 = diag(ones(1,5));

   obj.footSwappingMatrix = blkdiag(eye(2),R1);
end
