classdef modelConfig 
    %MODEL - Contains all information about the rigid body model.
    %
    % Copyright 2014-2015 Texas A&M University AMBER Lab
    % Author: Ayonga Hereid <ayonga@tamu.edu>
    %         Eric Cousineau <eacousineau@gmail.com>
    % Reference: Roy Featherstones - Rigid Body Dynamics Algorithm
    %            <http://royfeatherstone.org/spatial/>
       
    
    properties
        
        % MODELNAME (String) - name of the rigid body model
        modelName@char
        
        % MODELTYPE (String) - This specifies the type of the model, either planar or
        %   spatial.
        modelType@char
        
        config@char % - file path of model configuration YAML file.
        
        %% SVA designated properties
        
        % SVA (struct) - Contains all information required for SVA package
        sva@struct
        % NB (Int) - the number of bodies in the tree, including the 
        %   fixed base. NB is also the number of joints plus the base
        %   coordinates, since we are interested in dynamics model in
        %   floating base coordinates.
        %         NB
        
        % PARENT (Vector) - parent(i) is the body number of the parent of body 
        %   i in the tree.  The fixed base is defined to be body number 0; 
        %   and the remaining bodies are numbered from 1 to NB in any 
        %   order such that each body has a higher number than its 
        %   parent (so parent(i) < i for all i).  Joint i connects from 
        %   body parent(i) to body i.
        %         parent
           
        % JTYPE (Cell Array) - jtype{i} contains either the joint type 
        %   code or the joint-descriptor data structure for joint i.  
        %   Joint type codes are strings  and joint descriptors are 
        %   data structures containing a field called code and an 
        %   optional second field called pars, the former containing 
        %   the joint type code and the latter any parameters that 
        %   are needed by that type of joint.
        %         jtype
        
        % XTREE (Cell Array) - Xtree{i} is the coordinate transform from 
        %   the coordinate system of body parent(i) to the coordinate 
        %   system of the predecessor frame of joint i (which is 
        %   embedded in body parent(i)).  The product of this 
        %   transform with the joint transform for joint i, as 
        %   calculated by jcalc, is the transform from body parent(i) 
        %   coordinates to body i coordinates.
        %         Xtree
        
        % I (Cell Array) - I{i} is the spatial or planar (as appropriate) 
        %   inertia of body i, expressed in body i coordinates.
        %         I
        
        % GRAVITY (Vector) - This is specifying the gravitational acceleration 
        %   vector in base (body 0) coordinates.  d is either 2 or 3, 
        %   depending on whether the underlying arithmetic is planar 
        %   or spatial.  If this field is omitted then a default value 
        %   of [0;0;−9.81] is used in spatial models, and [0;0] in 
        %   planar models.
        %         gravity
        
        
        %% Additional Fields for RBDL (by Eric Cousineau)
        
        % JOINTNAME (Cell Array) - This contains the strings that describes 
        %   the name of the joints
        jointName@cell
        
        % JOINTMASS (Vector) - This field specifies the mass of the joints
        jointMass@double
        
        
        % ACTUATORINERTIA (Vector) - This field specifies the inertia of actuator that
        % should be counted on the calculation of dynamics. See HandC_Extra
        % by Eric for detail.
        actuatorInertia@double
        
        % ACTUATORRATIO (Vector) - This field specifies the gear ratio of
        % the actuator
        actuatorRatio@double
        
        % ACTUATORTYPE (Cell Array) - This field specifies the actuator type of the
        % joints. They can be 'base', 'spring', and 'rotor' for current
        % models
        actuatorType@cell
        
        
        %% Indices and Numbers for fast computation
        
        qeIndices@double    % (Vector) - Angle indices of extended cooridnates
        dqeIndices@double   % (Vector) - Velocity indices of extended coordinates 
        qIndices@double     % (Vector) - Angle indices of body cooridnates
        dqIndices@double    % (Vector) - Velocity indices of body cooridnates
        qbIndices@double    % (Vector) - Angle indices of base cooridnates
        dqbIndices@double   % (Vector) - Velocity indices of base cooridnates
        qsIndices@double    % (Vector) - Configuration indices of spring (prismatic) joints
        dqsIndices@double   % (Vector) - Velocity indices of spring (prismatic) joints
        qrIndices@double    % (Vector) - Configuration indices of rotational joints
        dqrIndices@double   % (Vector) - Velocity indices of rotational joints
        
        qWaistIndices@double % (Vector) - waist joints angles indices
        qRLegIndices@double  % (Vector) - right leg joints angles indices
        qLLegIndices@double  % (Vector) - left leg joints angles indices
        
        footSwappingMatrix@double % (Matrix) - matrix that swaps the left and right joints
        
        nDof@double          % (Int) - Number of DOFs, which is equal to NB normally
        nBase@double         % (Int) - Number of base coordinates
        
        nRotor@double        % (Int) - Number of rotary joints
        nSpring@double       % (Int) - Number of spring joints
        nState@double        % (Int) - Number of states in the system := 2*nDof
        
        
        pos@struct
        
        lengthToe@double        % (Double) - the length from the joint origin to toe
        lengthHeel@double       % (Double) - the length from the joint origin to heel
        widthFoot@double        % (Double) - the width of the foot
        heightFoot@double       % (Double) - the height of the foot
        
        angleLimitMin@double
        angleLimitMax@double
        
        velocityLimitMin@double
        velocityLimitMax@double
        
        torqueLimitMin@double
        torqueLimitMax@double
        
        frictionCoeff = 0.6;
    end
    
    methods
        
        %% model Constructor 
        
        
        function obj = modelConfig(modelInfo)
            % Model - Constructor of 'model' class
            
            %% load domain configuration
            parent_path = fileparts(pwd);
            
            obj.config = fullfile(parent_path,'config','model',...
                                  modelInfo);
            
            % check if the file exists
            assert(exist(obj.config,'file') == 2,...
                'The configuration file could not be found.\n');
            
            % load model configuration
            % configure model
            obj = configureModel(obj, obj.config);
            
            
        end
        
        
        
    end
    
end

