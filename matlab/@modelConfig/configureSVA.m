function obj = configureSVA(obj) 
    % configureSVA - configure model parameters struct for 
    % spatial vector algebra
    %
    % Copyright 2014-2015 Texas A&M University AMBER Lab
    % Author: Ayonga Hereid <ayonga@tamu.edu>
    %         Eric Cousineau <eacousineau@gmail.com>
    % Reference: Roy Featherstones - Rigid Body Dynamics Algorithm
    %            <http://royfeatherstone.org/spatial/>

    % read model configuration file
    config = cell_to_matrix_scan(yaml_read_file(obj.config));
    
    dofs = config.dofs;
    
    % Load model configuration from bodies
    switch obj.modelType
        case 'planar'
            obj = configurePlanarSVA(obj, dofs);
        case 'spatial'
            obj = configureSpatialSVA(obj, dofs);
        otherwise
            error('robotModel:constructor','Undefined model type.');
    end

end