function [obj] = configureSpatialSVA(obj, dofs)
    % configureSVA - configure model parameters struct for 
    % spatial vector algebra
    %
    % Copyright 2014-2015 Texas A&M University AMBER Lab
    % Author: Ayonga Hereid <ayonga@tamu.edu>
    %         Eric Cousineau <eacousineau@gmail.com>
    % Reference: Roy Featherstones - Rigid Body Dynamics Algorithm
    %            <http://royfeatherstone.org/spatial/>
    
    sva_struc = struct();
    % Number of bodies
    sva_struc.NB = length(dofs);
    % model type
    sva_struc.type = 'spatial';
    % Precollocation
    blankc = cell(sva_struc.NB, 1);
    blankm = zeros(sva_struc.NB, 1);
    blankm6 = zeros(6, 6, sva_struc.NB);
    
    sva_struc.jtype = blankc;
    sva_struc.Xtree = blankm6;
    sva_struc.I     = blankm6;
    sva_struc.parent  = blankm;
    % Gravity
    sva_struc.gravity = [0, 0, -9.81];
    sva_struc.axis  = blankm;
    sva_struc.isRevolute = blankm;
    
    
    
    % Spatial axis: 'Rx', 'Ry, 'Rz', 'Px', 'Py', 'Pz'
    axes = 'xyz';
    for i = 1:sva_struc.NB
        dof = dofs(i);
        sva_struc.parent(i) = dof.lambda;
        
        % Specify axis type, and assign prefix
        switch dof.type
            case 'prismatic'
                sva_struc.isRevolute(i) = false;
                prefix = 'P';
                full_index = find(dof.axis);
                index = full_index - 3;
            case 'revolute'
                sva_struc.isRevolute(i) = true;
                prefix = 'R';
                full_index = find(dof.axis);
                index = full_index;
            otherwise
                error('robotModel:: undefined joint type.');
        end
        
        
        
        % Specify whether it is positive or negative axis
        axis_value = dof.axis(full_index);
        if axis_value < 0
            sva_struc.jtype{i} = ['-', prefix, axes(index)];
            axis = -index;
        else
            sva_struc.jtype{i} = [prefix, axes(index)];
            axis = index;
        end
        
        % Assign other configuration
        sva_struc.Xtree(:,:,i) = plux(dof.E, dof.r);
        sva_struc.I(:,:,i) = mcI(dof.mass, dof.com, dof.inertia);
        sva_struc.axis(i) = axis;
        
        
    end
    
    obj.sva = sva_struc;
    
end