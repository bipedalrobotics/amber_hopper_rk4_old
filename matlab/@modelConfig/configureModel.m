function [obj] = configureModel(obj, newConfig)
    % configureModel - configure basic model information
    
    
    % overwrite the old configuration file path with new path
    if nargin > 1
        obj.config = newConfig;
    else
        if isempty(obj.config)
            error('please specifiy the model configuration file.');
        end
    end
    
    
    %% Read config YAML file, returns stucture that contains all
    % model information
    configStr = cell_to_matrix_scan(yaml_read_file(obj.config));
            
    % Model type: planar or spatial
    obj.modelType = configStr.type;
    
    % Model name
    obj.modelName = configStr.name;
    
    % number of DoFs
    obj.nDof = length(configStr.dofs);
    
    
    %% preallocation
    obj.jointMass = zeros(obj.nDof,1);
    obj.jointName = cell(obj.nDof,1);
    obj.actuatorInertia = zeros(obj.nDof,1);
    obj.actuatorRatio   = zeros(obj.nDof,1);
    obj.actuatorType    = cell(obj.nDof,1);
    obj.angleLimitMin = zeros(obj.nDof,1);
    obj.angleLimitMax = zeros(obj.nDof,1);
    
    obj.velocityLimitMin = zeros(obj.nDof,1);
    obj.velocityLimitMax = zeros(obj.nDof,1);
    
    obj.torqueLimitMin = zeros(obj.nDof,1);
    obj.torqueLimitMax = zeros(obj.nDof,1);
    
    %% record some basic information
    for i = 1:obj.nDof
        dof = configStr.dofs(i);
        obj.jointMass(i) = dof.mass;
        obj.jointName{i} = dof.name{1};
        obj.actuatorInertia(i) = dof.actuator.inertia;
        obj.actuatorRatio(i) = dof.actuator.ratio;
        obj.actuatorType{i}  = dof.actuator.type;
        
        % - commented by wl.ma
        %obj.angleLimitMin(i)   = dof.limit.min;
        %obj.angleLimitMax(i)   = dof.limit.max;
        
        %obj.velocityLimitMin(i) = -dof.actuator.maxVelocity;
        %obj.velocityLimitMax(i) =  dof.actuator.maxVelocity;
        
        %obj.torqueLimitMin(i) = -dof.actuator.maxTorque;
        %obj.torqueLimitMax(i) =  dof.actuator.maxTorque;
    end
            
    %% Setup indices for fast operator
    obj = setJointIndices(obj);
    
    %%% Get foot dimensions
    % obj.lengthToe  = configStr.kinConst.lt;
    % obj.lengthHeel = configStr.kinConst.lh;
    % obj.widthFoot  = configStr.kinConst.wf;
    % obj.heightFoot = configStr.kinConst.hf;
    
    obj.pos = configStr.positions;
end