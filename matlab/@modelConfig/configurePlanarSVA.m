function [obj] = configurePlanarSVA(obj, dofs)
    % configureSVA - configure model parameters struct for 
    % spatial vector algebra (planar model)
    %
    % Copyright 2014-2015 Texas A&M University AMBER Lab
    % Author: Ayonga Hereid <ayonga@tamu.edu>
    %         Eric Cousineau <eacousineau@gmail.com>
    % Reference: Roy Featherstones - Rigid Body Dynamics Algorithm
    %            <http://royfeatherstone.org/spatial/>
    
    
    
    sva_struc = struct();
    % Number of bodies
    sva_struc.NB = length(dofs);
    % model type
    sva_struc.type = 'planar';
    % Precollocation
    blankc = cell(sva_struc.NB, 1);
    blankm = zeros(sva_struc.NB, 1);
    blankm3 = zeros(3,3,sva_struc.NB);
    
    sva_struc.jtype = blankc;
    sva_struc.Xtree = blankm3;
    sva_struc.I     = blankm3;
    sva_struc.parent  = blankm;
    % Gravity
    sva_struc.gravity = [0, -9.81];
    
    
    
    obj.isRevolute = blankm;
    obj.jointMass = blankm;
    obj.jointName = blankc;
    obj.axisIndex = blankm;
    obj.actuatorInertia = blankm;
    obj.actuatorType = blankc;
    
    
    
    % Planar axis: 'r', 'px', 'py'
    axes = 'rxy';
    for i = 1:sva_struc.NB
        dof = dofs(i);
        sva_struc.parent(i) = dof.lambda;
        % Specify axis type, and assign prefix
        switch dof.type
            case 'prismatic'
                obj.isRevolute(i) = false;
                axis = [0;dof.axis([1 3])];
                prefix = 'p';
            case 'revolute'
                obj.isRevolute(i) = true;
                axis = [dof.axis(2);0;0];
                prefix = '';
            otherwise
                error('robotModel:: undefined joint type.');
        end
        
        
        
        
        % Find axis index
        index = find(axis);
        assert(isscalar(index));
        % Specify whether it is positive or negative axis
        axis_value = axis(index);
        if axis_value < 0
            sva_struc.jtype{i} = ['-', prefix, axes(index)];
        else
            sva_struc.jtype{i} = [prefix, axes(index)];
        end
        
        % Assign other configuration
        sva_struc.Xtree(:,:,i) = plnr(0, dof.offset([1 3]));
        sva_struc.I(:,:,i)     = mcI(dof.mass, dof.com([1 3]), dof.inertia(2,2));
        
        
    end
    
    obj.sva = sva_struc;

end