
% configure_IPOPT;
% x = opt.Z0;

% parpool(4)
% tic
for i=1:1
    tic
    f = funcs.objective(x);
    toc
end

for i=1:1
    tic
    g = funcs.gradient(x);
    toc
end

%%
for i=1:1
    tic
    c = IpoptConstraints(x, opt.constrArray, opt.dimsConstr, true);
    toc
end

%%
for i=1:1
    tic
    J = funcs.jacobian(x);
    toc
end
% toc