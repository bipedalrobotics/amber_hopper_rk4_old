x0 = zeros(model.nDof * 2, 1);

doc = struct();
[~, txt] = system('git rev-parse --short HEAD');
sha = strtrim(txt);
doc.source = {struct('repo', 'durus_3d', 'sha', sha)};

doc.behavior = behaviorName;
doc.controllers = {};
for j = 1:nDomains
    
    domain = domains{j};
    
    
    parent_path = fileparts(pwd);
    config_file = fullfile(parent_path,'config','domain',...
        strcat(domain.domainName,'.yaml'));
    % check if the file exists
    assert(exist(config_file,'file')==2,...
        'The configuration file could not be found.\n');
    % if exists, read the content and return as a structure
    domainConfig = cell_to_matrix_scan(yaml_read_file(config_file));
    
    h = domain.holConstrFunc(x0);
    if isempty(domain.ya1)
        ya1 = [];
    else
        ya1 = domain.ya1(x0);
    end
    ya2 = domain.ya2(x0);
    
    nd1 = length(ya1);
    nd2 = length(ya2);
    
    % Look at names for prefixes
    % Generate C++ code to associate domains...
    % Values: y_a, y_d, deltaphip, Jdeltaphip, h, Jh, dJh
    
    controller_out = struct();
    controller_out.name = domain.domainName;
    j_next = domain.nextDomain.domainIndex;
    controller_out.next_controller = int32(j_next);
    controller_out.next_controller_name = domain.nextDomain.domainName;
    controller_out.stance_leg = domain.stanceLeg;%##legID
    controller_out.nCon = int32(length(h));
    controller_out.nd1 = int32(nd1);
    controller_out.nd2 = int32(nd2);
    controller_out.index_in_step = int32(domain.indexInStep);
    if domain.indexInStep == 1
        controller_out.is_first_in_step = true;
    else
        controller_out.is_first_in_step = false;
    end
    if domain.indexInStep == domain.numDomainsInStep
        controller_out.is_last_in_step = true;
    else
        controller_out.is_last_in_step = false;
    end
    
    controller_out.qaIndices = int32(domain.qaIndices);
    controller_out.p_init = param_format_gen(p_init{j});
    if strcmp(domain.stanceLeg, 'left')
        x_minus = x_minus_l;
    else
        x_minus = x_minus_r;
    end
    controller_out.x_minus_step = x_minus;
    controller_out.ep = controller.ep;
    controller_out.ik_zero_dynamics_indices = int32(domain.qzIndices);
    % Volatile
    controller_out.p0 = domain.params.p0;
    controller_out.pdot0 = domain.params.pdot0;
    
    is_double_support = controller_out.is_first_in_step;
    if is_double_support
        controller_out.h_zero_indices = int32([4, 5, 6, 10, 11, 12]);
    else
        controller_out.h_zero_indices = int32([4, 5, 6]);
    end
    
    joints = domain.outputs.nonstanceIk.joints;
    if isempty(joints)
        joints = {};
    end
    controller_out.extra_ik_indices = int32(str_indices(joints, model.jointName));
    
%     assert(strcmp(domain.guardType, 'kinematics'));
    controller_out.guard_threshold = domain.guardThld;
    controller_out.guard_direction = int32(domain.guardDir);
    controller_out.has_impact = domain.hasImpact;
    if domain.hasImpact
        controller_out.nImp = int32(domain.nImpConstr);
    else
        controller_out.nImp = int32(0);
    end
    
    doc.controllers{j} = controller_out;
end

%%
file_path = ros_resolve_local_url(controllers_file);
fprintf('Writing yaml: %s\n', file_path);
yaml_write_file(file_path, doc);
