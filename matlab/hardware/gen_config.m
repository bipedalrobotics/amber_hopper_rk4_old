cur = model;

doc = struct();
[~, txt] = system('git rev-parse --short HEAD');
sha = strtrim(txt);
doc.source = {struct('repo', 'durus_3d', 'sha', sha)};

cur_s = rmfield(struct(cur), {'sva'});
doc = struct_overlay(doc, struct(cur_s), struct('Recursive', false, 'AllowNew', true));

doc_str = yaml_dump(doc);
% Get rid of trailing zeros
doc_str = regexprep(doc_str, '\.0(\D)', '$1');

name = sprintf('durus_3d');
file_path = ros_resolve_local_url(sprintf('package://proxi_cpp_redo/gen/share/%s.yaml', name));
fprintf('Writing test: %s\n', file_path);
fid = fopen(file_path, 'w');
fprintf(fid, '%s', doc_str);
fclose(fid);
