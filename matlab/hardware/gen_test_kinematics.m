% From: amber_classic:3215f14:score/matlab/+amber/+test/gen_tests_model.m


nRDof = length(model.qIndices);
nExtDof = model.nDof;

% Choose arbitrary seed
RAND_SEED = 97234;
% See the random number generator:
rng(RAND_SEED);

pos_blank = blank_struct(model.pos(1));

% configure SVA model
model = configureSVA(model);

pos_dof = repmat(pos_blank, nRDof, 1);
for i = 1:nRDof
    ib = model.qIndices(i);
    s = pos_blank;
    s.name = model.jointName{ib};
    s.lambda = ib;
    s.offset = zeros(3, 1);
    pos_dof(i) = s;
end
orig_count = length(model.pos);
is_zero = false(orig_count, 1);
for i = 1:orig_count
    if all(model.pos(i).offset == 0)
        is_zero(i) = true;
    end
end
positions = [pos_dof; model.pos(~is_zero)];
nPos = length(positions);

tests = [];
blankc = cell(nPos, 1);
for i = 1:4
    if i == 1 || i == 2
        qe = zeros(nExtDof, 1);
        dqe = qe;
        if i == 2
            qe(7) = pi / 2;
            label = 'Simple nonzero';
        else
            label = 'Zero';
        end
    elseif i == 3
        % Use somewhat different numbers (with a large LCM) so that it is easier to
        % distinguish position vs. velocity when debugging
        qe = (1:nExtDof) * 1.3;
        dqe = (1:nExtDof) * 1.5;
        label = 'Pattern';
    else
        qe = 20 * randn(nExtDof, 1);
        dqe = 20 * randn(nExtDof, 1);
        label = 'Random seeded';
    end
    xe = [qe; dqe];
    
    % Get kinematics for each dof to compare in Mathematica
    names = blankc;
    rs = blankc;
    Js = blankc;
    dJs = blankc;
    for ip = 1:nPos
        names{ip} = positions(ip).name;
        ib = positions(ip).lambda;
        offset = positions(ip).offset;
        rs{ip} = spatial_position(model.sva, qe, ib, offset, 1:3, []);
        [Js{ip}, dJs{ip}] = calcJacobianSvaHack(model.sva, qe, dqe, ib, offset, false, 1:3, 1:3);
    end
    test = struct();
    test.label = label;
    test.qe = qe;
    test.dqe = dqe;
    test.r = rs;
    test.J = Js;
    test.dJ = dJs;
    tests = struct_assign_loose(tests, test);
end
doc = struct();
[~, txt] = system('git rev-parse --short HEAD');
sha = strtrim(txt);
doc.source = {struct('repo', 'durus_3d', 'sha', sha)};

for ip = 1:nPos
    positions(ip).lambda = int32(positions(ip).lambda);
end
doc.positions = positions;
doc.tests = tests;

test_dir = fullfile('/tmp/tests');
if ~isdir(test_dir)
    mkdir(test_dir);
end
file_path = fullfile(test_dir, 'test_kinematics.yaml');
fprintf('Writing test file: %s\n', file_path);
yaml_write_file(file_path, doc);
