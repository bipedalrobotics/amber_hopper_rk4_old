calcs = horzcat_fields_domains([calcs_steps{:}], true);

names = {'BaseRotY', 'WaistPitch'};

is = str_indices(names, model.jointName);
qt = sum(calcs.qe(is, :), 1);

pad_ratio = 0.1;
[lower, upper] = create_bound(qt, 2, pad_ratio);

fprintf('Torso Clearance [deg]\n');
fprintf('  min: %g\n', lower * 180 / pi);
fprintf('  max: %g\n', upper * 180 / pi);
