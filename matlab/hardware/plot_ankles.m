angles = {
    'LeftAnklePitch', 'LeftAnkleRoll', ...
    'RightAnklePitch', 'RightAnkleRoll' ...
    };

is = [];
for i = 1:length(angles)
    is(end + 1) = str_index(angles{i}, model.jointName);
end


%%
figure(2);
clf();
subplot(2, 1, 1);
plot(ts, qes(:, is));
subplot(2, 1, 2);
plot(ts, dqes(:, is));
legend(model.jointName(is));
