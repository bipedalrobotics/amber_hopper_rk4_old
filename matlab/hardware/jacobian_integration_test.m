function [info] = jacobian_integration_test(hFunc, JFunc, q0, qf, tEnd)

nq = length(q0);

dq0 = (qf - q0) / tEnd;

h0 = hFunc(q0);
hf = hFunc(qf);

nh = length(h0);

% w = [q; h]

    function [dw] = dwFunc(t, w)
        q = w(1:nq);
        dq = dq0;
        dh = JFunc(q) * dq;
        dw = [dq; dh];
    end

w0 = [q0; h0];

sol = ode45(@dwFunc, [0, tEnd], w0);

wf = sol.y(:, end);
qfy = wf(1:nq);
hfy = wf(nq + (1:nh));

info.sol = sol;
info.q0 = q0;
info.h0 = h0;
info.qf = qf;
info.hf = hf;
info.qfy = qfy;
info.hfy = hfy;
info.qfDiff = qfy - qf;
info.hfDiff = hfy - hf;
info.hfDiffMax = max(max(abs(info.hfDiff)));

end