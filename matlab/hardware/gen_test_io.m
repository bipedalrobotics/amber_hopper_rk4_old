doc = struct();
[~, txt] = system('git rev-parse --short HEAD');
sha = strtrim(txt);
doc.source = {struct('repo', 'durus_3d', 'sha', sha)};

doc.setup = struct( ...
	'robot_file', robot_file, ...
	'controllers_file', controllers_file ...
);

doc.tolerance = 5e-4;
doc.domains = cell(1, nDomains);
for j = 1:nDomains
    % Split indices into a certain amount
    domain = domains{j};
    calc = calcs_steps{j, 1};
    ts = calc.t_rel;
    count = length(ts);
    test_count = min(5, count);
    is = round(linspace(1, count, test_count));
    
    doc_domain = struct();
    doc_domain.name = domain.domainName;
    
    param = calc.param(1);
    doc_domain.param = param_format_gen(param);
    
    doc_domain.tests = cell(test_count, 1);
    
    for it = 1:length(is)
        i = is(it);
        input = struct();
        input.t = ts(i);
        input.x = calc.x(:, i);
        input.is_clamped = controller.useClamped;
        
        output = struct();
        output.tau = calc.tau(i);
        output.dtau = calc.dtau(i);
        output.De = calc.De{i};
        output.He = calc.He(:, i);
        output.Be = calc.Be{i};
        output.Je = calc.Je{i};
        output.dJe = calc.dJe{i};
        output.fh = calc.vh(:, i);
        output.gh = calc.gh{i};
        output.Lf = calc.Lf(:, i);
        output.A = calc.A{i};
        output.mu = calc.mu(:, i);
        output.uq = calc.uq(:, i);
        output.ddqe = calc.ddqe(:, i);
        
        % Apply impact map to each state just for kicks and giggles
        next_domain = domains{domain.nextDomain.domainIndex};
        [x_plus, ~, output.impact] = calcResetMap(domain, model, input.x);
        output.impact.x_plus = x_plus;
        
        test = struct('input', input, 'output', output);
        doc_domain.tests{it} = test;
    end
    doc.domains{j} = doc_domain;
end

%%
file_path = ros_resolve_local_url(test_io_file);
fprintf('Writing yaml: %s\n', file_path);
yaml_write_file(file_path, doc);
