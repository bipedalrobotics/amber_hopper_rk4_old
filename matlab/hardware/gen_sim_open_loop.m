doc = struct();
[~, txt] = system('git rev-parse --short HEAD');
sha = strtrim(txt);
doc.source = {struct('repo', 'durus_3d', 'sha', sha)};

%%

out = horzcat_fields_domains([calcs_steps{:}], true);

t0 = out.t(1);
ts = out.t - t0;
qes = out.x(model.qeIndices, :)';
dqes = out.x(model.dqeIndices, :)';

figure(1);
clf();
subplot(2, 1, 1);
plot(ts, qes);
subplot(2, 1, 2);
plot(ts, dqes);

%% Plot envelopes
cur = model;
env = load('./hardware/hw_envelopes.mat');

figure(4);
clf();
subplot(2, 2, 1);
hold('on');
title('LeftAnkle');
poly = env.left_ankle.joint.output;
patch(poly(:, 1), poly(:, 2), [0.8, 0.8, 1], 'EdgeColor', 'none');
ijs = str_indices({'LeftAnklePitch', 'LeftAnkleRoll'}, ...
    cur.jointName);
plot(qes(:, ijs(1)), qes(:, ijs(2)), 'k');
xlabel(cur.jointName(ijs(1)));
ylabel(cur.jointName(ijs(2)));
subplot(2, 2, 2);
hold('on');
plot(ts, qes(:, ijs));
legend(cur.jointName(ijs));

subplot(2, 2, 3);
hold('on');
title('RightAnkle');
poly = env.right_ankle.joint.output;
ijs = str_indices({'RightAnklePitch', 'RightAnkleRoll'}, ...
    cur.jointName);
patch(poly(:, 1), poly(:, 2), [0.8, 0.8, 1], 'EdgeColor', 'none');
plot(qes(:, ijs(1)), qes(:, ijs(2)), 'k');
xlabel(cur.jointName(ijs(1)));
ylabel(cur.jointName(ijs(2)));
subplot(2, 2, 4);
hold('on');
plot(ts, qes(:, ijs));
legend(cur.jointName(ijs));


%%
clear lower upper;
count = numel(calcs_steps);
traj_domains = cell(count, 1);
for i = 1:count
    calcs = calcs_steps{i};
    name = [];
    if exist('calcs_names', 'var')
        name = calcs_names{i};
    else
        domain_index = mod(i - 1, length(domains)) + 1;
        name = domains{domain_index}.domainName;
    end
    stance_leg = 'left'; % left leg
    if strstartswith(lower(name), lower('right'))
        stance_leg = 'right';
    end
    calcs.J = [];
    calce = even_sample_fields(calcs, 100);
    traj_domains{i} = struct(...
        'name', name, ...
        'stance_leg', stance_leg, ...
        'ts', calce.t, ...
        'qes', calce.qe', ...
        'dqes', calce.dqe' ...
        );
end

% has_impact = false;
% % restart_index = -1;
% ndomain_step = length(domains) / 2; % Go from left -> right or whatevs
% restart_index = int32(0); % 0-based

has_impact = true;
ndomain_step = length(domains) / 2; % Go from left -> right or whatevs
restart_index = int32(count - 2 * ndomain_step + 1 - 1); % 0-based

if restart_index ~= -1
    fprintf('Restart %d (%s)  -> %d (%s)\n', count, traj_domains{count}.name, restart_index + 1, traj_domains{restart_index + 1}.name);
else
    fprintf('No repeat\n');
end

trajectory = struct(...
    'domains', {traj_domains}, ...
    'restart_index', restart_index ...
    );

%% Check periodicity
if restart_index ~= -1
    tolerance = 0.01;
    if has_impact
        if restart_index == 0
            warning('Cannot check pre-impact periodicity when restarting at beginning\n');
        else
            dq_tolerance = 0.5;
            x_last = [traj_domains{end}.qes(end, :), traj_domains{end}.dqes(end, :)];
            x_restart = [traj_domains{restart_index + 1 - 1}.qes(end, :), traj_domains{restart_index + 1 - 1}.dqes(end, :)];
            assert(maxabs(x_last(model.qrIndices) - x_restart(model.qrIndices)) < tolerance, 'Position is non-periodic!');
            vel_diff = x_last(model.dqrIndices) - x_restart(model.dqrIndices);
            if maxabs(vel_diff) > dq_tolerance
                warning('Velocity is non-periodic!');
                vel_diff'
            end
        end
    else
        x_last = [traj_domains{end}.qes(end, :), traj_domains{end}.dqes(end, :)];
        x_restart = [traj_domains{restart_index + 1}.qes(1, :), traj_domains{restart_index + 1}.dqes(1, :)];
        assert(maxabs(x_last - x_restart) < tolerance, 'Position and velocity are non-periodic!');
    end
end

%%
doc.trajectory = trajectory;

%%
file_path = ros_resolve_local_url(trajectory_file);
mkdir(fileparts(file_path));
fprintf('Writing yaml: %s\n', file_path);
yaml_write_file(file_path, doc);
