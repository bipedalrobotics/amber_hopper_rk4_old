function [lb, ub] = create_bound(xs, dim, pad_ratio, pad_min)

if nargin < 4
    pad_min = [];
end

lower = min(xs, [], dim);
upper = max(xs, [], dim);

range = upper - lower;
mid = (lower + upper) / 2;

pad = (1 + pad_ratio) .* range;
if ~isempty(pad_min)
    pad(pad < pad_min) = pad_min;
end

lb = mid - pad;
ub = mid + pad;

end
