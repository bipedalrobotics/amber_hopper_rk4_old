% Iterate through different parameter sets, update YAML, simulate, and
% generate tests


param_files = {
    'params_2015-06-04T20-36-0700';
    'params_2015-06-04T20-56-0700';
    'params_2015-06-03T18-10-0700';
    };
do_sim = true;

behaviorName = 'Periodic3DFlatWalking2Pack';
behavior_config_file = fullfile(sprintf('../config/behavior/%s.yaml', behaviorName));
behavior_orig = cell_to_matrix_scan(yaml_read_file(behavior_config_file));
for param_file_index = 1:length(param_files)
    param_file = param_files{param_file_index}
    
    behavior_config = behavior_orig;
    behavior_config.parameters = param_file;
    
    fprintf('IO\n');
    behavior_config.controller(1).type = 'IO';
    behavior_config.options(1).updateParameters = false;
    behavior_config.options(1).numSteps = 4;
    behavior_config.options(1).startFromRest = false;
    behavior_config.controller.applyDiscreteRegulation = false;
    yaml_write_file(behavior_config_file, behavior_config);
    
    save_name = sprintf('%s/%s', behaviorName, behavior_config.parameters);
    save_data_dir = sprintf('~/proj/sri/data/ayonga-gaits/%s', save_name);
    save_data = sprintf('%s/%s.mat', save_data_dir, behavior_config.controller.type)
    
    if do_sim
        simulate_new;
        behavior_save;
    else
        load(save_data);
    end
    
    results = gait_metrics(domains, model, calcs_steps);
    
    results.dq_max
    gen_behavior_config;
%     
%     fprintf('PD\n');
%     behavior_config.controller(1).type = 'PD';
%     behavior_config.options(1).updateParameters = true;
%     behavior_config.options(1).numSteps = 2;
%     behavior_config.options(1).startFromRest = false;
%     yaml_write_file(behavior_config_file, behavior_config);
%     simulate_new;
%     gen_behavior_config;
end
