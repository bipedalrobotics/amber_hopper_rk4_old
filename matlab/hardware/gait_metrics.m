% addpath('../matlab_utilities/matlab');
% addpath_matlab_utilities('yaml');
function [results] = gait_metrics(domains,model,calcs_steps)
tround = @(x) roundn(x, 4, 1e-5);

out = horzcat_fields_domains([calcs_steps{:}], true);
domain = domains{2};
u   = out.uq(model.qrIndices,:);
qr  = out.qe(model.qrIndices,:);
dqr = out.dqe(model.qrIndices,:);


q_min = min(qr, [], 2);
q_max = max(qr, [], 2);
vhip = out.ya1(1, :);
vhip_min = min(vhip);
vhip_max = max(vhip);
vhip_mean = mean(vhip);
vhip_std = std(vhip);
switch domain.outputs.desired.degreeOneOutput
    case 'Constant'
        vhip_des = domain.params.v;
    case 'MinJerk'
        vhip_des = domain.params.v(2);
end
% calc = calcs(end);
% Get max torque (peak) and rms (nominal)
results = struct();
results.walking_speed = struct( ...
    'desired', tround(vhip_des), ...
    'actual', struct( ...
        'min', tround(vhip_min), ...
        'max', tround(vhip_max), ...
        'mean', tround(vhip_mean), 'std', tround(vhip_std) ...
        ) ....
    );
results.u_max = struct(...
    'nominal', tround(max(rms(u, 2))), ...
    'peak', tround(max(abs(u(:)))) ...
    );
results.dq_max = tround(max(abs(dqr(:))));
steplength = calcs_steps{2,end}.x(1,end) - calcs_steps{1,end}.x(1,1);
if isfield(calcs_steps{1},'hd')
    stepwidth  = abs(calcs_steps{1,end}.hd(2,end)) + abs(calcs_steps{1,end}.hd(8,end));
elseif isfield(calcs_steps{1},'h')
    stepwidth = calcs_steps{1}.h(1,2);
end
results.step_length = tround(steplength);
results.stepwidth   = tround(stepwidth);
results.q = struct('min', tround(q_min), 'max', tround(q_max));
fprintf(yaml_dump(results));

end
