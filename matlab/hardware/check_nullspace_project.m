% Goal: Perturb BaseRotY velocity and position, check holonomic constraint,
% apply different reset maps, and see how resulting joint velocities change
cur_raw = cell(count, nDomains);
iy = str_index(model.jointName, 'BaseRotY');
tolerance = 1e-4;
nrand = @() rand() * 2 - 1;

Ie = eye(model.nDof);

tf = 0;

doc = struct();
[~, txt] = system('git rev-parse --short HEAD');
sha = strtrim(txt);
doc.source = {struct('repo', 'durus_3d', 'sha', sha)};

doc.setup = struct( ...
	'robot_file', robot_file, ...
	'controllers_file', controllers_file ...
);

doc.tolerance = 5e-4;
doc.domains = cell(nDomains, 1);

for j = 1:nDomains
    calc = calcs_steps{j, 1};
    domain = domains{j};

    count = length(calc.t);
    
    doc_domain = struct();
    doc_domain.name = domain.domainName;
    
    test_count = min(5, count);
    test_is = round(linspace(1, count, test_count));
    tests = cell(test_count, 1);
    test_index = 1;

    for i = 1:count
    %     x = calc.x(:, i);
        q = calc.qe(:, i);
        dq = calc.dqe(:, i);
        
        q_orig = q;
        dq_orig = dq;

        % Perturb
        % Add gaussian noise?
        q(iy) = q(iy); % + 0.2 * nrand();
        dq(iy) = dq(iy) + 0.5 * nrand();

        q_perturb = q;
        dq_perturb = dq;

        x = [q; dq];

        De = De_mat(x);
        Je = domain.holConstrJac(x);

        % @ref http://stackoverflow.com/questions/18553210/how-to-implement-matlabs-mldivide-a-k-a-the-backslash-operator
        % @ref http://stackoverflow.com/questions/7949229/how-to-implement-a-left-matrix-division-on-c-using-gsl
        % Use QR factorization
        [Q,R] = qr(Je);
    %     x = R \ (Q' * b);
        JeInv = Je' * pinv(Je * Je');
        N_a = (Ie - R \ (Q' * Je));
        N_b = (Ie - Je \ Je);

        dq_kin_a = dq;
        dq_kin_b = dq;
        
        input = struct();
        input.q = q;
        input.dq = dq;

        for k = 1 %:5
            dq_minus = dq;
            dq_minus_kin_a = dq_kin_a;
            dq_minus_kin_b = dq_kin_b;

            A = [De -Je'; Je zeros(domain.nHolConstr)];
            b = [De*dq_minus; zeros(domain.nHolConstr,1)];
            y = A\b;
            dq_plus = y(model.qeIndices);

            dq_plus_kin_a = N_a * dq_minus_kin_a;
            dq_plus_kin_b = N_b * dq_minus_kin_b;

            if k > 1
                assert(maxabs(Je * dq_minus) < tolerance);
                assert(maxabs(Je * dq_minus_kin_a) < tolerance);
                assert(maxabs(Je * dq_minus_kin_b) < tolerance);
                assert(maxabs(dq_minus - dq_plus) < tolerance);
                assert(maxabs(dq_minus_kin_a - dq_plus_kin_a) < tolerance);
            end

            dq = dq_plus;
            dq_kin_a = dq_plus_kin_a;
            dq_kin_b = dq_plus_kin_b;
        end

        assert(maxabs(Je * dq) < tolerance);
        assert(maxabs(Je * dq_kin_a) < tolerance);
        assert(maxabs(Je * dq_kin_b) < tolerance);
        
        output = struct();
        
        output.Je = Je;
        output.dh = Je * dq;
        output.N = N_a;
        output.dq_plus = dq_kin_a;
        if i == test_is(test_index)
            test = struct('input', input, 'output', output);
            tests{test_index} = test;
            test_index = test_index + 1;
        end

        cur = struct();
        cur.t = calc.t(i);
        cur.q_orig = q_orig;
        cur.dq_orig = dq_orig;
        cur.q_perturb = q_perturb;
        cur.dq_perturb = dq_perturb;
        cur.dq = dq;
        cur.dq_kin_a = dq_kin_a;
        cur.dq_kin_b = dq_kin_b;
        cur_raw{i, j} = cur;
    end
    doc_domain.tests = tests;
    doc.domains{j} = doc_domain;
end

curs = horzcat_fields([cur_raw{:}]);

is = model.qeIndices;

%%
file_path = ros_resolve_local_url(test_nullspace_file);
fprintf('Writing yaml: %s\n', file_path);
yaml_write_file(file_path, doc);

%%
dq_diff = curs.dq_orig - curs.dq;

figure(1);
clf();
subplot(2, 1, 1);
hold('on');
plot(curs.t, curs.dq_perturb(is, :));
plot(curs.t, curs.dq(is, :), '--', 'LineWidth', 2);
legend(model.jointName(is));
title('reset');
subplot(2, 1, 2);
plot(curs.t, dq_diff(is, :));
legend(model.jointName(is));

%%
dq_kin_a_diff = curs.dq_orig - curs.dq_kin_a;

figure(2);
clf();
subplot(2, 1, 1);
hold('on');
plot(curs.t, curs.dq_perturb(is, :));
plot(curs.t, curs.dq_kin_a(is, :), '--', 'LineWidth', 2);
legend(model.jointName(is));
title('kin a');
subplot(2, 1, 2);
plot(curs.t, dq_kin_a_diff(is, :));
legend(model.jointName(is));

%%
dq_kin_b_diff = curs.dq_orig - curs.dq_kin_b;

figure(3);
clf();
subplot(2, 1, 1);
hold('on');
plot(curs.t, curs.dq_perturb(is, :));
plot(curs.t, curs.dq_kin_b(is, :), '--', 'LineWidth', 2);
legend(model.jointName(is));
title('kin b');
subplot(2, 1, 2);
plot(curs.t, dq_kin_b_diff(is, :));
legend(model.jointName(is));
