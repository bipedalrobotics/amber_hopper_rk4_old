% Test consistency of holonomic constraints

q0 = zeros(model.nDof, 1);
% qf = 10 * ones(model.nDof, 1);
seed = 1234;
rng(seed);
qf = 10 * rand(model.nDof, 1);

dq_zero = zeros(model.nDof, 1);

tf = 5;

xpad = @(q) [q; dq_zero];

hFunc = @(q) domain.holConstrFunc(xpad(q));
JFunc = @(q) domain.holConstrJac(xpad(q));

fprintf('\n\nhol\n');
info = jacobian_integration_test(hFunc, JFunc, q0, qf, tf);
[info.hf, info.hfy, info.hf - info.hfy]

%%
fprintf('\n\nikActive\n');
hFuncX = str2func(sprintf('ikActive_%s', domain.domainName));
JFuncX = str2func(sprintf('JikActive_%s', domain.domainName));
hFunc = @(q) hFuncX(xpad(q));
JFunc = @(q) JFuncX(xpad(q));
info = jacobian_integration_test(hFunc, JFunc, q0, qf, tf);
[info.hf, info.hfy, info.hf - info.hfy]

%%
fprintf('\n\nikInactive\n');
hFuncX = str2func(sprintf('ikInactive_%s', domain.domainName));
JFuncX = str2func(sprintf('JikInactive_%s', domain.domainName));
hFunc = @(q) hFuncX(xpad(q));
JFunc = @(q) JFuncX(xpad(q));
info = jacobian_integration_test(hFunc, JFunc, q0, qf, tf);
[info.hf, info.hfy, info.hf - info.hfy]
