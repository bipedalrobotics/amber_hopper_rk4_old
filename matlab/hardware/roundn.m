function [n2] = roundn(n, m, tol)
k = fix(log10(abs(n)))-m;
n2 = round(10.^(-k).*n).*10.^k;

if nargin >= 3
    n2(abs(n2) < tol) = 0;
end
end
