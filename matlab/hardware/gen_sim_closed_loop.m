% pair_count = size(calcs_steps, 2);
pair_count = 4;
sub_count = pair_count;

doc = struct();
[~, txt] = system('git rev-parse --short HEAD');
sha = strtrim(txt);
doc.source = {struct('repo', 'durus_3d', 'sha', sha)};

%%
out = horzcat_fields_domains([calcs_steps{:}], true);

%%
t0 = out.t(1);
ts = out.t - t0;
qes = out.qe';
dqes = out.dqe';

%%
close('all');
figure();
clf();
subplot(2, 1, 1);
plot(ts, qes);
subplot(2, 1, 2);
plot(ts, dqes);

figure();   
plot(out.t, out.tau)

%%
domains_per_step = length(domains) / 2;
right_is = 1:domains_per_step;
left_is = domains_per_step + (1:domains_per_step);
iss = {right_is, left_is};
stance_legs = {'right', 'left'};
leg_count = 2;

% Pair is set of {right, left} steps
pairs = cell(1, pair_count);
pair_grid = cell(leg_count, pair_count);

tolerance = 1e-8;
qe_prev = calcs_steps{1, 1}.qe(:, 1);
t_prev = calcs_steps{1, 1}.t(1);
prev_index = -1;

for pair_index = 1:pair_count
    for leg_index = 1:leg_count
        domain_indices = iss{leg_index};
        % Ensure all domains have same 'p' value for given leg
        blank = nan(size(domain_indices));
        p0s = blank;
        pfs = blank;
%         vs = blank;
        for di = 1:length(domain_indices)
            domain_index = domain_indices(di);
            params = domains{domain_index}.params;
            p0s(di) = params.p(2);
            pfs(di) = params.p(1);
%             vs(di) = params.v;
        end
        step = calcs_steps(domain_indices, pair_index);
        out_uneven = horzcat_fields_domains([step{:}], true);
        out_uneven.J = [];
        out_uneven.t = out_uneven.t - t0;
        out = even_sample_fields(out_uneven, 100);
        cur = struct(...
            'stance_leg', stance_legs{leg_index}, ...
            'stance_leg_index', int32(leg_index - 1), ...
            'pair_index', int32(pair_index - 1), ...
            'tau_range', [step{1}.tau([1, end]), step{2}.tau(end)], ...
            'p0', p0s(1), ...
            'pf', pfs(1), ... %             'v_hip', vs(1), ...
            'ts', out.t, ... %            't_rels', out.t_rel, ...
            'taus', out.tau, ...
            'dtaus', out.dtau, ...
            'tau_min', min(out.tau), ...
            'tau_max', max(out.tau), ...
            'dtau_min', min(out.dtau), ...
            'dtau_max', max(out.dtau), ...
            'qes', out.qe', ...
            'dqes', out.dqe' ...
            );
        pairs{pair_index}{leg_index} = cur;
        pair_grid{leg_index, pair_index} = cur;
        
        % Ensure that start point lines up with end points
        qe_diff = qe_prev - out.qe(:, 1);
        t_diff = t_prev - out.t(1);
        assert(maxabs(qe_diff) < tolerance);
        assert(maxabs(t_diff) < tolerance);
        index = sub2ind(size(pair_grid), leg_index, pair_index) - 1;
        fprintf('Good: %d -> %d\n', prev_index, index);
        qe_prev = out.qe(:, end);
        t_prev = out.t(end);
        prev_index = index;
    end
end

%%
pair_grid_tmp = pair_grid;
for pair_index = 1:pair_count
    for leg_index = 1:leg_count
        cur = pair_grid_tmp{leg_index, pair_index};
        cur.qes = cur.qes';
        cur.dqes = cur.dqes';
        pair_grid_tmp{leg_index, pair_index} = cur;
    end
end
full = horzcat_fields_domains([pair_grid_tmp{:}]);

%%
figure(2); clf();
subplot(4, 1, 1);
plot(full.ts, full.qes(7:end, :));
subplot(4, 1, 2);
plot(full.ts, full.dqes(7:end, :));
subplot(4, 1, 3);
plot(full.ts, full.taus);
subplot(4, 1, 4);
plot(full.ts, full.dtaus);

%%
figure(3); clf();
plot(full.ts, full.qes(4:6, :));

%% Phase info, from mathematica, durus_3d:0216d9c:mathematica/durus_model_3D.nb
nx = size(out.x, 1);
x0 = zeros(nx, 1);
i_r = 1;
i_l = domains_per_step + 1;
assert(strcmp(domains{i_r}.stanceLeg, 'right'));
assert(strcmp(domains{i_l}.stanceLeg, 'left'));
J_r = domains{i_r}.Jdeltaphip(x0);
J_l = domains{i_l}.Jdeltaphip(x0);

doc.phases = struct('name', {'right', 'left'}, 'J', {J_r, J_l});

%% Not periodic
% Loop-back point? 0-based!
% Choose last step
step_is = [right_is, left_is];
ndomain = length(pair_grid(:));
ndomain_step = length(step_is); % Go from left -> right or whatevs
doc.restart_index = int32(ndomain - ndomain_step + 1 - 1);

doc.domains = pair_grid(:);

%% Plot envelopes
cur = model;
env = load('./hardware/hw_envelopes.mat');

%%
figure(4);
clf();
subplot(2, 1, 1);
hold('on');
title('LeftAnkle');
poly = env.left_ankle.joint.output;
patch(poly(:, 1), poly(:, 2), [0.8, 0.8, 1], 'EdgeColor', 'none');
ijs = str_indices({'LeftAnklePitch', 'LeftAnkleRoll'}, ...
    cur.jointName);
plot(qes(:, ijs(1)), qes(:, ijs(2)), 'k');

subplot(2, 1, 2);
hold('on');
title('RightAnkle');
poly = env.right_ankle.joint.output;
ijs = str_indices({'RightAnklePitch', 'RightAnkleRoll'}, ...
    cur.jointName);
patch(poly(:, 1), poly(:, 2), [0.8, 0.8, 1], 'EdgeColor', 'none');
plot(qes(:, ijs(1)), qes(:, ijs(2)), 'k');

%%
% file_path = ros_resolve_local_url(phased_trajectory_file);
% fprintf('Writing yaml: %s\n', file_path);
file_path = strcat(behaviorConfig.parameters,'.yaml')
yaml_write_file(file_path, doc);
