out = horzcat_fields_domains([calcs_steps{:}], true);

%%
figure(1); clf();
plot(out.t, out.Phi_cond);

figure(2); clf();
plot(out.t, out.qd);

figure(3); clf();
plot(out.t, out.tau);

%%
figure(4); clf();
hold('on');
count = numel(calcs_steps);
for i = 1:count
    cur = calcs_steps{i};
    if isempty(cur)
        continue;
    end
    plot(cur.t, cur.yd2);
end
