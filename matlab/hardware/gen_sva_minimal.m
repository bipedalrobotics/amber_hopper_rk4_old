% Generate minimal SVA tree representation for basic unittests
raw = yaml_read_file(model.config);

fields_to_keep = {'type', 'axis', 'mass', 'com', 'inertia', 'E', 'r', 'lambda'};
ndof = length(raw.dofs);
dofs = cell(ndof, 1);
for i = 1:ndof
    dof_in = raw.dofs(i);
    dof = struct();
    dof.name = dof_in.name{1};
    for f = 1:length(fields_to_keep)
        field = fields_to_keep{f};
        dof.(field) = dof_in.(field);
    end
    dof.lambda = int32(dof.lambda);
    dofs{i} = dof;
end

doc = struct();
[~, txt] = system('git rev-parse --short HEAD');
sha = strtrim(txt);
doc.source = {struct('repo', 'durus_3d', 'sha', sha)};
doc.dofs = cell2mat(dofs);

file_path = fullfile(tempdir(), 'test_model.yaml');
yaml_write_file(file_path, doc);
fprintf('Wrote file: %s\n', file_path);
