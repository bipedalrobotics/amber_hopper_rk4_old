function [info] = matrix_integration_test(JComplete, q0, qf, tEnd)

    function [J] = JFunc(x)
        [J, ~] = JComplete(x);
    end
    function [Jdot] = dJFunc(x)
        [~, Jdot] = JComplete(x);
    end

nq = length(q0);

dq0 = (qf - q0) / tEnd;

x0 = [q0; dq0];
xf = [qf; dq0];

J0 = JFunc(x0);
Jf = JFunc(xf);

Jsize = size(J0);
nJ = prod(Jsize);

    function [dw] = dwFunc(t, w)
        q = w(1:nq);
        dq = dq0;
        x = [q; dq];
        dJ = dJFunc(x);
        dw = [dq; dJ(:)];
    end

w0 = [q0; J0(:)];

sol = ode45(@dwFunc, [0, tEnd], w0);

wf = sol.y(:, end);
qfy = wf(1:nq);
Jfyflat = wf(nq + (1:nJ));
Jfy = reshape(Jfyflat, Jsize);

info.sol = sol;
info.q0 = q0;
info.J0 = J0;
info.qf = qf;
info.Jf = Jf;
info.qfy = qfy;
info.Jfy = Jfy;
info.dJf = dJFunc([qfy; dq0]);
info.qfDiff = qfy - qf;
info.JfDiff = Jfy - Jf;
info.JfDiffMax = max(max(abs(info.JfDiff)));

end