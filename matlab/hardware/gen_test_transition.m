doc = struct();
[~, txt] = system('git rev-parse --short HEAD');
sha = strtrim(txt);
doc.source = {struct('repo', 'durus_3d', 'sha', sha)};

doc.setup = struct( ...
	'robot_file', robot_file, ...
	'controllers_file', controllers_file ...
);

doc.tolerance = 5e-4;

count = 3;
rand_seed = 1234;
rng(rand_seed);
doc.bezier_tests = cell(count, 1);
for i = 1:count
    coeff = rand(1, 7);
    ss = linspace(0, 1, 10);
    y = bezier(coeff, ss);
    dcoeff = diff_coeff(coeff);
    dy = dbezier(coeff, ss);
    
    input = struct();
    input.coeff = coeff;
    input.s = ss;
    output = struct();
    output.y = y;
    output.dcoeff = dcoeff;
    output.dy = dy;
    test = struct();
    test.input = input;
    test.output = output;
    
    doc.bezier_tests{i} = test;
end
nDomains = numel(domains);
doc.tests = cell(nDomains, 1);
for j = 1:nDomains
    test = transition_tests{j};
    val = int32(test.input.output_indices);
    if isscalar(val)
        val = {val};
    end
    test.input.output_indices = val;
    test.input.params = param_format_gen(test.input.params);
    doc.tests{j} = test;    
end

%%
file_path = ros_resolve_local_url(test_transition_file);
fprintf('Writing yaml: %s\n', file_path);
yaml_write_file(file_path, doc);
