% setup environment
setup;
addpath('./build_sim/Periodic3DStepping');

behaviorName = 'Static3DFlatWalking'
[model, domains, controller, simOptions, behaviorConfig] = loadBehavior(behaviorName, false);

nDomains = length(domains);
nParam = 3; % MinJerk

%% Give a good initial guess
x0 = zeros(model.nDof * 2, 1);

% Move knees away from singularity
hips = str_indices({'LeftHipPitch', 'RightHipPitch'}, model.jointName);
ankles = str_indices({'LeftAnklePitch', 'RightAnklePitch'}, model.jointName);
knees = str_indices({'LeftKneePitch', 'RightKneePitch'}, model.jointName);
x0(hips) = -0.2;
x0(knees) = 0.4;
x0(ankles) = -0.2;

domain = domains{1};
x0(1:3) = 0;
h = domain.holConstrFunc(x0);
x0(1:3) = -h(1:3);

%%
sequence_set = yaml_read_file('sequence_set.yaml');

%% Sequence it

dt = sequence_set.config.dt;
duration = sequence_set.config.duration; % s

xd = x0;

calcs_steps = {};
calcs_names = {};

t_prev = 0;

domainNames = horzcat({behaviorConfig.domains.name});

for j = 1:length(sequence_set.domains)
    domain_index = str_index(sequence_set.domains(j).domain, domainNames);
    domain = domains{domain_index};

    p = ones(nDomains + 1, 1);
    domain = setParameters(domain, 'p', p);

    nd2 = domain.nOutputs;
    names = domain.outputs.actual.degreeTwoOutput;

    sequence = sequence_set.domains(j).sequence;
    nSequence = length(sequence);
    
    % Initialize actual outputs for nan values
    y_a2 = domain.ya2(xd);
    origTable = [names, num2cell(y_a2)]
    
    y_d1 = [];
    y_d2 = y_a2;
    dy_d2 = zeros(nd2, 1);
    
    for k = 1:nSequence
        y_d2_prev = y_d2;
        updates = sequence(k).updates;
        for i = 1:length(updates)
            row = updates{i};
            name = row{1};
            value = row{2};
            index = str_index(name, names);
            if iscell(value)
                % It's a relative value
                value = y_d2(index) + value{1};
            elseif isnan(value)
                value = y_a2(index);
            end
            y_d2(index) = value;
            dy_d2(index) = 0;
        end
        if k == 1 && j == 1
            continue;
        end

        ts = 0:dt:duration;
        count = length(ts);
        a = zeros(domain.nOutputs, nParam);
        a(:, 1) = y_d2;
        a(:, 2) = y_d2_prev;
        a(:, 3) = duration;

        calcs = struct();
        calcs.t = ts;
        calcs.x = zeros(length(x0), count);
        calcs.qe = zeros(model.nDof, count);
        calcs.dqe = zeros(model.nDof, count);

        for i = 1:count
            t = ts(i);
            xd_prev = xd;
            % Compute outputs
            y_d2_cur = domain.yd2(t, a(:));
            dy_d2_cur = domain.dyd2(t, a(:));
            [qd, dqd] = calcInverseKinematics(domain, t, xd_prev, model, controller, y_d1, y_d2_cur, dy_d2_cur, [], [], []);
            xd = [qd; dqd];
            calcs.x(:, i) = xd;
            calcs.qe(:, i) = qd;
            calcs.dqe(:, i) = dqd;
        end
        calcs.t = t_prev + calcs.t;
        calcs_steps{end + 1} = calcs;
        calcs_names{end + 1} = sprintf('%s/%s', domain.domainName, sequence(k).name);
        t_prev = calcs.t(end);
    end
end

%% 
anim_plot_3D([], calcs_steps);

%%
out = horzcat_fields_domains([calcs_steps{:}]);

figure(1);
clf();
subplot(2, 1, 1);
plot(out.t, out.x(model.qrIndices, :));
subplot(2, 1, 2);
plot(out.t, out.x(model.nDof + model.qrIndices, :));

%% 
save_name = sprintf('%s/%s', behaviorName, behaviorConfig.parameters);
save_dir = sprintf('package://proxi_cpp_redo/gen/share/%s', save_name);

trajectory_file = fullfile(save_dir, 'trajectory.yaml');

save_path = ros_resolve_local_url(save_dir);
if ~isdir(save_path)
    mkdir(save_path);
end
