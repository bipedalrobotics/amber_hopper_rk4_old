doc = struct();
[~, txt] = system('git rev-parse --short HEAD');
sha = strtrim(txt);
doc.source = {struct('repo', 'durus_3d', 'sha', sha)};
    
pad_ratio = 0.15;
q_pad_min = 0.1;
dq_pad_min = 0.5;

doc_domains = cell(nDomains, 1);

for j = 1:nDomains
    % Create saturation bound for desired positions and velocities in h_d
    % and dh_d...
    % But what about holonomic constraints?
    domain = domains{j};
    calcs_sub = calcs_steps(j, end);
    calcs = horzcat_fields_domains([calcs_sub{:}], true);
    
    % @todo Forgot about domains..
    
    doc_bounds = struct();
    doc_bounds.domain_name = domain.domainName;
    
    [lower, upper] = create_bound(calcs.qe, 2, pad_ratio, q_pad_min);
    doc_bounds.qd = struct('min', lower, 'max', upper);
    
    [lower, upper] = create_bound(calcs.dqe, 2, pad_ratio, dq_pad_min);
    doc_bounds.dqd = struct('min', lower, 'max', upper);
    
    doc_domains{j} = doc_bounds;
end

doc.domains = doc_domains;

%%
calcs_sub = calcs_steps(:, end);
calcs = horzcat_fields_domains([calcs_sub{:}], true);

doc_bounds = struct();
[lower, upper] = create_bound(calcs.qe, 2, pad_ratio, q_pad_min);
doc_bounds.qd = struct('min', lower, 'max', upper);

[lower, upper] = create_bound(calcs.dqe, 2, pad_ratio, dq_pad_min);
doc_bounds.dqd = struct('min', lower, 'max', upper);

doc.full = doc_bounds;

%%
file_path = ros_resolve_local_url(output_bounds_file);
fprintf('Writing yaml: %s\n', file_path);
yaml_write_file(file_path, doc);
