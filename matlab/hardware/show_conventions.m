setup;

cur = model;

%%
for i = [3,cur.qIndices(:)']
    x = zeros(cur.nDof * 2, 1);
    x(1) = 0;
    x(2) = 0;
    name = cur.jointName{i};
    angle = 30;
    if i==4 || i==11
        x(i) = 0.03;
    else
        x(i) =  angle*pi/180;
    end
    
    fprintf('joint: %s\nangle: %g deg\n\n', name, angle);
    %     figure(1); clf();
    plot3_robot_pos(x);
    %     view([49, 18]);
    keyboard;
    
end
