
%%
foot_R_dof_names = {'RightAnklePitch', 'RightAnkleRoll', 'RightFootSpring'};
count = length(foot_R_dof_names);
foot_R_dofs = zeros(count, 1);
for i = 1:count
    foot_R_dofs(i) = str_index(foot_R_dof_names{i}, model.jointName);
end

foot_R_mass = sum(model.jointMass(foot_R_dofs))

robot_mass = sum(model.jointMass)
