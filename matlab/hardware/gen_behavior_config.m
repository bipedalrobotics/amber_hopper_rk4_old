save_dir = sprintf('package://proxi_cpp_redo/gen/share/%s', save_name);

robot_file = 'package://proxi_cpp_redo/gen/share/durus_3d.yaml';
controllers_file = fullfile(save_dir, 'controllers.yaml');

phased_trajectory_file = fullfile(save_dir, 'phased_trajectory.yaml');
trajectory_file = fullfile(save_dir, 'trajectory.yaml');

output_bounds_file = fullfile(save_dir, 'output_bounds.yaml');
test_ik_file = fullfile(save_dir, 'test_ik.yaml');
test_io_file = fullfile(save_dir, 'test_io.yaml');
test_transition_file = fullfile(save_dir, 'test_transition.yaml');
test_nullspace_file = fullfile(save_dir, 'test_nullspace.yaml');

%%
% save_path = ros_resolve_local_url(save_dir);
save_path = '/home/ayonga/Dropbox/research/DURUS/durus_3d/matlab';
if ~isdir(save_path)
    mkdir(save_path);
end

if strcmp(controller.type, 'IO')
    gen_controller;
    gen_sim_closed_loop;
    gen_sim_open_loop;
    gen_test_io;
    gen_output_bounds;
    check_nullspace_project;
    fprintf('Ensure PD results are generated\n');
elseif strcmp(controller.type, 'QP-CLF')
    gen_controller;
    gen_sim_closed_loop;
    gen_sim_open_loop;
    gen_output_bounds;
elseif strcmp(controller.type, 'PD')
    gen_test_ik;
    gen_test_transition;
    fprintf('Ensure IO results are generated\n');
else
    error('Unknown control type: %s', controller.type);
end

save_setup = struct(...
    'name', save_name, ...
    'robot_file', robot_file, ...
    'controllers_file', controllers_file, ...
    'phased_trajectory_file', phased_trajectory_file, ...
    'output_bounds_file', output_bounds_file, ...
    'test_io_file', test_io_file, ...
    'test_ik_file', test_ik_file, ...
    'test_transition_file', test_transition_file ....
    );
fprintf('%s\n', yaml_dump(save_setup));
