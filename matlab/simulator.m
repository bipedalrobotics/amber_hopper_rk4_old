function [model, domains, calcs_steps, controller, behaviorConfig] = ...
                                simulator(behaviorName)
%%% simulation script
setup; % setup environment
if nargin == 0
    behaviorName = 'Periodic2DRuning';
end

%% Initialization
[model, domains, controller, simOptions, behaviorConfig] = ...
                                            loadBehavior(behaviorName);

ref         = Ref();
ref.h       = struct();
ref.h.calcs = {};
ref.h.calc  = {};

nSteps   = simOptions.numSteps;
nDomains = numel(domains);

% Initialize the logger
calcs_steps      = cell(nDomains, nSteps);
% transition_tests = cell(nDomains, nSteps);
record_domains   = cell(nDomains, nSteps);

x0      = domains{1}.params.x_plus;
footpos = domains{1}.holConstrFunc(x0);
x0(1)   = x0(1) - footpos(1);

%%% starting from the rest, set joint velocities to be zero
if simOptions.startFromRest
    x0(model.dqeIndices) = zeros(model.nDof,1);
end

% x_minus = domains{end}.params.x_minus;

% controller.updatePhaseRange = false;

% if controller.applyDiscreteRegulation
%     for i = 1:nDomains
%         domains{i}.dlp = struct();
%         %% HUBICKI ADDITION
%         domains{i}.dlp.desired_velocity = desired_velocity;
%         domains{i}.dlp.pushoff_gain = pushoff_gain;
%         
%         domains{i}.dlp.deltaQ_current = 0;
%         
%         domains{i}.dlp.deltaQ_prev = 0;
%         
%         domains{i}.dlp.tau_takeoff = 0;
%         
%         %% END HUBICKI ADDITION
%     end
% end


%% Initial Parameters
% Record initial p values
p_init = cell(nDomains, 1);
for j = 1:nDomains
    p_init{j} = domains{j}.params;
end

% x_minus_r = domains{nDomains/2}.params.x_minus;
% x_minus_l = domains{nDomains}.params.x_minus;


%% Simulate the system using ODE solver
t_plus = 0;
for k = 1:nSteps
    
    tic    
    t_i = t_plus;
    x_i = x0;
    for j = 1 : nDomains
        fprintf('{step: %d, domain: %d, t_plus: %g}\n', k, j, t_plus);
        current_domain = domains{j};
        
        %%% motion transition???????? -w.ma
        % if simOptions.updateParameters
        %     if simOptions.useSameParameters
        %         x_update_target = blkdiag(model.footSwappingMatrix,model.footSwappingMatrix)...
        %                           * x_minus;
        %     else
        %         x_update_target = current_domain.params.x_minus;
        %     end
        %     %heck!!! change t0, tf for time based trajectory
        %     if strcmp(current_domain.outputs.type,'Time-Based')
        %         [current_domain,transition_tests{j,k}] = ...
        %         calcMotionTransition(current_domain, model, controller,...
        %                              current_domain.params.p(2), x0,...
        %                              current_domain.params.p(1), x_update_target);
        %     elseif strcmp(current_domain.outputs.type,'State-Based')
        %         [current_domain, transition_tests{j, k}] =...
        %         calcMotionTransition(current_domain, model, controller,...
        %                              0, x0, 1, x_update_target);
        %     else
        %         warning('undefined output type, motion transition could not be performed.\n');
        %     end
        % end
        
        % domain = setParameters(domain,'x_plus',x0);
        %%% For PD / IK
        % if strcmpi(controller.type,'PD')
        %     p0    = current_domain.deltaphip(x0);
        %     pdot0 = current_domain.Jdeltaphip(x0) * x0(model.dqeIndices);
        %     current_domain = setParameters(current_domain,'p0',p0,'pdot0',pdot0);
        % end
        
        ref.h.calcs  = {};
        ref.h.calc   = {};
        ref.h.stepNo = k;
        
        odeopts = odeset('MaxStep', 1e-2, 'OutputFcn', @outputfcn,...
                         'Events', @(t,x,ref)checkGuard(current_domain,...
                                              t,x,model,controller,ref),...
                         'RelTol',1e-6, 'AbsTol',1e-6);
        
        sol = ode45(@(t, x, ref) calcDynamics(current_domain, t, ...
                                              x, model, controller, ref),...
                    [0, 10], x0, odeopts, ref);
        
        %%% Compute reset map at the guard
        t_f = sol.x(end);
        x_f = sol.y(:,end);
        x0  = calcResetMap(current_domain, model, x_f);
        
        %%% log the simulation data
        calcDynamics(current_domain, t_f, x_f, model, controller, ref);
        ref.h.calcs{end} = ref.h.calc;
        
        %%% Store calculations
        calcs       = horzcat_fields(cell2mat(ref.h.calcs));
        calcs.t_rel = calcs.t;
        calcs.t     = t_plus + calcs.t;
        calcs_steps{j,k} = calcs;
        
        %%% Reset for next integration
        t_plus = t_plus + t_f;
        
        if simOptions.useSameParameters
            if current_domain.indexInStep == current_domain.numDomainsInStep
                x_minus = x_f;
            end
        else
            x_minus = x_f;
        end
        
        %%% keyboard;
        current_domain = setParameters(current_domain,'x_minus',x_f);
        if strcmp(current_domain.outputs.type,'TimeBased')
            current_domain.params.p(1) = t_f;
        end
        
        %%%  keyboard;
        % domain = setParameters(domain,'x_minus',x_f);
        domains{j} = current_domain;
        record_domains{j,k} = current_domain;
        
        % if controller.applyDiscreteRegulation
        %     %% HUBICKI ADDITION
        %     if(current_domain.indexInStep == 1) % the first domain (double support)
        %         for i = 1:nDomains
        %             domains{i}.dlp.tau_takeoff = calcs.tau(end);
        %         end
        %     end
        % 
        %     %%% the last domain (single support)
        %     if(current_domain.indexInStep == current_domain.numDomainsInStep) 
        %         phipdot = current_domain.Jdeltaphip(x_f)*...
        %             x_f(model.dqeIndices);
        %         % Compute velocity error
        %         velocity_error = desired_velocity - phipdot
        %         velocity_log(2*k-1) = phipdot;
        %         for i = 1:nDomains
        %             domains{i}.dlp.deltaQ_prev = domains{i}.dlp.deltaQ_current;
        % 
        %             domains{i}.dlp.deltaQ_current = velocity_error*pushoff_gain;
        %         end
        %         if(k < nSteps)
        %             for i=1:nDomains
        %                 domains{i}.dlp.vel_meas = phipdot;
        %             end
        %         end
        %     end
        % 
        % 
        %     % /END HUBICKI ADDITION
        % end
    end
    toc
    
end

% debug

end