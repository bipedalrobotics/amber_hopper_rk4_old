function [x_post,ImpF,calc] = calcResetMap(obj, model, x_pre)
    % - Calculate the reset map of the domain
    % Input: 
    %    * domain - domain
    %    * x_pre  - pre guard states
    %
    % Output:
    %    * x_post - post guard states
    %
    % Copyright 2014-2015 Georgia Tech, AMBER Lab
    % Author: Ayonga Hereid <ayonga@tamu.edu>
    %         Wen-Loong Ma<modified for hopping, wenloong.ma@gmail.com>
    
    if nargout >= 3
        calc = struct();
    end

    % x_pre -> [qe_pre; dqe_pre]
    qe_pre    = x_pre(model.qeIndices);
    dqe_pre   = x_pre(model.dqeIndices);
    
    %% compute the impact map if the domain transition involves a rigid impact
    if obj.hasImpact
        % jacobian of impact constraints
        Je  = obj.impConstrJac(x_pre);
        
        % inertia matrix
        De = De_mat(x_pre);
        
        % Compute Dynamically Consistent Contact Null-Space from Lagrange
        % Multiplier Formulation
        %         DbarInv = Je * (De \ transpose(Je));
        %         I = eye(obj.nDof);
        %         Jbar = De \ transpose(transpose(DbarInv) \ Je );
        %         Nbar = I - Jbar * Je;
        
        % Apply null-space projection
        %         dqe_plus = Nbar * dqe_pre;        
        
        A = [De -Je'; Je zeros(obj.nImpConstr)];
        b = [De*dqe_pre; zeros(obj.nImpConstr,1)];
        y = A\b;
        
        ImpF    = y((model.nDof+1):end);
        dqe_pre = y(model.qeIndices);
        
        if nargout >= 3
            calc.Je = Je;
            calc.De = De;
        end
        
    else
        ImpF = [];
    end
    
    %% if swapping the stance/non-stance foot, multiply with
    %  'footSwappingMatrix'
    if obj.isSwapping
        %first, calculate nsf_minus
        x_tmp = [qe_pre; zeros(length(qe_pre),1)];
        pos   = jpos_mat(x_tmp);
        foot  = pos([1,3], 6);
        if abs(foot(2)) > 1e-6
            warning('rest foot_z is not zero');
        end
        
        % qe_pre(1) = ...
        % -sin(qe_pre(3) + qe_pre(4)/2 + qe_pre(5)/2)*((28*cos(qe_pre(4)/2 - qe_pre(5)/2))/125 ...
        % + (143*cos(qe_pre(4)/2 - qe_pre(5)/2 + qe_pre(6)))/250);

        qe_pre(1) = qe_pre(1) - foot(1);
        
        %%% for fly phase
        %qe_pre(1)    = 0;%nsf(1);
        %qe_pre(2)    = 0; %reset next step sf_z to be 0
        %dqe_pre(1:2) = zeros(2,1); % reset next step sf speed to be [0,0]
    end

    % construct the post impact states
    x_post = [ qe_pre;
              dqe_pre];
end
