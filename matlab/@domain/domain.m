classdef domain
    %DOMAIN Specifies the structure of a domain in the hybrid system model
    % Description
    %   This class specifies the structure of a domain in the hybrid system
    %   model, including:
    %          * Holonomic constraints
    %          * Continuous dynamics
    %          * Guard condition
    %          * Reset map (map from the current domain to the next domain)
    %          * Controller
    %          * Zero dynamics ???
    %
    % Copyright 2014-2015 Texas A&M University AMBER Lab
    % Author: Ayonga Hereid <ayonga@tamu.edu>
    
    properties
        domainName@char % domain name
        
        domainIndex@double % domain index
        
        indexInStep@double % index of the domain in the step
        numDomainsInStep@double % number of domains in one step
        
        % legID (legID) - Indicates which leg is the stance leg
        legID@double 
        stanceLeg@char
        %% Holonomic constraints related function handls
                      
        nHolConstr@double % number of holonomic constraints
        
        holConstrName@cell  % names of holonomic constraints
        
        % constrFunc - function that computes the value of holonomic constraints
        holConstrFunc@function_handle 
        
        % constrJac - function that computes the jacobian of holonomic constraints
        holConstrJac@function_handle  
        
        % constrJacDor - function that computes time derivatives 
        % of the jacobian matrix of holonomic constraints
        holConstrJacDot@function_handle 
        
        %% Guard
        guardName@char % name of the guard condition
        
        guardDir@double % direction of the guard 
        % guardDir:
        %   * +1: if guard function cross zero from negative to positive
        %   * -1 (default) : if guard function cross zero from positive to negative
        
        guardThld@double % threshold of the guard condition, normally zero
        
        guardType@char  % type of guard condition
        
        guardFunc@function_handle % function that computes the guard value
        
        guardJac@function_handle  % function that computes the jacobian of guard
        %%  Reset maps
        
        hasImpact@logical  % indicates there is a rigid impact
        
        isSwapping@logical % indicates whether swap the stant/non-stance legs
        
        % function that computes the jacobian of impact constraints
        impConstrJac@function_handle
        
        nImpConstr@double % number of impact constraints
        
        impConstrName@cell % name of impact constraints
        
        % actual hip position and velocity
        phip@function_handle
        vhip@function_handle
        
        
        %% virtual constraints (i.e., outputs)
        outputs@struct
        nOutputs@double
        
        nParamRD1@double
        nParamRD2@double
        nParamPhaseVar@double
        
        
        ya1@function_handle  % relative degree one output - actual
        ya2@function_handle  % relative degree two outputs - actual
        yd1@function_handle  % relative degree one output - desired
        yd2@function_handle  % relative degree two outputs - desired
        
        % First order jacobian of outputs
        Dya1@function_handle  % 1st Jacobian of relative degree one output - actual
        Dya2@function_handle  % 1st Jacobian of relative degree two outputs - actual
        
        % Second order jacobian of outputs
        DLfya2@function_handle % 2nd Jacobian of relative degree two outputs - actual
        
        dyd1@function_handle % 1st derivative of desired degree one output w.r.t tau
        dyd2@function_handle % 1st derivative of desired degree two outputs w.r.t tau
        ddyd2@function_handle % 2nd derivative of desired degree two outputs w.r.t tau
       
        deltaphip   % Linearized hip position function handle
        Jdeltaphip % Jacobian of linearized hip position
        
        tau@function_handle   % phase variable tau
        dtau@function_handle  % time derivative of tau
        Jtau@function_handle  % jacobian of tau w.r.t to system states x
        Jdtau@function_handle % jacobian of dtau w.r.t to system states x
        
        
        clfs@struct % structure of clfs
        %% actuated joints information (if it is not the same as rotor joints)
        nAct@double
        qaIndices@double 
        dqaIndices@double
        
        
        %% zero dynamics coordinates
        nZero@double
        qzIndices@double
        dqzIndices@double
        
        
        %% domain parameters
        params@struct
        
        %% next domain
        nextDomain@struct
       
        % discrete regulation
        dlp@struct
        
        % vline spec
        vlineSpec
    end
    
    methods
        
        %% constructor
        
        function obj = domain(name, index, model)
            % domain - The constructor of the class
            
            
            
            % Domain name
            obj.domainName  = name;
            obj.domainIndex = index; 
            % specifiy legID
            if ~isempty(regexpi(obj.domainName,'left'))
                obj.legID = -1;
                obj.stanceLeg = 'left';
            else
                obj.legID = 1;
                obj.stanceLeg = 'right';
            end
            
            
            %% load domain configuration
            parent_path = fileparts(pwd);
    
            config_file = fullfile(parent_path,'config','domain',...
                strcat(obj.domainName,'.yaml'));
            
            % check if the file exists
            assert(exist(config_file,'file')==2,...
                'The configuration file could not be found.\n');
            
            % if exists, read the content and return as a structure
            domainConfig = cell_to_matrix_scan(yaml_read_file(config_file));
           
            %% domain index in a step
            obj.indexInStep      = domainConfig.indexInStep;
            obj.numDomainsInStep = domainConfig.numDomainsInStep;
            
            %% configure holonomic constraints
            obj = setHolonomicConstraints(obj, domainConfig.constraints);
            
            %% configure guard
            obj = setGuard(obj, domainConfig.guard);
            
            %% configure reset map
            obj = setResetMap(obj, domainConfig.resetMap);
            
            %% configure outputs
            obj = setOuputStructure(obj, domainConfig.outputs, model);
            
            
            %% assign function for hip position and velocity
            obj.phip = str2func(strcat('phip_',obj.domainName));
            obj.vhip = str2func(strcat('vhip_',obj.domainName));
        end
        
        
        
        
        
        
    end
    
end

