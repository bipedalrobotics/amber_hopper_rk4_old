function [v, scot, frequency, stepL] = AnalysisGaits()
%% this load all ../results/sim/*/gaits.mat

retPath = '../results/sim/';
V_name = dir(retPath);
ret = cell(0);


%%
tic
for vIndex = 3 : length(V_name)
    V_folder = [V_name(vIndex).name, '/'];
    
    gaitsName = dir( [retPath,V_folder, '*.mat'] );
    
    for gaitIndex = 1:length(gaitsName)
        gaitName = gaitsName(gaitIndex).name;
        [~, gaitName, ~] = fileparts(gaitName);
        gait = loadGait(retPath, gaitName, V_folder);
        
        ret{end+1} = gait;
    end
    
end
toc

%%
for i = 1:length(ret)
    v(i) = ret{i}.speed;
    
    scot(i)      = mean( ret{i}.scot_abs );
    
    frequency(i) = ret{i}.Frequency;
    stepL(i)     = ret{i}.sim_step_length;
    
    %opt_umax(i)  = ret{i}.opt_u_max;
    %opt_dqmax(i) = ret{i}.opt_dq_max;
    sim_umax(i)  = ret{i}.sim_u_max;
    sim_dqmax(i) = ret{i}.sim_dq_max;
    
end

end



%% to log the simulation data and opt data
function gait = loadGait(retPath, gaitName, V_folder)
matPath = [retPath, V_folder, gaitName, '.mat'];
load(matPath);


%%
gait = struct;
%%% scot
gait.scot_abs = scot_abs(:,end);
gait.scot_pos = scot_pos(:,end);
gait.scot_sum = scot_sum(:,end);


%%
gait.name  = behaviorConfig.parameters;
gait.speed = str2double(V_folder(2:4));
uIndex     = model.qrIndices;


%%
clear record_domains;
clear behaviorConfig controller domains;
clear scot_abs scot_pos scot_sum model;


%% load optmization resutls
yamalPath = ['../config/parameters/Periodic3DFlatRunning/', ...
             V_folder, gaitName, '.yaml'];
w = cell_to_matrix_scan(yaml_read_file(yamalPath));
gait.opt_u_max = w.gait_metrics.u_max;
gait.opt_dq_max = w.gait_metrics.dq_max;
gait.opt_step_length = w.gait_metrics.step_length;


%% load simulation results
lastStep = calcs_steps(:, end);
domain1  = lastStep{end-1};
domain2  = lastStep{end};
u   = [domain1.u, domain2.u];
dqe = [domain1.dqe, domain2.dqe];

gait.sim_u_max  = max(max(abs(u))) ;
gait.sim_dq_max = max(max(abs(dqe)));
gait.sim_step_length = domain2.com(1,end) - domain1.com(1,1);
sim_T = domain2.t(end) - domain1.t(1);
gait.Frequency = 1/sim_T;


%% compute power
power = zeros( 15, size(u,2) );
for i = 1: size(u,2)
    power(:,i) = u(:,i).* dqe(uIndex, i);
end
gait.power = power;


end