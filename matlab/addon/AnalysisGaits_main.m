function [] = Analysis2(v, scot, frequency, stepL)

plot1(v, scot, 'SCOT')
plot1(v, stepL, 'Step Length (m)')
plot1(v, frequency, 'Running Frequency (Hz)')

end

%%
function [] = plot1(x, y, ylabels)
%%
figure; clf;
h = plot(x, y, 'o', 'LineWidth',2); grid on;

xlabel('$V_{com} (m/s)$', 'Fontsize',20, 'Interpreter','LaTex');
ylabel(ylabels,     'Fontsize',20, 'Interpreter','LaTex');
set(gca,'FontSize', 17, ...
        'position', [0.15   0.28   0.775   0.69]);
%ylim([0.8, 2.0]);
ylim([min(y)*0.95, max(y)*1.06])
% print(h,'../../IROS/figures/results/multigaits/scot', picForm);

end