%% save_opt

info_path = ['../config/parameters/Periodic2DRuning/', param_name, '_info/']; 
if ~exist(info_path,'dir')
    mkdir(info_path);
end

% save([info_path,'/opt_info'], 'info');
copyfile('../config/opt/opt_2DRuning.yaml', info_path);
% copyfile('../config/model/durus_parameters_3d_noboom.yaml', info_path);

if exist('log.yaml', 'file')
    movefile('log.yaml', info_path);
end