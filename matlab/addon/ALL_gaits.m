% save all the running time and iterations from yaml files.
% to analysis the optimization performance.
data = [...
1.5 484  188;
1.5 855  273;
1.6 584  251;
1.6 168  92;
1.7 449  226;
1.8 1137 348;
1.9 664  352;
1.9 548  176;
2.0 722   374;
2.0 722   376;
2.0 1200  464;
2.0 1200  474;
2.1 396    203;
2.2 705   347;
2.3 700   366;
2.4 1000  780
2.5 1000  539;
2.5 660   390;
2.6 1000 685;
2.6 206  113;
2.7 447   289;  
2.8 477   316;
2.9 221   174;
2.9 333   227;
3.0 319   211
];

%%
clear data
data = [...
1.5 669.5 230.5;
1.6 171.5 251;
1.7 449   226;
1.8 1137 348;
1.9 606   264;
2.0 961   422;
2.1 396  203;
2.2  705   347;
2.3 700   366;
2.4 1000  780;
2.5 830  464.5;
2.6 603   399;
2.7 447   289;  
2.8 477   316;
2.9 277   200.5;
3.0 319   211];

v    = data(:,1);
iter = data(:, 2);
time = data(:, 3);

%%
% figure(2); clf
% labels = {'iteration', 'Computation Time'};
% 
% hk(1) = bar(v,  iter, 0.7,'FaceColor',[0.2 0.2 0.5]); hold on;
% hk(2) = bar(v,  time, 0.4,'FaceColor',[0 0.7 0.7]); grid on;
% 
% xlim([1.4,3.1]); ylim([0.]);
% xlabel('$V_{com}(m/s)$',      'Fontsize',20, 'Interpreter','LaTex');
% % ylabel(hk(1), 'Computation Time(s)','Fontsize',20, 'Interpreter','LaTex');
% % ylabel(hk(2), 'Iterations','Fontsize',20, 'Interpreter','LaTex');
% %%
% ax1 = gca;
% ax1.YColor = 'r';
% 
% ax1_pos = ax1.Position; % position of first axes
% ax2 = axes('Position',ax1_pos,...
%            'YAxisLocation','right',...
%             'Color','none');



%%
figure(3); clf

[ax,h1,h2] = plotyy(v,iter, v, time,'bar'); grid on
%%% legend('iteration', 'time');
labels = {'iteration', 'Computation Time'};
xlabel('$V_{com}(m/s)$',      'Fontsize',20, 'Interpreter','LaTex');
ylabel(ax(1), 'Computation Time(s)','Fontsize',20, 'Interpreter','LaTex');
ylabel(ax(2), 'Iterations','Fontsize',20, 'Interpreter','LaTex');
% ax(1).xlim = [1.4,3.1];
% ax(2).xlim = [1.4,3.1];

h2.BarWidth = 0.4;
h2.FaceColor = 'c';
h1.BarWidth = 0.75;
h1.FaceColor = 'b';

set(gca,'FontSize', 14, ...
        'position', [0.15   0.28   0.72  0.69]);

a = [0.2; 0.55];
%ax    = axes('position',get(gca,'position'), 'visible','off');
for i = 1:2
 legend( ax(i), labels(i), 1,...
               'Location', 'none', ...
               'Orientation','Horizontal', ...
               'Box','off',...
               'Color','none',...
               'FontSize',15,'Interpreter','LaTex',...
               'Position',[a(i), 0.02, 0.2 0.1]);
end


%%
figure(1); clf
[ax,b,p] = plotyy(v,iter,v,time,'bar','plot'); grid on;
% legend('iteration', 'time');
labels = {'Iteration', 'Computation Time'};
xlabel('$V_{com}(m/s)$',      'Fontsize',20, 'Interpreter','LaTex');
ylabel(ax(1), 'Iteration','Fontsize',20, 'Interpreter','LaTex');
ylabel(ax(2), 'Computation Time(s)','Fontsize',20, 'Interpreter','LaTex');
ax(2).YColor = 'k';%, 'Color','k');
ax(1).XLim = [1.4,3.1];
ax(2).XLim = [1.4,3.1];
ax(1).FontSize = 15;
ax(2).FontSize = 15;

b.FaceColor = [0, 0.7, 0.7];%'b'
p.LineWidth = 3.5;
p.Color = [0.2 0.2 0.5];


% ax(2).
set(gca, 'position', [0.15   0.28   0.73   0.69]);

a = [0.2; 0.6];
%ax    = axes('position',get(gca,'position'), 'visible','off');
for i = 1:2
 legend( ax(i), labels(i), 1,...
               'Location', 'none', ...
               'Orientation','Horizontal', ...
               'Box','off',...
               'Color','none',...
               'FontSize',15,'Interpreter','LaTex',...
               'Position',[a(i), 0.05, 0.2 0.1]);
end