VPath = ['V', num2str(calcs_steps{1}.yd1(1), '%.1f'), '/'];

%%
%logFile  = [behaviorConfig.parameters, '_simLog.txt'];
logFile = [behaviorConfig.parameters, '.mat'];
logBase = '../results/sim/';

save(logFile, 'model', 'domains', 'calcs_steps', 'behaviorConfig',...
              'record_domains', 'controller',...
              'scot_pos', 'scot_abs', 'scot_sum');

if ~exist([logBase, VPath])
    mkdir([logBase, VPath]);
end
movefile(logFile, [logBase, VPath]);


%%
% movefile('simLog.txt', logFile);
gaitFile = [behaviorConfig.parameters, '.yaml'];
gaitBase = '../config/parameters/Periodic3DFlatRunning/';

movefile([gaitBase, gaitFile],...
         [gaitBase, VPath]);