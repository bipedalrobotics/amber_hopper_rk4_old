function [obj] = configureConstraints(obj)
% configureConstraints - register constraints
%
% Copyright: Georgia Tech, AMBER Lab
% Author:    Ayonga Hereid <ayonga@tamu.edu>
%            Wen-Loong Ma  <wenloong.ma@gmail.com> [modifed for hopping.]

    %% register constraints
    constraints = cell(obj.nDomain,1);
    for i = 1 : obj.nDomain
        domain = obj.domains{i};
        
        %% common inner-node constraints
        % initial linearized hip position: p(i-1) - deltaphip(q_1) = 0;
        % p(1): final deltaphip of one step/fly domain
        % p(2): initial deltaphip of s.s. domain
        % p(3): initial deltaphip of fly domain
        if ~obj.options.OpenLoopController && ~obj.options.TimeBased
            domain = addConstraint(domain,'Linear-Equality',...
                                   'deltaPhip0', 1, 1, {{'q','p'}},...
                                   0, 0);
            if i == obj.nDomain
                % final linearized hip position: p(1) = deltaphip(q_N);
                % p(1): final deltaphip of fly domain
                domain = addConstraint(domain,'Linear-Equality',...
                                       'deltaPhipf',1,domain.nNode,...
                                       {{'q','p'}}, -1e-5, 1e-5);
            end
        end
        
        % dynamics equation: D*ddq + H(q,dq) - Be*u - J^T(q)*Fe = 0;
        domain = addConstraint(domain,'Nonlinear-Equality',...
                              'dynamics',domain.nDof,1:domain.nNode,...
                              {{'q','dq','ddq','u','Fe'}},0,0);
        
        %% step Length defined
        if i == obj.nDomain
            domain = addConstraint(domain,'Linear-Equality',...
                                   'stepLengthConstr',1, domain.nNode,...
                                   {{'q','p','h'}}, 0, 0);
        end
        
        %% holonomic constraint                  
        % holonomic constraint (position level): h(q) - hd = 0;
        domain = addConstraint(domain,'Nonlinear-Equality',...
                                'holonomicPos',domain.nHolConstr,1,...
                                {{'q'}}, -1e-7, 1e-7);
        
        % holonomic constraint (velocity level): J(q)dq = 0;
        domain = addConstraint(domain,'Nonlinear-Equality',...
                                'holonomicVel',domain.nHolConstr,1,...
                                {{'q','dq'}}, -1e-7, 1e-7);
        
        % holonomic constraint (acceleration level):
        % J(q)ddq + Jdot(q,dq)dq = 0;
        domain = addConstraint(domain,'Nonlinear-Equality',...
                                'holonomicAcc', domain.nHolConstr,...
                                1:domain.nNode,...
                                {{'q','dq','ddq'}}, -1e-7, 1e-7);
                    
        %% guard condition: g(q) = 0;
        switch domain.guardType
            case 'kinematics'
                %%% inequality constraints to make sure it never cross the
                %%% guard during the domain.
                % domain = addConstraint(domain,'Nonlinear-Inequality',...
                %                        'guard',1,1:(domain.nNode-1),{{'q'}},0,inf);
                % relaxed equality constraint to make sure it hits the
                % guard at the end of the domain
                % if domain.guardDir == -1
                %     lb_guard = -5e-4;
                %     ub_guard = -5e-5;
                % elseif domain.guardDir == 1
                %     lb_guard = 5e-5;
                %     ub_guard = 5e-4;
                % end
                domain = addConstraint(domain,'Nonlinear-Equality',...
                                       'guard', 1, domain.nNode, {{'q'}}, ...
                                       0, 0);
            case 'forces'
                % inequality constraints to make sure it never cross the
                % guard during the domain.
                domain = addConstraint(domain,'Nonlinear-Inequality',...
                                       'guard',1,1:(domain.nNode-1),...
                                       {{'Fe'}}, 0, inf);
                % relaxed equality constraint to make sure it hits the
                % guard at the end of the domain
                domain = addConstraint(domain,'Nonlinear-Equality',...
                                       'guard',1,domain.nNode,...
                                       {{'Fe'}}, -1e-5, 0);
            case 'phase'
                % inequality constraints to make sure it never cross the
                % guard during the domain.
                domain = addConstraint(domain,'Nonlinear-Inequality',...
                    'guard',1,1:(domain.nNode-1),{{'qe','p'}}, 0, inf);
                % relaxed equality constraint to make sure it hits the
                % guard at the end of the domain
                domain = addConstraint(domain,'Nonlinear-Equality',...
                    'guard',1,domain.nNode,{{'q','p'}}, -5e-4, 5e-4);
            otherwise
                error('undefined guard type');
        end
        
        
        %% virtual constraints
        if ~obj.options.OpenLoopController
            if ~obj.options.TimeBased
                if ~isempty(domain.outputs.actual.degreeOneOutput)
                    %%% relative degree 1 output (velocity level) %%%%%%%%%
                    if obj.options.ZeroRD1Error
                        domain = addConstraint(domain,'Linear-Equality',...
                                               'y1Vel', 1, 1,...
                                               {{'q','dq','p','v'}}, 0,0);
                    end
                    % relative degree 1 output (acceleration level)
                    domain = addConstraint(domain, 'Linear-Equality',...
                                           'y1Acc', 1,1:domain.nNode,...
                                           {{'q','dq','ddq','p','v'}},...
                                           -5e-4, 5e-4);
                end
                
                errorbound = 5e-6;
                
                %%% relative degree 2 output : %%%%%%%%%%%%%%%%%%%%%%%%%%%%
                % ya2(q) - yd2(q,p) = 0; (position level)
                domain = addConstraint(domain,'Nonlinear-Equality',...
                                        'y2Pos',domain.nOutputs, 1,...
                                        {{'q','p','a'}},...
                                        -errorbound, errorbound);
 
                % relative degree 2 output (velocity level):
                % \dot{ya2(q)- yd2(q,p)} = 0;
                domain = addConstraint(domain,'Nonlinear-Equality',...
                                       'y2Vel',domain.nOutputs, 1,...
                                       {{'q','dq','p','a'}},...
                                       -errorbound, errorbound);
                
                %  relative degree 2 output (acceleration level):
                % \ddot{ya2(q)- yd2(q,p)} = 0;
                domain = addConstraint(domain,'Nonlinear-Equality',...
                                       'y2Acc',domain.nOutputs, 1:domain.nNode,...
                                      {{'q','dq','ddq','p','a'}},...
                                      -errorbound, errorbound);

            else
                %% ####FIX TIME BASED OUTPUT#####
                extra = [1,(domain.nNode+1)/2];
                if ~isempty(domain.outputs.actual.degreeOneOutput) && obj.options.ZeroRD1Error
                    % relative degree 1 output
                    domain = addConstraint(domain,'Nonlinear-Equality',...
                                          'y1Vel',1,1,...
                                          {{'t','q','dq','v'}}, 0,0,extra);
                end
                
                % relative degree 2 output (position level):
                % ya2(q) - yd2(q,p) = 0;
                domain = addConstraint(domain,'Nonlinear-Equality',...
                    'y2Pos',domain.nOutputs,1,...
                    {{'t','q','a'}},-5e-4,5e-4,extra);
                % relative degree 2 output (velocity level):
                % \dot{ya2(q)- yd2(q,p)} = 0;
                domain = addConstraint(domain,'Nonlinear-Equality',...
                    'y2Vel',domain.nOutputs,1,...
                    {{'t','q','dq','a'}},-5e-4,5e-4,extra);
                switch obj.options.IntegrationScheme
                    case 'Hermite-Simpson'
                        nodeList = 1:2:domain.nNode;
                        for j = nodeList
                            extra = [(j+1)/2,(domain.nNode+1)/2];
                            if ~isempty(domain.outputs.actual.degreeOneOutput)
                                % relative degree 1 output
                                domain = addConstraint(domain,'Nonlinear-Equality',...
                                    'y1Acc',1,j,...
                                    {{'t','q','dq','ddq','v'}},0,0,extra);
                            end
                            % relative degree 2 output (acceleration level):
                            % \ddot{ya2(q)- yd2(q,p)} = 0;
                            domain = addConstraint(domain,'Nonlinear-Equality',...
                                'y2Acc',domain.nOutputs,j,...
                                {{'t','q','dq','ddq','a'}},0,0,extra);
                        end
                        for j = nodeList(1:end-1)
                            extra = [(j+1)/2,(domain.nNode+1)/2];
                            
                            if ~isempty(domain.outputs.actual.degreeOneOutput)
                                % relative degree 1 output
                                domain = addConstraint(domain,'Nonlinear-Equality',...
                                    'y1AccMid',1,j,...
                                    {{'t'},{'q','dq','ddq','v'}},0,0,extra);
                            end
                            % relative degree 2 output (acceleration level):
                            % \ddot{ya2(q)- yd2(q,p)} = 0;
                            domain = addConstraint(domain,'Nonlinear-Equality',...
                                'y2AccMid',domain.nOutputs,j,...
                                {{'t'},{'q','dq','ddq','a'}},0,0,extra);
                        end
                    case 'Trapezoidal'
                        nodeList = 1:domain.nNode;
                        for j = nodeList
                            extra = [j,domain.nNode];
                            if ~isempty(domain.outputs.actual.degreeOneOutput)
                                % relative degree 1 output
                                domain = addConstraint(domain,'Nonlinear-Equality',...
                                    'y1Acc',1,j,...
                                    {{'t','q','dq','ddq','v'}},0,0,extra);
                            end
                            % relative degree 2 output (acceleration level):
                            % \ddot{ya2(q)- yd2(q,p)} = 0;
                            domain = addConstraint(domain,'Nonlinear-Equality',...
                                'y2Acc',domain.nOutputs,j,...
                                {{'t','tn','q','dq','ddq','p'}},0,0,extra);
                        end
                end
            end %/end of time based loop
        end %/end of IF open loop controller

        

        %% register domain specific inner-node constraints %%%%%%%%%%%%%%%
        %% hip velocity constraints
        if isfield(domain.auxiliaryConstr,'hipVelocity')            
            domain = addConstraint(domain,'Nonlinear-Inequality',...
                                   'hipVelocity',1,1:domain.nNode,{{'q','dq'}},...
                                   domain.auxiliaryConstr.hipVelocity.min,...
                                   domain.auxiliaryConstr.hipVelocity.max);
        end
        
        %% human data constraints
        if obj.options.HumanDataConstraints
            humanOutputMin = [-1, 0.1, -1, -1];
            humanOutputMax = [1,  0.1,  1,  1];
            domain = addConstraint(domain,'Nonlinear-Inequality',...
                                   'humanData',4,1:domain.nNode,{{'q','p'}},...
                                   humanOutputMin,...
                                   humanOutputMax);
        end
        
        %% impact velocity
        if isfield(domain.auxiliaryConstr,'impactVelocity2D')
            domain = addConstraint(domain,'Nonlinear-Inequality',...
                            'impactVel2D', 2, domain.nNode, {{'q','dq'}},...
                            domain.auxiliaryConstr.impactVelocity2D.min,...
                            domain.auxiliaryConstr.impactVelocity2D.max);
        end
        
        if isfield(domain.auxiliaryConstr,'impactVelocity')
            domain = addConstraint(domain,'Nonlinear-Inequality',...
                            'impactVel',3,domain.nNode,{{'q','dq'}},...
                            domain.auxiliaryConstr.impactVelocity.min,...
                            domain.auxiliaryConstr.impactVelocity.max);
        end
        
        if isfield(domain.auxiliaryConstr,'impactHeelVelocity')
            domain = addConstraint(domain,'Nonlinear-Inequality',...
                            'impactHeelVel',3,domain.nNode,{{'q','dq'}},...
                            domain.auxiliaryConstr.impactHeelVelocity.min,...
                            domain.auxiliaryConstr.impactHeelVelocity.max);
        end
        
        %% ZMP
        if obj.options.ZMPConstraints && isfield(domain.auxiliaryConstr,'ZMP')
            % ZMP constraints:
            %##heck: do not impose ZMP constraints on first two and last
            %two point, since the constraints force is zero. Or it could be
            %done be setting different margin (continuously changing) for
            %each node.
            domain = addConstraint(domain,'Linear-Inequality',...
                                  'zmp',domain.auxiliaryConstr.ZMP.dimension,...
                                  1:domain.nNode-2,{{'Fe'}}, -inf,...
                                  -domain.auxiliaryConstr.ZMP.margin);
        end
        
        %%
        if isfield(domain.auxiliaryConstr,'position')
            dimension = numel(domain.auxiliaryConstr.position);
            pos_min = zeros(dimension,1);
            pos_max = zeros(dimension,1);
            for k = 1:dimension
                pos_min(k) = domain.auxiliaryConstr.position(k).min;
                pos_max(k) = domain.auxiliaryConstr.position(k).max;
            end
            
            domain = addConstraint(domain,'Nonlinear-Inequality',...
                                   'posConstr',dimension,1:domain.nNode,...
                                   {{'q'}},pos_min,pos_max);
        end
        
        %%
        if isfield(domain.auxiliaryConstr,'outputs')
            dimension = numel(domain.auxiliaryConstr.outputs);
            output_min = zeros(dimension,1);
            output_max = zeros(dimension,1);
            for k = 1:dimension
                output_min(k) = domain.auxiliaryConstr.outputs(k).min;
                output_max(k) = domain.auxiliaryConstr.outputs(k).max;
            end
            if obj.options.TimeBased
                domain = addConstraint(domain,'Nonlinear-Inequality',...
                    'outputBoundary',dimension,1:domain.nNode,{{'q'}},...
                    output_min,output_max);
            else
                domain = addConstraint(domain,'Nonlinear-Inequality',...
                    'outputBoundary',dimension,1:domain.nNode,{{'q','p','a'}},...
                    output_min,output_max);
            end
        end
        
        %% foot clearance
        if isfield(domain.auxiliaryConstr,'FootClearance')
            quadCoef = [domain.auxiliaryConstr.FootClearance.a,...
                        domain.auxiliaryConstr.FootClearance.ep,...
                        domain.auxiliaryConstr.FootClearance.hmax,...
                        domain.auxiliaryConstr.FootClearance.h0];

            if ~obj.options.TimeBased
                if domain.domainIndex == 1
                    errbound = -1.1;
                else
                    errbound = -1.1;
                end
                
                domain = addConstraint(domain,'Nonlinear-Inequality',...
                                       'footClearance',1,1:domain.nNode,...
                                       {{'q','p'}}, errbound, 0.0, ...
                                       quadCoef);

            else
                % foot height clearance
                switch obj.options.IntegrationScheme
                    case 'Hermite-Simpson'
                        nodeList = 1:2:domain.nNode;
                        for j = nodeList
                            extra = [(j+1)/2,(domain.nNode+1)/2];
                            domain = addConstraint(domain,'Nonlinear-Inequality',...
                                'footClearance',1,j,{{'q'}},-0.1,0,[extra,quadCoef]);
                        end
                    case 'Trapezoidal'
                        nodeList = 1:1:domain.nNode;
                        for j = nodeList
                            extra = [j,domain.nNode];
                            
                            domain = addConstraint(domain,'Nonlinear-Inequality',...
                                'footClearance',1,j,{{'q'}},-0.1,0,[extra,quadCoef]);
                        end
                end
            end
        end
        
        if isfield(domain.auxiliaryConstr,'ToeClearance')
            quadCoef = [domain.auxiliaryConstr.ToeClearance.a,...
                domain.auxiliaryConstr.ToeClearance.ep,...
                domain.auxiliaryConstr.ToeClearance.hmax,...
                domain.auxiliaryConstr.ToeClearance.h0];
            if ~obj.options.TimeBased
                % foot height clearance
                domain = addConstraint(domain,'Nonlinear-Inequality',...
                                       'toeClearance',1,1:domain.nNode,...
                                       {{'q','h'}},-0.1,0,quadCoef);
            else
                % foot height clearance
                switch obj.options.IntegrationScheme
                    case 'Hermite-Simpson'
                        nodeList = 1:2:domain.nNode;
                        for j = nodeList
                            extra = [(j+1)/2,(domain.nNode+1)/2];
                            domain = addConstraint(domain,'Nonlinear-Inequality',...
                                'toeClearance',1,j,{{'q'}},-0.1,0,[extra,quadCoef]);
                        end
                    case 'Trapezoidal'
                        nodeList = 1:1:domain.nNode;
                        for j = nodeList
                            extra = [j,domain.nNode];
                            
                            domain = addConstraint(domain,'Nonlinear-Inequality',...
                                                   'toeClearance',1,j,{{'q'}},...
                                                   -0.1,0,[extra,quadCoef]);
                        end
                end
            end
        end
        
        if isfield(domain.auxiliaryConstr,'HeelClearance')
            quadCoef = [domain.auxiliaryConstr.HeelClearance.a,...
                domain.auxiliaryConstr.HeelClearance.ep,...
                domain.auxiliaryConstr.HeelClearance.hmax,...
                domain.auxiliaryConstr.HeelClearance.h0];
            if ~obj.options.TimeBased
                % foot height clearance
                domain = addConstraint(domain,'Nonlinear-Inequality',...
                                       'heelClearance',1,1:domain.nNode,...
                                       {{'q','h'}},-0.1,0,quadCoef);
            else
                % foot height clearance
                switch obj.options.IntegrationScheme
                    case 'Hermite-Simpson'
                        nodeList = 1:2:domain.nNode;
                        for j = nodeList
                            extra = [(j+1)/2,(domain.nNode+1)/2];
                            domain = addConstraint(domain,'Nonlinear-Inequality',...
                                                   'heelClearance',1,j,{{'q'}},...
                                                   -0.1,0,[extra,quadCoef]);
                        end
                    case 'Trapezoidal'
                        nodeList = 1:1:domain.nNode;
                        for j = nodeList
                            extra = [j,domain.nNode];
                            
                            domain = addConstraint(domain,'Nonlinear-Inequality',...
                                'heelClearance',1,j,{{'q'}},-0.1,0,[extra,quadCoef]);
                        end
                end
            end
        end

        %% common inter-node constraints        
        switch obj.options.IntegrationScheme
            case 'Hermite-Simpson'
                nodeList = 1:2:domain.nNode-1;
                domain   = addConstraint(domain,'Linear-Equality',...
                                         'timeCont',1,nodeList,...
                                         {{'t'},{},{'t'}}, 0, 0);
                for j = nodeList
                    % integration constraints (position level)
                    % q(i+1) - q(i) - (dt/2) * (dq(i+1) + dq(i)) = 0;
                    extra = [(j+1)/2, (domain.nNode+1)/2];
                    domain = addConstraint(domain,'Nonlinear-Equality',...
                                           'intPos',domain.nDof,j,...
                                           {{'t','q','dq'},{'dq'},...
                                           {'q','dq'}},0,0,extra);
                    
                    % integration constraints (velocity level)
                    % dq(i+1) - dq(i) - (dt/2) * (ddq(i+1) + ddq(i)) = 0;
                    domain = addConstraint(domain,'Nonlinear-Equality',...
                                           'intVel',domain.nDof,j,...
                                           {{'t','dq','ddq'},{'ddq'},...
                                           {'dq','ddq'}},0,0,extra);
                    
                    domain = addConstraint(domain,'Nonlinear-Equality',...
                                           'midPointPos',domain.nDof,j,...
                                           {{'t','q','dq'},{'q'},...
                                           {'q','dq'}},0,0,extra);
                    
                    domain = addConstraint(domain,'Nonlinear-Equality',...
                        'midPointVel',domain.nDof,j,...
                        {{'t','dq','ddq'},{'dq'},{'dq','ddq'}},0,0,extra);
                    
                    if obj.options.UseLIPMConstraints
                        domain = addConstraint(domain,'Nonlinear-Equality',...
                            'LipmIntPos',2,j,...
                            {{'t','xc','dxc'},{'dxc'},{'xc','dxc'}},0,0,extra);
                        
                        % integration constraints (velocity level)
                        % dq(i+1) - dq(i) - (dt/2) * (ddq(i+1) + ddq(i)) = 0;
                        domain = addConstraint(domain,'Nonlinear-Equality',...
                            'LipmIntVel',2,j,...
                            {{'t','dxc','ddxc'},{'ddxc'},{'dxc','ddxc'}},0,0,extra);
                        
                        domain = addConstraint(domain,'Nonlinear-Equality',...
                            'LipmIntZMP',2,j,...
                            {{'t','xz','dxz'},{'dxz'},{'xz','dxz'}},0,0,extra);
                        
                        
                        domain = addConstraint(domain,'Nonlinear-Equality',...
                            'LipmMidPos',2,j,...
                            {{'t','xc','dxc'},{'xc'},{'xc','dxc'}},0,0,extra);
                        
                        % integration constraints (velocity level)
                        % dq(i+1) - dq(i) - (dt/2) * (ddq(i+1) + ddq(i)) = 0;
                        domain = addConstraint(domain,'Nonlinear-Equality',...
                            'LipmMidVel',2,j,...
                            {{'t','dxc','ddxc'},{'dxc'},{'dxc','ddxc'}},0,0,extra);
                        
                        domain = addConstraint(domain,'Nonlinear-Equality',...
                            'LipmMidZMP',2,j,...
                            {{'t','xz','dxz'},{'xz'},{'xz','dxz'}},0,0,extra);
                        
                    end
                end
                
            case 'Trapezoidal'
                
                nodeList = 1:1:(domain.nNode-1);
                domain = addConstraint(domain,'Linear-Equality',...
                    'timeCont',1,nodeList,...
                    {{'t'},{'t'}},0,0);
                for j = nodeList
                    % integration constraints (position level)
                    % q(i+1) - q(i) - (dt/2) * (dq(i+1) + dq(i)) = 0;
                    extra = [j,domain.nNode];
                    domain = addConstraint(domain,'Nonlinear-Equality',...
                        'intPos',domain.nDof,j,...
                        {{'t','q','dq'},{'q','dq'}},0,0,extra);
                    
                    % integration constraints (velocity level)
                    % dq(i+1) - dq(i) - (dt/2) * (ddq(i+1) + ddq(i)) = 0;
                    domain = addConstraint(domain,'Nonlinear-Equality',...
                        'intVel',domain.nDof,j,...
                        {{'t','dq','ddq'},{'dq','ddq'}},0,0,extra);
                    
                    
                    if obj.options.UseLIPMConstraints
                        domain = addConstraint(domain,'Nonlinear-Equality',...
                            'LipmIntPos',2,j,...
                            {{'t','xc','dxc'},{'xc','dxc'}},0,0,extra);
                        
                        % integration constraints (velocity level)
                        % dq(i+1) - dq(i) - (dt/2) * (ddq(i+1) + ddq(i)) = 0;
                        domain = addConstraint(domain,'Nonlinear-Equality',...
                            'LipmIntVel',2,j,...
                            {{'t','dxc','ddxc'},{'dxc','ddxc'}},0,0,extra);
                        
                        domain = addConstraint(domain,'Nonlinear-Equality',...
                            'LipmIntZMP',2,j,...
                            {{'t','xz','dxz'},{'xz','dxz'}},0,0,extra);
                    end
                end
            otherwise
                error('Undefined Integration Scheme.\n');
        end
        
        if ~obj.options.OpenLoopController
            % parameter continuity: p(i+1,:) - p(i,:) = 0;
            domain = addConstraint(domain, 'Linear-Equality',...
                                   'pCont', domain.nParamPhaseVar, ...
                                   1:(domain.nNode-1),...
                                   {{'p'},{'p'}}, 0, 0);
            
            if ~isempty(domain.outputs.actual.degreeOneOutput)
                domain = addConstraint(domain,'Linear-Equality',...
                                       'vCont',domain.nParamRD1,...
                                       1:(domain.nNode-1),...
                                       {{'v'},{'v'}},0,0);
            end
            
            domain = addConstraint(domain,'Linear-Equality',...
                                   'aCont',domain.nParamRD2,...
                                   1:(domain.nNode-1),...
                                   {{'a'},{'a'}}, 0, 0);
        end
        
        %%% step parameters continuity constraints
        domain = addConstraint(domain, 'Linear-Equality',...
                               'desHolCont', 2, 1:(domain.nNode-1),...
                               {{'h'},{'h'}}, 0, 0);
        
        
        %% CoM position constraints ###need updates the condition
        %         if i == 1
        %             domain = addConstraint(domain,'Nolinear-Inequality',...
        %                  'ycom',2,...
        %                  1:domain.nNode,{{'q'}},[-inf,0]);
        %         end
        
        
        %% common inter-domain constraints
        if i == obj.nDomain
            next_domain = obj.domains{1};
        else
            next_domain = obj.domains{i+1};
            if ~obj.options.OpenLoopController
                % parameter continuity: p(i+1,:) - p(i,:) = 0;
                cur_deps  = domain.optVarIndices.p(end,:);
                next_deps = next_domain.optVarIndices.p(1,:);
                domain = addConstraint(domain,'Inter-Domain-Linear',...
                                       'pCont',numel(cur_deps), domain.nNode,...
                                       {cur_deps,next_deps},0,0);
                
                if ~isempty(domain.outputs.actual.degreeOneOutput)
                    cur_deps = domain.optVarIndices.v(end,:);
                    next_deps = next_domain.optVarIndices.v(1,:);
                    domain = addConstraint(domain,'Inter-Domain-Linear',...
                        'vCont',numel(cur_deps),domain.nNode,...
                        {cur_deps,next_deps},0,0);
                end
                
                if obj.options.UseSameParameters
                    cur_deps  = domain.optVarIndices.a(end,:);
                    next_deps = next_domain.optVarIndices.a(1,:);
                    func = str2func(['f_aContDomain_',domain.domainName]);
                    constr = func([cur_deps,next_deps]);
                    dimension = numel(constr);
                    domain = addConstraint(domain,'Inter-Domain-Linear',...
                                           'aContDomain',dimension,domain.nNode,...
                                           {cur_deps,next_deps},0,0);
                end
            end
            
            % desired holonomic constraints: h(i+1,:) - h(i,:) = 0;
            cur_deps  = domain.optVarIndices.h(1,:);
            next_deps = next_domain.optVarIndices.h(1,:);
            dimension = min(numel(cur_deps),numel(next_deps));
            domain = addConstraint(domain,'Inter-Domain-Linear',...
                                   'desHolCont',dimension,domain.nNode,...
                                   {cur_deps,next_deps},0,0);
        end
        
        
        %%  if the gait is not periodic, then there is no reset map
        %%% constraints for the last domain
        if obj.options.PeriodicGait || (i~=obj.nDomain)
            % angle continuity constraints: q0(i+1) - (R*qf(i)) = 0;
            cur_deps  = domain.optVarIndices.q(end,:);
            next_deps = next_domain.optVarIndices.q(1,:);
            domain = addConstraint(domain,'Inter-Domain-Nonlinear',...
                'qResetMap',domain.nDof,domain.nNode,...
                {cur_deps,next_deps},0,0);
            % velocity continuity constraints: dq0(i+1) - (R*ResetMap(qf(i),dqf(i))) = 0;
            if domain.hasImpact
                cur_deps = [domain.optVarIndices.q(end,:),...
                    domain.optVarIndices.dq(end,:),...
                    domain.optVarIndices.Fimp(end,:)];
                if domain.isSwapping
                    next_deps = [next_domain.optVarIndices.q(1,:),...
                        next_domain.optVarIndices.dq(1,:)];
                else
                    next_deps = next_domain.optVarIndices.dq(1,:);
                end
                domain = addConstraint(domain,'Inter-Domain-Nonlinear',...
                    'dqResetMap',domain.nDof+domain.nImpConstr,...
                    domain.nNode,{cur_deps,next_deps},0,0);
            else
                cur_deps = domain.optVarIndices.dq(end,:);
                next_deps = next_domain.optVarIndices.dq(1,:);
                domain = addConstraint(domain,'Inter-Domain-Nonlinear',...
                    'dqResetMap',domain.nDof,...
                    domain.nNode,{cur_deps,next_deps},0,0);
            end
        end
        
        
        if obj.options.UseLIPMConstraints
            
            domain = addConstraint(domain,'Nonlinear-Equality',...
                'LipmDynamics',4,1:domain.nNode,...
                {{'xc','ddxc','xz','dxz','uz','z0'}},0,0);
            switch domain.domainName
                case 'RightDS3DFlatWalking'
                    domain = addConstraint(domain,'Nonlinear-Inequality',...
                        'pcop',6,1:domain.nNode,...
                        {{'xz','h'}},-inf,0);
                case 'RightSS3DFlatWalking'
                    domain = addConstraint(domain,'Nonlinear-Inequality',...
                        'pcop',4,1:domain.nNode,...
                        {{'xz','h'}},-inf,0);
                    
            end
            if ~strcmp(obj.options.ObjectiveType,'ZMP')
                domain = addConstraint(domain,'Nonlinear-Inequality',...
                    'pcom',3,1:domain.nNode,...
                    {{'q','xc','z0'}},-0.01,0.01);
            end
            domain = addConstraint(domain,'Nonlinear-Inequality',...
                'z0const',1,1:(domain.nNode-1),...
                {{'z0'},{'z0'}},0,0);
            
            if i~=obj.nDomain
                % LIPM state/control continuity
                cur_deps = domain.optVarIndices.xc(end,:);
                next_deps = next_domain.optVarIndices.xc(1,:);
                dimension = min(numel(cur_deps),numel(next_deps));
                domain = addConstraint(domain,'Inter-Domain-Linear',...
                    'LipmCont',dimension,domain.nNode,...
                    {cur_deps,next_deps},0,0);
                
                cur_deps = domain.optVarIndices.dxc(end,:);
                next_deps = next_domain.optVarIndices.dxc(1,:);
                dimension = min(numel(cur_deps),numel(next_deps));
                domain = addConstraint(domain,'Inter-Domain-Linear',...
                    'LipmCont',dimension,domain.nNode,...
                    {cur_deps,next_deps},0,0);
                
                cur_deps = domain.optVarIndices.ddxc(end,:);
                next_deps = next_domain.optVarIndices.ddxc(1,:);
                dimension = min(numel(cur_deps),numel(next_deps));
                domain = addConstraint(domain,'Inter-Domain-Linear',...
                    'LipmCont',dimension,domain.nNode,...
                    {cur_deps,next_deps},0,0);
                
                cur_deps = domain.optVarIndices.xz(end,:);
                next_deps = next_domain.optVarIndices.xz(1,:);
                dimension = min(numel(cur_deps),numel(next_deps));
                domain = addConstraint(domain,'Inter-Domain-Linear',...
                    'LipmCont',dimension,domain.nNode,...
                    {cur_deps,next_deps},0,0);
                
                cur_deps = domain.optVarIndices.dxz(end,:);
                next_deps = next_domain.optVarIndices.dxz(1,:);
                dimension = min(numel(cur_deps),numel(next_deps));
                domain = addConstraint(domain,'Inter-Domain-Linear',...
                    'LipmCont',dimension,domain.nNode,...
                    {cur_deps,next_deps},0,0);
                
                cur_deps = domain.optVarIndices.uz(end,:);
                next_deps = next_domain.optVarIndices.uz(1,:);
                dimension = min(numel(cur_deps),numel(next_deps));
                domain = addConstraint(domain,'Inter-Domain-Linear',...
                    'LipmCont',dimension,domain.nNode,...
                    {cur_deps,next_deps},0,0);
                
                cur_deps = domain.optVarIndices.z0(end,:);
                next_deps = next_domain.optVarIndices.z0(1,:);
                dimension = min(numel(cur_deps),numel(next_deps));
                domain = addConstraint(domain,'Inter-Domain-Linear',...
                    'z0const',dimension,domain.nNode,...
                    {cur_deps,next_deps},0,0);
                
            end
        end

        % configure domain structure
        domain = configConstrStructure(domain,...
                                       obj.dimsConstr,...
                                       obj.nzmaxConstr);

        constraints{i} = domain.constrArray;
        % update the dimension of constraints and jacobian
        obj.dimsConstr = obj.dimsConstr + domain.dimsConstr;

        obj.nzmaxConstr= obj.nzmaxConstr + domain.nzmaxConstr;
        
        obj.domains{i} = domain;

    end
    
    obj.constrArray = vertcat(constraints{:});
    nConstr = numel(obj.constrArray);
    
    % generate entry structure for sparse jacobian matrix
    obj.constrRows = ones(obj.nzmaxConstr,1);
    obj.constrCols = ones(obj.nzmaxConstr,1);
    constr_lb         = zeros(obj.dimsConstr,1);
    constr_ub         = zeros(obj.dimsConstr,1);
    for i=1:nConstr
        % get dimension of the constraint dimension
        dims = obj.constrArray(i).dims;
        indices = obj.constrArray(i).c_index;
        % get jacobian entry indices
        j_index = obj.constrArray(i).j_index;
        
        % get number of dependent variables
        deps = obj.constrArray(i).deps;
        num_deps = numel(deps);
        
        % rows (i)
        obj.constrRows(j_index,1) = reshape(indices*ones(1,num_deps),...
            [numel(j_index),1]);
        % columns (j)
        obj.constrCols(j_index,1) = reshape(ones(dims,1)*deps,...
            [numel(j_index),1]);
        
        
        % constraints bound
        constr_lb(indices,1) = obj.constrArray(i).cl;
        constr_ub(indices,1) = obj.constrArray(i).cu;
    end
    
    obj.cl = constr_lb;
    obj.cu = constr_ub;
    
    
end