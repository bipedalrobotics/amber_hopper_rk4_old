function [obj] = genBoundaries(obj)
    % genBoundaries - Generate upper and lower boundaries
    % for all optimization variables
    %
    %
    % Author: Ayonga Hereid <ayonga@tamu.edu>
    %         Wen-Loong Ma <wenloong.ma@gmail.com> [modified for running]

    % check required arguments

    if obj.nOptVar == 0
        error('optDirectDomain:checkArgs',...
              'Please generate indices for optimization variables first.');
    end
    obj.lb = -inf(obj.nOptVar,1);
    obj.ub =  inf(obj.nOptVar,1);
            
    for j = 1:obj.nDomain

        current_domain = obj.domains{j};
        optVarArray = vertcat(current_domain.optVars{:});
        for i=1:length(optVarArray)
            var = optVarArray(i);

            % if the 'lb' is scaler, then assume the set of variables has
            % the same lower bound.
            if isscalar(var.lb)
                lowerbound = var.lb*ones(var.dimension,1);
            else
                lowerbound = var.lb;
            end
            % if the 'ub' is scaler, then assume the set of variables has
            % the same upper bound.
            if isscalar(var.ub)
                upperbound = var.ub*ones(var.dimension,1);
            else
                upperbound = var.ub;
            end
            
            % set boundary condition for initial and final states, if the
            % target states are not empty
            if (var.node == 1) 
                if strcmp(var.name,'q')
                    if ~isempty(current_domain.target_q0)
                        lowerbound = current_domain.target_q0;
                        upperbound = current_domain.target_q0;
                    end
                end
                if strcmp(var.name,'dq')
                    if ~isempty(current_domain.target_dq0)
                        lowerbound = current_domain.target_dq0;
                        upperbound = current_domain.target_dq0;
                    end
                end
            end
            
            if (var.node == current_domain.nNode) 
                if strcmp(var.name,'q')
                    if ~isempty(current_domain.target_qf)
                        lowerbound = current_domain.target_qf;
                        upperbound = current_domain.target_qf;
                    end
                end
                if strcmp(var.name,'dq')
                    if ~isempty(current_domain.target_dqf)
                        lowerbound = current_domain.target_dqf;
                        upperbound = current_domain.target_dqf;
                    end
                end
            end
            

            % assign the boundary value over all nodes
            obj.lb(current_domain.optVarIndices.(var.name)(var.node,:)) = ...
                   transpose(lowerbound);
            obj.ub(current_domain.optVarIndices.(var.name)(var.node,:)) = ...
                   transpose(upperbound);

        end
    end %/end of for domain loop
    
    
    %% Extra constraints on chosen point
    config_file = fullfile('..', 'config', 'opt', 'opt_2DRuning.yaml');
    optConfig   = cell_to_matrix_scan(yaml_read_file(config_file));
    
    obj.ub(obj.domains{1}.optVarIndices.q(1, 1)) = ...
                                      optConfig.specialPoints.px_0_ss_ub;
    obj.lb(obj.domains{1}.optVarIndices.dq(1, 1)) = ...
                                      optConfig.specialPoints.px_dot_0_ss_lb;
    %obj.lb(obj.domains{1}.optVarIndices.dq(end, 2)) = ...
    %                                  optConfig.specialPoints.pz_dot_f_ss_lb;

    
end