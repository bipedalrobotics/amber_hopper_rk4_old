function [obj] = configureObjective(obj)
    % addCost - register cost function
    %
    % Copyright 2014-2015 Texas A&M University AMBER Lab
    % Author: Ayonga Hereid <ayonga@tamu.edu>
    obj.nzmaxCost = 0;
    costInfos = cell(obj.nDomain,1);
    for i=1:obj.nDomain
        switch obj.options.ObjectiveType
            case 'power'
                nodeList = 1:2:obj.domains{i}.nNode-1;
                for j = nodeList
                    extra = [(j+1)/2,(obj.domains{i}.nNode+1)/2];
                    obj.domains{i} = addCost(obj.domains{i},'powerCost',...
                        {'t','dq','u'}, j, extra);
                end
            case 'scot'
                nodeList = 1:2:obj.domains{i}.nNode-1;
                for j = nodeList
                    extra = [(j+1)/2,(obj.domains{i}.nNode+1)/2];
                    obj.domains{i} = addCost(obj.domains{i},'scotCost',...
                        {'t','dq','u','h'}, j, extra);
                end
            case 'torque'
                obj.domains{i} = addCost(obj.domains{i},'torqueCost',...
                    {'u'}, 1:obj.domains{i}.nNode);
            case 'HumanData'
                obj.domains{i} = addCost(obj.domains{i},'humanCost',...
                    {'q','p'}, 1:obj.domains{i}.nNode);
            case 'ZMP'
                obj.domains{i} = addCost(obj.domains{i},'zmpCost',...
                    {'q','xc','z0'}, 1:obj.domains{i}.nNode);
            case 'zero'
                obj.domains{i} = addCost(obj.domains{i},'zeroCost',...
                    {'u'}, 1:obj.domains{i}.nNode);
            case 'parameters'
                obj.domains{i} = addCost(obj.domains{i},'parameterCost',...
                    {'a'}, 1:obj.domains{i}.nNode);
        end
        % configure domain structure
        obj.domains{i} = configObjectiveStructure(obj.domains{i},...
            obj.nzmaxCost);
        
        
        obj.nzmaxCost = obj.nzmaxCost + obj.domains{i}.nzmaxCost;
        costInfos{i} = obj.domains{i}.costArray;
    end
    obj.costArray = vertcat(costInfos{:});
    nCosts = numel(obj.costArray);
    % construct the row and column indices for the sparse jacobian
    % matrix
    obj.costRows = ones(obj.nzmaxCost,1);
    obj.costCols = ones(obj.nzmaxCost,1);
    for i=1:nCosts
        
        j_index = obj.costArray(i).j_index;
        obj.costCols(j_index) = obj.costArray(i).deps;

    end
end
