%%things to test after getting a gait
savePlot = false;
showPlot = false;


%% animate the simulation 
anim_plot_3D(model, calcs_steps, false, 1200);


%% extract one step data
[calcs_steps] = extractResult(opt, x, model);


%% plot the states
plotStates(model, calcs_steps, savePlot);


%% plot tau
figure(101); clf;
plot(calcs_steps{1}.t, calcs_steps{1}.tau,...
     calcs_steps{2}.t, calcs_steps{2}.tau);
legend('\tau'); grid on


%% plot nsf
% figure(119)
% subplot(1,2,1)
% for i = 1:4
%     plot(calcs_steps{1}.t, calcs_steps{1}.yd2(i,:)); hold on
% end
% title('yd2, domain 1')
% subplot(1,2,2)
% for i = 1:4
%     plot(calcs_steps{2}.t, calcs_steps{2}.yd2(i,:)); hold on
% end
% title('yd2, domain 2')

figure(118); clf
subplot(1,2,1)
for i = 1:4
    plot(calcs_steps{1}.t, calcs_steps{1}.y2(i,:)); hold on
end
title('y2 error, domain 1')
subplot(1,2,2)
for i = 1:4
    plot(calcs_steps{2}.t, calcs_steps{2}.y2(i,:)); hold on
end
title('y2 error, domain 2')


%% plot outputs


%% export parameters
% exportParameters(opt, x, model)

% if ~showPlot
%     keyboard;
%     close all;
% end
