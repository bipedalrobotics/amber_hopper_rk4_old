clc;

% rh, fh, rk
syms var1 var2 var3; 
Lc  = 0.572;
Lth = 0.224;

r = Lth* cos(var1 /2 - var2 /2) + Lc* cos(var3 - var1/2 + var2/2);
w = Lth^2 + r^2 - Lc^2 - 2*r*Lth*cos(var1/2 - var2/2);

w;
% expand(w) 
simplify(expand(w))


%% for rest map
% used in calcResetMap.m and OptDomain.m

%    x  z  tor fh rh fk rk
syms q1 q2 q3  q4 q5 q6 q7;

r = Lth* cos(q5/2 - q4/2) + Lc* cos(q6 - q5/2 + q4/2);
q = q5/2 + q4/2 + q3 - pi;

r * sin(q);


%% holonomic constraint of front knee, the version that doesn't require q_rh
clc;
syms qtor qfh qrh qfk qrk;


%syms Lc
%syms Lth
 Lth = 0.224;
 Lc  = 0.572;

%Fx = Lth*sin(qfh + qtor) + Lc*sin(qfh + qfk + qtor);
%Fz = Lth*cos(qfh + qtor) + Lc*cos(qfh + qfk + qtor);
%r  = simplify( sqrt(Fx^2 + Fz^2) )
%clear r;
r = sqrt(Lc^2 + Lth^2 + 2*Lc*Lth*cos(qfk))
% r  = vpa(r)

const = Lth^2 + r^2  - Lc^2 - 2*r*Lth*cos(qrh/2 - qfh/2)